var fs = require('fs');

// RegExp
RegExp.prototype.execAll = function(string) {
    var match = null;
    var matches = new Array();
    while (match = this.exec(string)) {
        var matchArray = [];
        for (i in match) {
            if (parseInt(i) == i) {
                matchArray.push(match[i]);
            }
        }
        matches.push(matchArray);
    }
    return matches;
}

console.log('updating karma.conf.js files list');

// Getting index.html text
var indexStr = fs.readFileSync('./src/index.html', 'utf8');

// Getting scripts sources
var jsRegExp = /<script.+?src=\"\/(.+?)\".*?><\/script>/g;
var jsRegMatch = jsRegExp.execAll(indexStr);
var jsPaths = [];
jsRegMatch.forEach(function(item){
	jsPaths.push(item[1]);
});

// Generating code to insert into file
var jsPathsStr = '';
jsPaths.forEach(function(item){
	if((item.indexOf('app.env.js') >= 0)||(item.indexOf('app.templates.js') >= 0)) {
		return jsPathsStr += '        ,".tmp/' + item + "\"\n";
	}
	jsPathsStr += '        ,"src/' + item + "\"\n";
});
jsPathsStr = "// App files start\n" + jsPathsStr + '        // App files end';

// Updating karma.conf.js file
var karmaConfStr = fs.readFileSync('./karma.conf.js', 'utf8');
var karmaConfFilesPlaceholderRegexp = /\/\/ App files start[\s\S]+?\/\/ App files end/g;
karmaConfStr = karmaConfStr.replace(karmaConfFilesPlaceholderRegexp, jsPathsStr);
fs.writeFileSync('./karma.conf.js', karmaConfStr, 'utf8');

console.log('updating karma.conf.js files list done');