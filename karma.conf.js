// Karma configuration
// Generated on Thu Jun 30 2016 10:28:18 GMT+0300 (EEST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha', 'chai'],

    // plugins: ['karma-mocha-reporter'],


    // list of files / patterns to load in the browser
    files: [
        // Mockups
         "test/libs/karma-read-json/karma-read-json.js"
        ,{pattern: 'test/mockups/*.json', included: false}
        ,"test/mockups/mockups.js"
        // Tools
        ,"test/unit/tools.js"
        // App files start
        ,"src/assets/libs/log/log.js"
        ,"src/assets/libs/md5/md5.min.js"
        ,"src/assets/libs/filesaver/file-saver.min.js"
        ,"src/assets/libs/chart.js/dist/Chart.min.js"
        ,"src/assets/libs/d3/d3.min.js"
        ,"src/assets/libs/underscore/underscore-min.js"
        ,"src/assets/libs/async/dist/async.min.js"
        ,"src/assets/libs/moment/min/moment.min.js"
        ,"src/assets/libs/braintree-web/dist/braintree.js"
        ,"src/assets/libs/tinycolor/dist/tinycolor-min.js"
        ,"src/assets/libs/jquery/dist/jquery.min.js"
        ,"src/assets/libs/jquery-ui/jquery-ui.min.js"
        ,"src/assets/libs/jquery-steps/build/jquery.steps.min.js"
        ,"src/assets/libs/jquery-slimscroll/jquery.slimscroll.min.js"
        ,"src/assets/libs/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"
        ,"src/assets/libs/toastr/toastr.min.js"
        ,"src/assets/libs/angular/angular.min.js"
        ,"src/assets/libs/angular-resource/angular-resource.min.js"
        ,"src/assets/libs/angular-animate/angular-animate.min.js"
        ,"src/assets/libs/angular-aria/angular-aria.min.js"
        ,"src/assets/libs/angular-messages/angular-messages.min.js"
        ,"src/assets/libs/angular-ui-router/release/angular-ui-router.min.js"
        ,"src/assets/libs/angular-ui-sortable/sortable.min.js"
        ,"src/assets/libs/angular-material/angular-material.min.js"
        ,"src/assets/libs/angular-bootstrap/ui-bootstrap-tpls.min.js"
        ,"src/assets/libs/angular-cookies/angular-cookies.js"
        ,"src/assets/libs/angular-scroll/angular-scroll.min.js"
        ,"src/assets/libs/angular-simple-logger/dist/angular-simple-logger.min.js"
        ,"src/assets/libs/angular-google-maps/dist/angular-google-maps.min.js"
        ,"src/assets/libs/ng-file-upload/ng-file-upload-shim.min.js"
        ,"src/assets/libs/ng-file-upload/ng-file-upload.min.js"
        ,"src/assets/libs/angular-chart.js/dist/angular-chart.min.js"
        ,"src/assets/libs/checklist/checklist-model.min.js"
        ,"src/assets/libs/angular-material-datetimepicker/js/angular-material-datetimepicker.min.js"
        ,"src/assets/libs/md-color-picker/files/mdColorPicker.min.js"
        ,"src/assets/libs/googleplace/googleplace.module.min.js"
        ,"src/assets/js/initLog.js"
        ,"src/app/shared/pando/utils/pandoUtils.js"
        ,"src/app/shared/pando/api/pandoApi.js"
        ,"src/app/shared/pando/subscription/pandoSubscription.js"
        ,"src/app/shared/pando/orm/pandoOrm.js"
        ,"src/app/shared/pando/storage/pandoParamsStorage.js"
        ,"src/app/shared/pando/auth/pandoAuth.js"
        ,"src/app/shared/pando/analytics/pandoAnalytics.js"
        ,"src/app/shared/pando/analytics/pandoAnalyticsUtils.js"
        ,".tmp/app/app.env.js"
        ,".tmp/app/app.templates.js"
        ,"src/app/app.module.js"
        ,"src/app/app.config.js"
        ,"src/app/app.routes.js"
        ,"src/app/app.design.js"
        ,"src/app/shared/constants/timezones.js"
        ,"src/app/shared/constants/consts.js"
        ,"src/app/shared/directives/a.js"
        ,"src/app/shared/directives/autofocus.js"
        ,"src/app/shared/directives/focusMe.js"
        ,"src/app/shared/directives/timetostr.js"
        ,"src/app/shared/directives/mdChips.js"
        ,"src/app/shared/filters/capitalize.js"
        ,"src/app/shared/filters/abs.js"
        ,"src/app/shared/services/dialogsService.js"
        ,"src/app/shared/services/localNotifyCenterService.js"
        ,"src/app/shared/services/mainLoaderService.js"
        ,"src/app/shared/services/pagesService.js"
        ,"src/app/shared/services/paramsStorageServce.js"
        ,"src/app/shared/services/toastMsgService.js"
        ,"src/app/shared/services/urlToImgService.js"
        ,"src/app/shared/services/userParamsStorageFactory.js"
        ,"src/app/shared/services/utils.js"
        ,"src/app/shared/appsWeightControl/appsWeightControlDirective.js"
        ,"src/app/components/appLayout/appCtrl.js"
        ,"src/app/components/appLayout/activityIndicatorService.js"
        ,"src/app/components/appLayout/headerCtrl.js"
        ,"src/app/components/appLayout/locationsBarCtrl.js"
        ,"src/app/components/auth/signIn/signInCtrl.js"
        ,"src/app/components/auth/signUp/signUpCtrl.js"
        ,"src/app/components/auth/forgotPass/forgotPassCtrl.js"
        ,"src/app/components/locationDashboard/locationDashboardCtrl.js"
        ,"src/app/components/locationDashboard/locationDashboardDeviceCtrl.js"
        ,"src/app/components/locationDashboard/locationDashboardStatsCtrl.js"
        ,"src/app/components/locationStats/averageShopTimeChart/averageShopTimeChartCtrl.js"
        ,"src/app/components/locationStats/circlesChart/circlesChartDirective.js"
        ,"src/app/components/locationStats/conversionChart/conversionChartCtrl.js"
        ,"src/app/components/locationStats/locationStatsPageCtrl.js"
        ,"src/app/components/locationStats/realtimeStats/realtimeInfographCtrl.js"
        ,"src/app/components/locationStats/realtimeStats/realtimePlayerCtrl.js"
        ,"src/app/components/locationStats/realtimeStats/realtimeStatsCtrl.js"
        ,"src/app/components/locationStats/timelineControl/timelineControlDirective.js"
        ,"src/app/components/locationStats/traficStats/traficStatsCtrl.js"
        ,"src/app/components/locationStats/userHistoryStats/userHistoryDialogCtrl.js"
        ,"src/app/components/locationStats/userHistoryStats/userHistoryStatsCtrl.js"
        ,"src/app/components/locationStats/visitByDayOfWeekChart/visitByDayOfWeekChartCtrl.js"
        ,"src/app/components/locationStats/visitByHourOfDayChart/visitByHourOfDayChartCtrl.js"
        ,"src/app/components/locationStats/visitorsProgressChart/visitorsProgressChartDirective.js"
        ,"src/app/components/locationEdit/locationEditCtrl.js"
        ,"src/app/components/deviceApps/activateFirstAppInvite/activateFirstAppInviteDialogCtrl.js"
        ,"src/app/components/deviceApps/appControl/appControlDirective.js"
        ,"src/app/components/deviceApps/appEdit/appEditGraphicsCtrl.js"
        ,"src/app/components/deviceApps/appEdit/appEditTwitterCtrl.js"
        ,"src/app/components/deviceApps/appEdit/appEditWebviewAddCtrl.js"
        ,"src/app/components/deviceApps/appEdit/appEditWebviewCtrl.js"
        ,"src/app/components/deviceApps/appsMirroringService.js"
        ,"src/app/components/deviceApps/appWidgets/appWidgeHashDirective.js"
        ,"src/app/components/deviceApps/appWidgets/appWidgetGraphicsDirective.js"
        ,"src/app/components/deviceApps/appWidgets/appWidgeTwitterDirective.js"
        ,"src/app/components/deviceApps/deviceAppsCtrl.js"
        ,"src/app/components/deviceApps/devicesWithLocationFilter.js"
        ,"src/app/components/deviceApps/enableMirroring/enableMirroringDialogCtrl.js"
        ,"src/app/components/deviceApps/filters/app.item.src.to.img.filter.js"
        ,"src/app/components/deviceApps/filters/app.to.desc.filter.js"
        ,"src/app/components/deviceApps/filters/app.to.img.filter.js"
        ,"src/app/components/deviceApps/filters/app.to.name.filter.js"
        ,"src/app/components/deviceApps/graphicItems/graphicItemsEditDialogCtrl.js"
        ,"src/app/components/deviceApps/graphicItems/graphicItemsEditIdsCtrl.js"
        ,"src/app/components/deviceApps/graphicItems/graphicItemsEditScheduleCtrl.js"
        ,"src/app/components/deviceApps/graphicItems/graphicItemsListDirective.js"
        ,"src/app/components/deviceApps/graphicItems/graphicItemsTileDirective.js"
        ,"src/app/components/deviceConfig/deviceConfigCtrl.js"
        ,"src/app/components/admin/apps/adminAppsCtrl.js"
        ,"src/app/components/admin/apps/adminAppsUploadDialogCtrl.js"
        ,"src/app/components/admin/companies/adminCompaniesCtrl.js"
        ,"src/app/components/admin/companies/adminSingleCompanyCtrl.js"
        ,"src/app/components/admin/devices/adminDevicesCtrl.js"
        ,"src/app/components/admin/devices/adminSingleDeviceCtrl.js"
        ,"src/app/components/admin/firmwares/adminFirmwaresCtrl.js"
        ,"src/app/components/admin/firmwares/adminFirmwaresUploadDialogCtrl.js"
        ,"src/app/components/admin/users/adminUsersCtrl.js"
        ,"src/app/components/company/billing/billingCtrl.js"
        ,"src/app/components/company/info/infoCtrl.js"
        ,"src/app/components/company/integr/integrCtrl.js"
        ,"src/app/components/company/subscription/addDeviceCtrl.js"
        ,"src/app/components/company/subscription/subscriptionCtrl.js"
        ,"src/app/components/company/users/usersCtrl.js"
        ,"src/app/components/company/users/inviteDialogCtrl.js"
        ,"src/app/components/userSettings/profileCtrl.js"
        ,"src/app/shared/usersEditForm/usersEditFormDirective.js"
        ,"src/app/shared/companyEditForm/companyEditFormDirective.js"
        // App files end
        // Angular mocks
        ,"test/libs/angular-mocks/angular-mocks.js"
        // Tests
        ,"test/**/*.spec.js"
    ],

    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        "src/app/*.js": "coverage",
        "src/app/**/*.js": "coverage"
    },

    coverageReporter: {
        type : 'html',
        dir : 'coverage/'
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['mocha', 'coverage'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
