/**
 * Each module has a <moduleName>.module.js file.  This file contains the angular module declaration -
 * angular.module("moduleName", []);
 * The build system ensures that all the *.module.js files get included prior to any other .js files, which
 * ensures that all module declarations occur before any module references.
 */
(function (module) {
    module.directive('googlePlace', function () {
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };
        var mapping = {
            street_number: 'number',
            route: 'street',
            locality: 'city',
            administrative_area_level_1: 'state',
            country: 'country',
            postal_code: 'zip'
        };
        // https://gist.github.com/VictorBjelkholm/6687484 
        // modified to have better structure for details
        return {
            require: 'ngModel',
            scope: {
                ngModel: '='
            },
            link: function (scope, element, attrs, model) {
                var options = {
                    types: attrs.googlePlace !== "" ? attrs.googlePlace.split(',') : [],
                    componentRestrictions: {}
                };

                if(window.mapsLoaded){
                    initElement();
                }else{
                    var handler = setInterval(function(){
                        if(!window.mapsLoaded) return;
                        clearInterval(handler);
                        initElement();
                    }, 100);
                }
                
                function initElement(){
                    scope.gPlace = new google.maps.places.Autocomplete(element[0], options);
                    google.maps.event.addListener(scope.gPlace, 'place_changed', function () {
                        var place = scope.gPlace.getPlace();
                        var details = place.geometry && place.geometry.location ? {
                            latitude: place.geometry.location.lat(),
                            longitude: place.geometry.location.lng()
                        } : {};
                        // Get each component of the address from the place details
                        // and fill the corresponding field on the form.
                        for (var i = 0; i < place.address_components.length; i++) {
                            var addressType = place.address_components[i].types[0];
                            if (componentForm[addressType]) {
                                var val = place.address_components[i][componentForm[addressType]];
                                details[mapping[addressType]] = val;
                            }
                        }
                        details.formatted = place.formatted_address;
                        details.placeId = place.place_id;
                        scope.$apply(function (){
                            model.$setViewValue(details);
                        });
                    });
                }

            }
        };
    });
    // The name of the module, followed by its dependencies (at the bottom to facilitate enclosure)
}(angular.module("google.places", [])));