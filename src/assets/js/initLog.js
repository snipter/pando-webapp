(function(){
	'use strict';
	
	var logLevel = window.localStorage.getItem('log.level');
    if(logLevel){
        log.opt.level = logLevel;
        log.warn('setting log level from local storage: ' + logLevel, 'log');
    }

})();