angular.module('app')
	.run(function ($rootScope, $state, $stateParams){
		"ngInject";
		$rootScope.$on('$stateChangeStart', function(){
			$rootScope.$state = $state;
		})
	})

	.config(function($locationProvider){
		"ngInject";
		$locationProvider.html5Mode(true);
	})

	// Redirect from one state to another
	.run(function($rootScope, $state){
		"ngInject";
		$rootScope.$on('$stateChangeStart', function(evt, to, params) {
			if(to.redirectTo){
				evt.preventDefault();
				$state.go(to.redirectTo, params, {location: 'replace'})
			}
		});
	})

	.config(function($stateProvider, $urlRouterProvider){
		"ngInject";
		// Auth
		$stateProvider.state('auth', {
			 templateUrl: 'app/components/auth/authPageView.html'
		});
		$stateProvider.state('auth.signIn', {
			 url: '/signin'
			,templateUrl: 'app/components/auth/signIn/signInView.html'
			,data : { title: 'Signin' }
		});
		$stateProvider.state('auth.signUp', {
			 url: '/signup'
			,templateUrl: 'app/components/auth/signUp/signUpView.html'
			,data : { title: 'Signup' }
		});
		$stateProvider.state('auth.signUpToken', {
			 url: '/signup/:token'
			,templateUrl: 'app/components/auth/signUp/signUpView.html'
			,data : { title: 'Signup' }
		});
		$stateProvider.state('auth.signUpJoin', {
			 url: '/join/:token'
			,templateUrl: 'app/components/auth/signUp/signUpView.html'
			,data : { title: 'Signup' }
		});
		$stateProvider.state('auth.forgotPass', {
			 url: '/forgot-password'
			,templateUrl: 'app/components/auth/forgotPass/forgotPassView.html'
			,data : { title: 'Forgot password' }
		});
		$stateProvider.state('auth.forgotPassToken', {
			 url: '/forgot-password/:token'
			,templateUrl: 'app/components/auth/forgotPass/forgotPassView.html'
			,data : { title: 'Forgot password' }
		});

		// App
		$stateProvider.state('app', {
			 templateUrl: 'app/components/appLayout/appView.html'
		});

		// Locations
		$stateProvider.state('locations', {
			 url: '/locs'
			,parent: 'app'
			,templateUrl: 'app/components/locationDashboard/locationDashboardView.html'
			,data : { title: 'Locations' }
		});
		$stateProvider.state('locationWithId', {
			 url: '/locs/:locId'
			,parent: 'app'
			,templateUrl: 'app/components/locationDashboard/locationDashboardView.html'
			,data : { title: 'Locations' }
		});
		$stateProvider.state('locationStats', {
			 url: '/locs/:locId/stats'
			,parent: 'app'
			,templateUrl: 'app/components/locationStats/locationStatsPageView.html'
			,data : { title: 'Statistics' }
		});
		$stateProvider.state('locationEdit', {
			 url: '/locs/:locId/edit'
			,parent: 'app'
			,templateUrl: 'app/components/locationEdit/locationEditView.html'
			,data : { title: 'Edit location' }
		});

		// Devices
		$stateProvider.state('deviceApps', {
			 url: '/locs/:locId/:deviceId/apps'
			,parent: 'app'
			,templateUrl: 'app/components/deviceApps/deviceAppsView.html'
			,data : { title: 'Apps' }
		});
		$stateProvider.state('deviceConfig', {
			 url: '/devices/:deviceId/config'
			,parent: 'app'
			,templateUrl: 'app/components/deviceConfig/deviceConfigView.html'
			,data : { title: 'Device configuration' }
		});

		$stateProvider.state('settings', {
			 url: '/settings'
			,parent: 'app'
			,templateUrl: 'app/components/userSettings/settingsView.html'
			,data : { title: 'Settings' }
		});

		// Company
		$stateProvider.state('company', {
			 url: '/company'
			,parent: 'app'
			,templateUrl: 'app/components/company/companyView.html'
			,data : { title: 'Company' }
		});

		// Admin
		$stateProvider.state('app.admin', {
			 url: '/admin'
			,parent: 'app'
			,templateUrl: 'app/components/admin/adminView.html'
			,data : { title: 'Admin' }
		});

		// Pages
		$stateProvider.state('page', {
			 templateUrl: 'app/components/textPages/textPageView.html'
		});
		$stateProvider.state('page.privacy', {
			 url: '/privacy'
			,templateUrl: 'app/components/textPages/privacy/privacyView.html'
			,data : { title: 'Privacy statement' }
		});

		// Default state
		$urlRouterProvider.when('', '/locs');
		$urlRouterProvider.when('/', '/locs');
		$urlRouterProvider.otherwise('/locs');
	})

	.run(function ($rootScope, $location, pagesService, pandoAuth){
		"ngInject";
		var m = 'routes';
		
		// Register listener to watch for route changes
		$rootScope.$on('$locationChangeStart', function (event, newUrl, oldUrl, newState, oldState) {
		    if($('.browsehappy').length) return event.preventDefault();
		    var url = $location.url();
			// if user loading usual page 
			// just continue to load
			// log.trace('location change start; oldUrl: ' + oldUrl + '; newUrl: ' + newUrl, m);
			// log.trace('location change start; oldState: ' + oldState + '; newState: ' + newState, m);
		    if(pagesService.isUsualPage(url)) return;
		    if(pandoAuth.currentUser){
				// if user logined and loading auth page -
				// redirect to root page
		        if(pagesService.isAuthPage(url)){
		        	pagesService.loadRootPage();
		        }
		    }else{
				// if user try to load not auth page - 
				// redirect to singin page
		        if(!pagesService.isAuthPage(url)){
		        	pagesService.loadSigninPage();
		        }
		    }
		});

	});