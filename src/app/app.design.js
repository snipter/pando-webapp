angular.module('app')
    .factory('appConfig', function(){
        "ngInject";
        
        return {
            color: {
                 primary:    '#0D38FD'
                ,success:    '#8BC34A'
                ,info:       '#00BCD4'
                ,infoAlt:    '#7E57C2'
                ,warning:    '#FFCA28'
                ,danger:     '#F44336'

                ,greenLight: '#00e3db'
                ,violet:     '#a92ff7'
                ,violetLight:'#bc6ef1'
                ,violetDark: '#9425ec'
                ,gray:       '#d5d5d5'
                ,grayLight:  '#9e9e9e'
                ,aquaMarine: '#4de2dc'
            }
        };
        
    })

    .config(function($mdThemingProvider) {
        "ngInject";
        var cyanAlt = $mdThemingProvider.extendPalette('cyan', {
            'contrastLightColors': '500 600 700 800 900',
            'contrastStrongLightColors': '500 600 700 800 900'
        });
        var lightGreenAlt = $mdThemingProvider.extendPalette('light-green', {
            'contrastLightColors': '500 600 700 800 900',
            'contrastStrongLightColors': '500 600 700 800 900'
        });
        var pandoPrimary = $mdThemingProvider.extendPalette('blue', {
              '900': '#0D38FD',
              '800': '#0D38FD',
              '700': '#0D38FD',
              '600': '#0D38FD',
              '500': '#0D38FD'
        });
        var pandoAccent = $mdThemingProvider.extendPalette('blue', {
              '900': '#4de2dc',
              '800': '#4de2dc',
              '700': '#4de2dc',
              '600': '#4de2dc',
              '500': '#4de2dc'
        });

        $mdThemingProvider
            .definePalette('cyanAlt', cyanAlt)
            .definePalette('lightGreenAlt', lightGreenAlt)
            .definePalette('pandoPrimary', pandoPrimary)
            .definePalette('pandoAccent', pandoAccent);

        $mdThemingProvider.theme('default')
            .primaryPalette('pandoPrimary', {
                'default': '800'
            })
            .accentPalette('pandoAccent', {
                'default': '800'
            })
            .warnPalette('red', {
                'default': '500'
            })
            .backgroundPalette('grey');
    });