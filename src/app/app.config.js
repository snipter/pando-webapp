angular.module('app')
    // Configuring API's root from local storage
    // Production: https://platform.getpando.com/
    // Testing: https://platform-testing.getpando.com/
    .config(function(pandoApiProvider, env){
        "ngInject";
        var m = 'config';
        // Setting api root from local storage
        var localApiRoot = window.localStorage.getItem('api.root');
        if(localApiRoot){
            log.warn('setting api root from local storage variable', m);
            pandoApiProvider.setConfig('apiRoot', localApiRoot);
        }
        // Setting api root from enviromoment variable
        else if(env.apiRoot){
            log.warn('setting api root from enviromoment variable', m);
            pandoApiProvider.setConfig('apiRoot', env.apiRoot);
        }
        // Using default api root
        else {
            log.debug('using default api root value', m);
        }
        // Output to console api root
        log.debug('api root: ' + pandoApiProvider.getConfig('apiRoot'), m);
    })

    // Chart configs
    .config(function (ChartJsProvider){
        "ngInject";
        // Configure all charts
        ChartJsProvider.setOptions({
             defaultFontColor: '#374a5c'
            ,defaultFontFamily: '"Futura", "Roboto", "Helvetica Neue", Arial, sans-serif'
            ,responsive: true
            ,maintainAspectRatio: false
            ,scales: {
                xAxes: [{
                    gridLines: {
                        color: "rgb(236,236,236)"
                    }
                    ,ticks: {
                        fontColor: "rgb(55,74,92)"
                    }
                }],
                yAxes: [{
                    gridLines: {
                        color: "rgb(236,236,236)"
                    }
                    ,ticks: {
                        fontColor: "rgb(55,74,92)"
                    }
                }]
            }
        });
    })

    // urlToImg sevice config
    .config(function(urlToImgProvider, env){
        "ngInject";
        urlToImgProvider.setCredentials(env.urlToImg);
    });