angular.module('app')
    .factory('UserParamsStorage', function(paramsStorage){
        "ngInject";

        var m = 'UserParamsStorage';

        function UserParamsStorage(user){
            if(!user) throw new Error('user not set');
            if(!user.id) throw new Error('user id not set');
            var self = this;
            self.id = user.id;
        }

        UserParamsStorage.prototype.set = function(key, val){
            key = this._userKey(key);
            paramsStorage.set(key, val);
        }

        UserParamsStorage.prototype.get = function(key){
            key = this._userKey(key);
            return paramsStorage.get(key);
        }

        UserParamsStorage.prototype.remove = function(key){
            key = this._userKey(key);
            paramsStorage.remove(key);
        }

        UserParamsStorage.prototype._userKey = function(key){
            var self = this;
            return key + ':' + self.id;
        }

        return UserParamsStorage;
        
        
    });