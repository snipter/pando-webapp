angular.module('app')
	.provider('urlToImg', function(){
		"ngInject";

		var provider = this;

		provider.cred = {};

		provider.setCredentials = function(cred){
			provider.cred = cred;
		}

		this.$get = function(){
			return function(url, width){
			    var apiUrl = 'https://api.screenshotlayer.com/api/capture';
			    var viewport = '1440x900';
			    var defWidth = 530;
			    if(!width || width <= 0) width = defWidth;
			    var imgSrc = apiUrl 
			               + '?access_key=' + provider.cred.token 
			               + '&viewport=' + viewport
			               + '&width=' + width
			               + '&secret_key=' + md5(url+provider.cred.secret)
			               + '&url=' + url;
			    return imgSrc;
			}
		}

	})
	
	.filter('urlToImg', function(urlToImg){
    	"ngInject";
	    return function(url, width) {
	        return urlToImg(url, width);
	    }
	});