angular.module('app')
    .service('dialogs', function($rootScope, $uibModal, $uibModalStack, $mdDialog){
        "ngInject";

        var m = 'dialogsService';

        var self = this;

        self.message = function(opt, cb){
            if(typeof opt === 'function'){cb = opt; opt = {};}

            var dialog = $mdDialog.confirm();
            if(opt.title) dialog.title(opt.title);
            if(opt.content) dialog.content(opt.content);
            if(opt.text) dialog.content(opt.text);
            if(opt.ok) dialog.ok(opt.ok);
            else dialog.ok('Ok');

            var dialogHandler = $mdDialog.show(dialog);
            if(cb) dialogHandler.then(cb);
        }

        self.confirm = function($event, opt, okCb, cancelCb){
            var dialog = $mdDialog.confirm();
            dialog.targetEvent($event);
            if(opt.title) dialog.title(opt.title);
            if(opt.content) dialog.content(opt.content);
            if(opt.text) dialog.content(opt.text);
            if(opt.ok) dialog.ok(opt.ok);
            else dialog.ok('Ok');
            if(opt.cancel) dialog.cancel(opt.cancel);
            else dialog.cancel('Cancel');
            $mdDialog.show(dialog).then(okCb, cancelCb);
        }

        self.prompt = function($event, opt, okCb, cancelCb){
            var dialog = $mdDialog.prompt();
            dialog.targetEvent($event);
            if(opt.title) dialog.title(opt.title);
            if(opt.textContent) dialog.textContent(opt.textContent);
            if(opt.content) dialog.textContent(opt.content);
            if(opt.placeholder) dialog.placeholder(opt.placeholder);
            if(opt.content) dialog.content(opt.content);
            if(opt.size) dialog.size(opt.size);
            if(opt.text) dialog.content(opt.text);
            if(opt.ok) dialog.ok(opt.ok);
            else dialog.ok('Ok');
            if(opt.cancel) dialog.cancel(opt.cancel);
            else dialog.cancel('Cancel');
            $mdDialog.show(dialog).then(okCb, cancelCb);
        }

        // ====================
        // Modal dialogs
        // ====================

        self.sizes = {
            small: 'sm',
            medium: 'md',
            large: 'lg'
        }

        self.modal = function(opt, okCb, cancelCb){
            if(opt.animation === undefined) opt.animation = true;
            if(opt.size === undefined) opt.size = 'md';
            var modal = $uibModal.open(opt);
            if((okCb !== undefined) || (cancelCb !== undefined)){
                modal.result.then(okCb, cancelCb);
            }
        }

        // ====================
        // Dialogs control
        // ====================

        self.dismissAll = function(){
            $uibModalStack.dismissAll('cancel');
        }

    });