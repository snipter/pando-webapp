angular.module('app')
    .service('localNotifyCenter', function($rootScope, consts){
        "ngInject";

        /*============ Apps ============*/

        this.broadcastAppUpdated = function(appName, appData){
            return this.broadcast(consts.notify.apps.data.updated, {name: appName, data: appData});
        }

        this.onAppUpdated = function(handler){
            return this.on(consts.notify.apps.data.updated, function(event, eventData){
                handler(eventData.name, eventData.data);
            });
        }

        this.broadcastAppActionAdd = function(appName){
            return this.broadcast(consts.notify.apps.action.add, {name: appName});
        }

        this.onAppActionAdd = function(handler){
            return this.on(consts.notify.apps.action.add, function(event, eventData){
                handler(eventData.name);
            });
        }

        this.broadcastAppItemClicked = function(appName, itemData){
            return this.broadcast(consts.notify.apps.action.item.clicked, {name: appName, data: itemData});
        }

        this.onAppItemClicked = function(handler){
            return this.on(consts.notify.apps.action.item.clicked, function(event, eventData){
                handler(eventData.name, eventData.data);
            });
        }

        this.broadcastAppItemAdd = function(appName, itemData){
            return this.broadcast(consts.notify.apps.item.add, {name: appName, data: itemData});
        }

        this.onAppItemAdd = function(handler){
            return this.on(consts.notify.apps.item.add, function(event, eventData){
                handler(eventData.name, eventData.data);
            });
        }

        this.broadcastAppItemsAdd = function(appName, itemsData){
            return this.broadcast(consts.notify.apps.items.add, {name: appName, data: itemsData});
        }

        this.onAppItemsAdd = function(handler){
            return this.on(consts.notify.apps.items.add, function(event, eventData){
                handler(eventData.name, eventData.data);
            });
        }

        this.broadcastAppItemsUpdated = function(appName){
            return this.broadcast(consts.notify.apps.items.updated, {name: appName});
        }

        this.onAppItemsUpdated = function(handler){
            return this.on(consts.notify.apps.items.updated, function(event, eventData){
                handler(eventData.name);
            });
        }

        /*============ Base funcitonality ============*/

        this.broadcast = function(event, data){
            return $rootScope.$broadcast(event, data);
        }

        this.on = function(event, handler){
            return $rootScope.$on(event, handler);
        }
    });