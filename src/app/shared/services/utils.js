angular.module('app')
    .service('utils', function(){
        "ngInject";

        var utils = this;
        
        utils.isArr = function(obj){
            return Object.prototype.toString.call( obj ) === '[object Array]';
        }

        utils.isStr = function(obj){
            return typeof obj === 'string';
        }

        utils.isDate = function(obj){
            return obj instanceof Date;
        }

        utils.isUrl = function(str){
            var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            return regexp.test(str); 
        }

        utils.arrToStr = function(arr){
            return _.reduce(arr, function(memo, item){
                if(memo == '') return item.toString();
                else return memo + ',' + item.toString();
            }, '');
        }

        utils.checkNested = function(obj /*, level1, level2, ... levelN*/) {
            var args = Array.prototype.slice.call(arguments, 1);
            for (var i = 0; i < args.length; i++) {
                if (!obj || !obj.hasOwnProperty(args[i])) {
                    return false;
                }
                obj = obj[args[i]];
            }
            return true;
        }

        utils.clone = function(obj){
            var self = this;
            if(obj === null || typeof(obj) !== 'object' || 'isActiveClone' in obj)
                return obj;
            var temp = obj.constructor(); // changed
            for(var key in obj) {
                if(Object.prototype.hasOwnProperty.call(obj, key)) {
                    obj['isActiveClone'] = null;
                    temp[key] = self.clone(obj[key]);
                    delete obj['isActiveClone'];
                }
            }    
            return temp;
        }

        utils.genId = function(){
            return '_' + Math.random().toString(36).substr(2, 9);
        }

        utils.pad = function(str, max) {
            str = str.toString();
            return str.length < max ? this.pad("0" + str, max) : str;
        }

        utils.randomIntArr = function(min, max, count) {
            var arr = [];
            for(var i = 0; i < count; i++ ){
                arr.push(this.randomInt(min, max));
            }
            return arr;
        }

        utils.random = function(min, max) {
            return Math.random() * (max - min) + min;
        }

        utils.randomInt = function(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        utils.extractInt = function(str){
            var re = /\d+/g;
            var match = re.exec(str);
            if(!match) return null;
            return parseInt(match[0]);
        }

        utils.maxAxes = function(arr){
            var max = _.max(arr);
            var q = 0;
            var y = 0;
            if(max < 100000) q = 10000;
            if(max < 10000) q = 1000;
            if(max < 1000) q = 100;
            if(max < 100) q = 50;
            if(max < 50) q = 10;
            while( y < max ) y += q;
            return y;
        }

        utils.getQueryParams = function(){
            var query_string = {};
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                // If first entry with this name
                if (typeof query_string[pair[0]] === "undefined") {
                    query_string[pair[0]] = decodeURIComponent(pair[1]);
                // If second entry with this name
                } else if (typeof query_string[pair[0]] === "string") {
                    var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
                    query_string[pair[0]] = arr;
                // If third or later entry with this name
                } else {
                    query_string[pair[0]].push(decodeURIComponent(pair[1]));
                }
            } 
            return query_string;
        }

        utils.lastVersion = function(versionArr){
            versionArr = _.uniq(versionArr);
            versionArr = _.sortBy(versionArr, function(item){ return item; });
            return _.last(versionArr);
        }

        utils.increseVersion = function(versionStr){
            var version = this.parseVersion(versionStr);
            if(!version) return null;
            version.build += 1;
            return this.versionObjToStr(version);
        }

        utils.parseVersion = function(versionStr){
            var regFull = /(\d+)\.(\d+)\.(\d+)/g;
            var match = regFull.exec(versionStr);
            if(match) return {major: parseInt(match[1]), minor: parseInt(match[2]), build: parseInt(match[3])};
            var regShort = /(\d+)\.(\d+)/g;
            match = regShort.exec(versionStr);
            if(match) return {major: parseInt(match[1]), minor: parseInt(match[2]), build: 0};
            else return null;
        }

        utils.versionObjToStr = function(obj){
            if(!obj) return '';
            return obj.major + '.' + obj.minor + '.' + obj.build;
        }

        utils.clearAngularObj = function(obj){
            if(!obj) return null;
            if(obj.$$hashKey) delete obj.$$hashKey;
            return obj;
        }

        utils.second = 1000;
        utils.minute = utils.second * 60;
        utils.hour = utils.minute * 60;
        utils.day = utils.hour * 24;
        utils.week = utils.day * 7;
        utils.month = utils.day * 30;
    });