angular.module('app')
    .service('mainLoader', function($rootScope){
        "ngInject";
        
        var m = 'mainLoader';

        var c = 1;

        this.show = function() {
            c++;
            if(c > 1) return;
            $('#loader-container').fadeIn("slow")
        }
        this.hide = function() {
            if(c <= 0){c = 0; return;}
            c--;
            if(c == 0) $('#loader-container').fadeOut("slow")
        }

    });