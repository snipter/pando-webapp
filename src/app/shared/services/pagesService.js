angular.module('app')
    .service('pagesService', function($location, $state, $document, consts){
        "ngInject";
        
        var m = 'pagesService';
        var self = this;

        /*============ Loading pages ============*/

        this.loadSigninPage = function (){
            log.debug('loading signin page', m);
            $state.go('auth.signIn');
        }

        this.loadRootPage = function(){
            log.debug('loading root page', m);
            $state.go('locations');
        }

        /*============ Pages Access ============*/

        this.isUrlRequiredSignIn = function(url){
            if(url == '/') return true;
            if(self.isUsualPage(url)) return false;
            if(self.isAuthPage(url)) return false;
            return true;
        }

        this.isAuthPage = function(url){
            var authPagesList = [
                 '/signin'
                ,'/signup'
                ,'/forgot-password'
                ,'/join'
                ,'/resetpassword'
            ];
            return isPageUrlInList(url, authPagesList);
        }

        this.isUsualPage = function(url){
            var usualPagesList = [
                 '/support'
                ,'/privacy'
            ]
            return isPageUrlInList(url, usualPagesList);
        }

        function isPageUrlInList(url, urlList){
            var founedItem = _.find(urlList, function(urlItem){
                return url.indexOf(urlItem) == 0; 
            });
            return founedItem ? true : false;
        }

        /*============ Scroll ============*/

        this.scrollToTop = function(){
            $document.scrollTo(0, 0);
        }
        
    });