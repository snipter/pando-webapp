angular.module('app')
    .service('toastMsg', function(){
        "ngInject";

        var self = this;

        var m = 'notificationsService';

        /*============ Toast ============*/

        toastr.options = {
            "closeButton": true,
            "positionClass": "toast-bottom-right",
            "timeOut": "3000"
        };

        var logger = {
            log: function(message) {
                logIt(message, 'info');
            },
            logWarning: function(message) {
                logIt(message, 'warning');
            },
            logSuccess: function(message) {
                logIt(message, 'success');
            },
            logError: function(message) {
                logIt(message, 'error');
            }
        };

        function logIt(message, type) {
            return toastr[type](message);
        };

        /*============ Notifications ============*/

        this.dataSaved = function(){
            this.success('Data saved successfully!');
        }

        this.dataSavingErr = function(){
            this.error('Data saving error.');
        }

        this.dataLoadingErr = function(){
            this.error('Data loading error.');
        }

        this.info = function(msg){
            return logger.log(msg);
        }

        this.success = function(msg){
            return logger.logSuccess(msg);
        }

        this.warning = function(msg){
            return logger.logWarning(msg);
        }

        this.error = function(msg){
            return logger.logError(msg);
        }

        this.err = function(msg){
            return logger.logError(msg);
        }

    });