angular.module('app')
	.directive('appsWeightControl', function($document){
		"ngInject";
		return {
			restrict: 'E',
			scope: {model:'=', property: '@', step: '@', total: '@', images: '@', change: '&'},
			template:   '<div class="apps-weight-control">' +
							'<div class="apps-weight-control__bar-wrap">' +
								'<div class="apps-weight-control__bar"></div>' +
							'</div>' +
						'</div>',
			replace: true,
			link: function($scope, element, attrs, ctrl){

				var m = 'appsWeightControl';

				// set to true for debug
				var debug = false;

				var barElement = angular.element(element[0].getElementsByClassName('apps-weight-control__bar'));
				barElement.css('position', 'relative');

				var pTotal = 100;
				var images = [];

				logDebug('init');

				function step(){
					return $scope.step ? parseFloat($scope.step) : 0;
				}

				function getTotal(){
					return $scope.total ? parseFloat($scope.total) : 100;
				}

				/*============ Reset ============*/

				function initControl(){
					resetControl();
					initWeights();
				}

				function resetControl(){
					logDebug('reset control', m);
					handles = [];
					handlesVal = [];
					infobars = [];
					pTotal = getTotal();
					$(barElement).empty();
				}

				/*============ Weights ============*/

				function getModelWeights(){
					return _.map($scope.model, function(item){ 
						var val = $scope.property ? item[$scope.property] : item;
						return val ? val : 1;
					});
				}

				function setModelWeights(weights){
					_.each(weights, function(weight, index){
						if($scope.property) $scope.model[index][$scope.property] = weight;
						else $scope.model[index] = weight;
					});
				}

				function initWeights(){
					logDebug('init weights');
					resetControl();
					initHandles(getModelWeights());
				}

				/*============ Handles ============*/

				var handles = [];
				var handlesVal = [];

				function initHandles(weights){
					logDebug('init handlers');
					var vals = weightsToHandlesVals(weights);
					if(vals.length){
						_.each(vals, function(val){
							addHanle(val);
						});
						updateHandlesPositions();
					}
					initInfobars();
				}

				// converting weights to handler's procents
				function weightsToHandlesVals(weights){
					var sum = _.reduce(weights, function(memo, num){return memo + num; }, 0);
					var dp = pTotal / sum;
					var vals = [];
					for(var i = 0; i < weights.length - 1; i++){
						if(i == 0) vals.push(weights[i] * dp);
						else vals.push(weights[i] * dp + vals[i - 1]);
					}
					return vals;
				}

				// convertion handler's procents to weights
				function handlesValsToWeights(handlesVal){
					var weights = [];
					_.each(handlesVal, function(val, index){
						if(index == 0) return weights.push(val);
						else return weights.push(val - handlesVal[index - 1]);
					});
					weights.push(pTotal - _.last(handlesVal));
					return weights;
				}

				function addHanle(handleVal){
					var handle = angular.element('<div class="apps-weight-control__handle-wrap"><div class="apps-weight-control__handle"></div></div>');
					handles.push(handle);
					handlesVal.push(handleVal);
					barElement.append(handle);

					var index = handles.length - 1;
					var startX = 0;
					var startP = 0;

					handle.on("mousedown", function(event){
						// console.log('apps-weight: mousedown index: ' + index);
						var mouseMove = function (event){
							var dp = (event.screenX - startX) / barElement.prop("clientWidth") * pTotal;
							var newVal = startP + dp;
							// console.log('apps-weight: dp: ' + dp);
							var bounds = handleBounds(index);
							// console.log('apps-weight: bounds: ' + JSON.stringify(bounds));
							if(newVal > bounds.max){
								setHandleVal(index, bounds.max);
							}else if(newVal < bounds.min){
								setHandleVal(index, bounds.min);
							}else{
								setHandleVal(index, newVal);
							}
							// // console.log('apps-weight: weights: ' + JSON.stringify(handlesValsToWeights(handlesVal)));
							updateHandlesPositions(); 
							updateInfobars();
						}
						var mouseUp = function(event){
							// console.log('apps-weight: mouseup: ' + index);
							$document.unbind("mousemove", mouseMove);
							$document.unbind("mouseup", mouseUp);
							setModelWeights(handlesValsToWeights(handlesVal));
							if($scope.change) $scope.change();
						}

						event.preventDefault();
						startX = event.screenX;
						startP = getHandleVal(index);

						// console.log('apps-weight: startP: ' + startP);

						$document.on("mousemove", mouseMove);
						$document.on("mouseup", mouseUp);
					});
				}

				function getHandleVal(index){
					return handlesVal[index];
				}

				function setHandleVal(index, p){
					var s = step();
					if (s > 0) p = Math.round(p/s) * s;
					handlesVal[index] = p;
				}

				function handleBounds(index){
					var handleMargin = 1;
					if(!handles.length || handles.length == 1) return {min: 0, max: pTotal};
					if(index == 0){
						return {min: 0 + handleMargin, max: handlesVal[index + 1] - handleMargin};
					}else if(index == (handles.length - 1)){
						return {min: handlesVal[index - 1] + handleMargin, max: pTotal - handleMargin};
					}else{
						return {min: handlesVal[index - 1] + handleMargin, max: handlesVal[index + 1] - handleMargin};
					}
				}

				function updateHandlesPositions(){
					for(var i = 0; i < handles.length; i++){
						var handle = handles[i];
						var p = getHandleVal(i);
						var x = p/pTotal * 100;
						var left = x + '%';
						handle.css({left: left});
					}
				}

				/*============ Infobars ============*/

				var infobars = [];

				function initInfobars(){
					logDebug('init infobars');
					for(var i = 0; i <= handlesVal.length; i++){
						var html = '<div class="apps-weight-control__procents">0%</div>';
						if(images.length){
							var imgSrc = images[i];
							if(imgSrc){
								html = '<div class="apps-weight-control__icon"><img src="' + imgSrc + '" /></div>' + html;
							}
						}
						html = '<div>' + html + '</div>';
						html = '<div class="apps-weight-control__infobar">' + html + '</div>';
						var infobar = angular.element(html);
						barElement.append(infobar);
						infobars.push(infobar);
					}
					updateInfobars();
				}

				function updateInfobars(){
					var opacityStart = 0.4;
					var opacityEnd = 1.0;
					for(var i = 0; i < infobars.length; i++){
						var left = 0;
						var width = 0;
						var bar = infobars[i];
						if(!handlesVal.length && (i == 0)){
							left = 0;
							width = 100;
						}else if(i == 0){
							left = 0;
							width = handlesVal[0];
						}else if(i == infobars.length - 1){
							left = handlesVal[i - 1];
							width = pTotal - left;
						}else{
							left = handlesVal[i - 1];
							width = handlesVal[i] - handlesVal[i - 1];
						}
						var opacity = opacityStart + ((opacityEnd - opacityStart) * width / 100);
						$('.apps-weight-control__procents', bar).text(Math.round(width) + '%');
						bar.css('margin-left', left + '%');
						bar.css('width', width + '%');
						bar.css('background-color', 'rgba(77, 226, 220, ' + opacity + ')');
					}
				}

				/*============ Events ============*/

				$scope.$watch('model', function(newVal, oldVal){
					logDebug('model updated');
					if(newVal && newVal.length){
						initControl();
					}else{
						resetControl();
					}
				});

				$scope.$watch('images', function(newVal, oldVal){
					logDebug('images updated');
					if(newVal){
						try{
							images = JSON.parse(newVal);
						}catch(err){
							log.err(err, m);
							images = [];
						}
						initControl();
					}else{
						images = [];
					}
				});

				$scope.$watch('total', function(newVal, oldVal){
					logDebug('total updated');
					if(newVal){
						initControl();
					}else{
						pTotal = 100;
						initControl();
					}
				});

				/*============ Log ============*/

				function logDebug(data){
					if(!debug) return;
					log.debug(data, m);
				}

				function logTrace(data){
					if(!debug) return;
					log.trace(data, m);
				}

			}
		}
	});