angular.module('app')
	.directive('usersEditForm', function(){
		return {
			restrict: 'E',
			scope: {user:'=', valid:'=', hidePassword:'@', disabled:'@'},
			templateUrl: 'app/shared/usersEditForm/usersEditFormView.html',
			replace: true,
			link: function($scope, element, attrs, ctrl){
				$scope.$watch('usersEditForm.$valid', function(newVal, oldVal){
					$scope.valid = newVal;
				});
			}
		}
	});