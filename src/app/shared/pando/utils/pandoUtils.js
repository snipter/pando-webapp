angular.module('pando.utils',[])
	.service('pandoUtils', function(){
		"ngInject";
		// ====================
		// Objects
		// ====================

		// deep cloning of object

		function clone(item) {
		    if (!item) { return item; } // null, undefined values check

		    var types = [ Number, String, Boolean ], 
		        result;

		    // normalizing primitives if someone did new String('aaa'), or new Number('444');
		    types.forEach(function(type) {
		        if (item instanceof type) {
		            result = type( item );
		        }
		    });

		    if (typeof result == "undefined") {
		        if (Object.prototype.toString.call( item ) === "[object Array]") {
		            result = [];
		            item.forEach(function(child, index, array) { 
		                result[index] = clone( child );
		            });
		        } else if (typeof item == "object") {
		            // testing that this is DOM
		            if (item.nodeType && typeof item.cloneNode == "function") {
		                var result = item.cloneNode( true );    
		            } else if (!item.prototype) { // check that this is a literal
		                if (item instanceof Date) {
		                    result = new Date(item);
		                } else {
		                    // it is an object literal
		                    result = {};
		                    for (var i in item) {
		                        result[i] = clone( item[i] );
		                    }
		                }
		            } else {
		                // depending what you would like here,
		                // just keep the reference, or create new object
		                if (false && item.constructor) {
		                    // would not advice to do that, reason? Read below
		                    result = new item.constructor();
		                } else {
		                    result = item;
		                }
		            }
		        } else {
		            result = item;
		        }
		    }

		    return result;
		}

		this.clone = clone;

		// ====================
		// Random
		// ====================

		this.randomId = function(idLength){
			if(!idLength) idLength = 10;
			return Math.random().toString(36).substr(2, idLength);
		}

		this.random = function(min, max) {
		    return Math.random() * (max - min) + min;
		}

		this.randomInt = function(min, max) {
			if(min === 'undefined') min = 0;
			if(max === 'undefined') max = 1000;
		    return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		// ====================
		// Date / Time
		// ====================

		this.second = 1000;
		this.minute = this.second * 60;
		this.hour = this.minute * 60;
		this.day = this.hour * 24;
		this.week = this.day * 7;
		this.month = this.day * 30;
		this.year = this.day * 365;

	});