angular.module('pando.api', [])
    .provider('pandoApi', function(){
        var prov = this;

        prov.config = {
            apiRoot: 'https://platform.getpando.com/'
        }

        prov.setConfig = function(name, val){
            prov.config[name] = val;
        }

        prov.getConfig = function(name){
            return prov.config[name];
        }

        function apiServise($rootScope, $http, PandoErr){
            "ngInject";
                    
            var self = this;
            var m = 'pandoApi';

            /*============ Token ============*/

            self.accessToken = null;

            self.setAccessToken = function(accessToken){
                log.debug('setting access token: ' + accessToken, m);
                if(accessToken){
                    self.accessToken = accessToken;
                    $http.defaults.headers.common['X-Pando-Authentication'] = accessToken;
                }
            }

            self.getAccessToken = function(token){
                return self.accessToken;
            }

            self.removeAccessToken = function(){
                log.debug('removing acces token', m);
                if($http.defaults.headers.common['X-Pando-Authentication']){
                    delete $http.defaults.headers.common['X-Pando-Authentication'];
                    self.accessToken = null;
                }
                log.debug('removing acces token done', m);
            }

            /*============ Events ============*/

            self.observers = [];

            self.on = function(name, handler){
                self.observers.push({name: name, handler: handler});
            }

            self.emit = function(name, data){
                _.each(self.observers, function(obs){
                    if(obs.name == name) obs.handler(data);
                });
            }

            self.notify = {
                req: {
                    begin: m + ":req:begin",
                    end: m + ":req:end",
                    success: m + ":req:success",
                    error: m + ":req:error"
                }
            }

            /*============ Supported Hardware Types ============*/

            self.hardwareTypes = {
                raspberryPi2: {name: 'Raspberry Pi 2 Model B', val: 'Raspberry_2B'},
                raspberryPi3: {name: 'Raspberry Pi 3 Model B', val: 'Raspberry_3B'}
            }

            /*============ Supported Visit profiles ============*/

            self.visitProfiles = {
                defalut: {name: 'Default', val: 'default'},
                terminal: {name: 'Terminal', val: 'terminal'}
            }

            /*============ Supported subscription plans ============*/

            self.subscriptionPlans = {
                pilot: { name: 'Pilot Plan', price: 0.0, planId: 'pilot'},
                standart: { name: 'Standart Monthly Plan', price: 79.00, planId: 'fsmm'}
            }

            /*============ Auth ============*/

            self.modificateAccessTokenParam = function(data){
                if(data && data["X-Pando-Authentication"]){
                    data.accessToken = data["X-Pando-Authentication"];
                    delete data["X-Pando-Authentication"];
                }
                return data;
            }

            self.auth = {};

            // http://docs.vehicle.apiary.io/#reference/users-signup/register-user
            self.auth.signUp = function(data, cb){
                self.apiReq('POST', 'signup/', data, function(err, data){
                    if(err) return cb(err);
                    data = self.modificateAccessTokenParam(data);
                    return cb(null, data);
                });
            }

            // http://docs.vehicle.apiary.io/#reference/users-signup/join-a-company/register-user-and-join-a-company
            self.auth.signUpAndJoinCompany = function(data, cb){
                self.apiReq('POST', 'signup/join/', data, function(err, data){
                    if(err) return cb(err);
                    data = self.modificateAccessTokenParam(data);
                    return cb(null, data);
                });
            }

            // http://docs.vehicle.apiary.io/#reference/authentication-and-authorization/authentication-tokens/authenticate-user
            self.auth.logIn = function(email, password, cb){
                var data = {email: email, password: password};
                self.apiReq('POST', 'auth/', data, function(err, data){
                    if(err) return cb(err);
                    data = self.modificateAccessTokenParam(data);
                    return cb(null, data);
                });
            }

            // http://docs.vehicle.apiary.io/#reference/authentication-and-authorization/authentication-tokens/logout
            self.auth.logOut = function(cb){
                self.apiReq('DELETE', 'auth/', {}, function(err){
                    if(err) return cb(err);
                    return cb(null);
                });
            }

            // http://docs.vehicle.apiary.io/#reference/authentication-and-authorization/authentication-tokens/refresh-token
            self.auth.refreshToken = function(cb){
                self.apiReq('GET', 'auth/', function(err, data){
                    if(err) return cb(err);
                    data = self.modificateAccessTokenParam(data);
                    return cb(null, data);
                });
            }

            self.auth.pass = {};

            // http://docs.vehicle.apiary.io/#reference/authentication-and-authorization/authentication-tokens/change-password
            self.auth.pass.change = function(password, cb){
                self.apiReq('PUT', 'auth/', {password: password}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/authentication-and-authorization/password-restore/restore-password-request
            self.auth.pass.reset = function(email, cb){
                self.apiReq('POST', 'auth/restore/', {email: email}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/authentication-and-authorization/password-restore/restore-password-action
            self.auth.pass.restore = function(emailToken, password, cb){
                self.apiReq('PUT', 'auth/restore/', {emailToken: emailToken, password: password}, function(err, data){
                    if(err) return cb(err);
                    data = self.modificateAccessTokenParam(data);
                    return cb(null, data);
                });
            }

            /*============ Users ============*/

            self.users = {};

            // http://docs.vehicle.apiary.io/#reference/users-and-permissions/users-collection/list-all-users
            self.users.list = function(cb){
                self.apiReq('GET', 'rest/user/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/users-and-permissions/users-collection/add-user
            self.users.add = function(data, cb){
                self.apiReq('POST', 'rest/user/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/users-and-permissions/access-specified-user/retrieve-a-user-entry
            self.users.get = function(userId, cb){
                self.apiReq('GET', 'rest/user/'+userId+'/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/users-and-permissions/access-specified-user/update-a-user-entry
            self.users.update = function(userId, data, cb){
                self.apiReq('PUT', 'rest/user/'+userId+'/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/users-and-permissions/access-specified-user/remove-a-user-from-company
            self.users.remove = function(userId, cb){
                self.apiReq('DELETE', 'rest/user/'+userId+'/', {}, cb);
            }

            /*============ Locations ============*/

            self.locations = {};

            // http://docs.vehicle.apiary.io/#reference/users-and-permissions/access-specified-user/add-new-location
            self.locations.add = function(data, cb){
                self.apiReq('POST', 'rest/location/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/locations/locations-collection/get-list-of-locations
            self.locations.list = function(cb){
                self.apiReq('GET', 'rest/location/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/locations/access-specified-location/retrieve-specified-location
            self.locations.get = function(id, cb){
                self.apiReq('GET', 'rest/location/' + id + '/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/locations/access-specified-location/update-a-location
            self.locations.update = function(id, data, cb){
                self.apiReq('PUT', 'rest/location/' + id + '/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/locations/access-specified-location/delete-a-location-entry
            self.locations.remove = function(id, cb){
                self.apiReq('DELETE', 'rest/location/' + id + '/', {}, cb);
            }

            self.locations.realtime = {};

            // http://docs.vehicle.apiary.io/#reference/device-maintenance/device-restart/retrieve-location-realtime-data
            self.locations.realtime.get = function(locationId, data, cb){
                self.apiReq('GET', 'rest/statistics/' + locationId + '/', data, cb);
            }

            /*============ Devices ============*/

            self.devices = {};

            self.devices.permissions = {
                none: {name: 'None', val: 'NONE'},
                manager: {name: 'Manager', val: 'MANAGER'},
                analytics: {name: 'Analytics', val: 'ANALYTICS'},
                admin: {name: 'Admin', val: 'ADMIN'},
                owner: {name: 'Owner', val: 'OWNER'}
            }

            // http://docs.vehicle.apiary.io/#reference/devices/devices-collection/provision-new-device
            self.devices.add = function(data, cb){
                self.apiReq('POST', 'rest/device/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/devices/devices-collection/retrieve-all-devices
            self.devices.listAll = function(cb){
                self.apiReq('GET', 'rest/device/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/devices/devices-collection/retrieve-all-devices
            self.devices.list = function(cb){
                self.devices.listAll(function(err, devices){
                    if(err) return cb(err);
                    devices = _.filter(devices, function(item){
                        return !item.isDeleted;
                    });
                    return cb(null, devices);
                });
            }

            // http://docs.vehicle.apiary.io/#reference/devices/location-devices-collection/retrieve-all-location-devices
            self.devices.listAtLocation = function(locationId, cb){
                self.apiReq('GET', 'rest/device/', {locationId: locationId}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/devices/access-specified-device/retrieve-specified-device
            self.devices.get = function(deviceId, cb){
                self.apiReq('GET', 'rest/device/' +  deviceId + '/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/devices/access-specified-device/update-the-device
            self.devices.update = function(deviceId, data, cb){
                if(!data) throw new Error('data not set');
                if(!data.hardwareConfiguration) throw new Error('hardwareConfiguration not set');
                if(_.isString(data.hardwareConfiguration.rotation)){
                    data.hardwareConfiguration.rotation = parseInt(data.hardwareConfiguration.rotation);
                }
                if(_.isString(data.hardwareConfiguration.resolutionCode)){
                    data.hardwareConfiguration.resolutionCode = parseInt(data.hardwareConfiguration.resolutionCode);
                }
                self.apiReq('PUT', 'rest/device/' +  deviceId + '/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/devices/access-specified-device/deprovision-delete-the-device
            self.devices.remove = function(deviceId, cb){
                self.apiReq('DELETE', 'rest/device/' +  deviceId + '/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/device-maintenance/device-restart/process-device-restart
            self.devices.restart = function(deviceId, cb){
                self.apiReq('POST', 'rest/device/' +  deviceId + '/maintenance/', {}, cb);
            }

            self.devices.firmware = {};

            // http://docs.vehicle.apiary.io/#reference/device-maintenance/forced-firmware-update/process-firmware-update
            self.devices.firmware.update = function(deviceId, cb){
                self.apiReq('POST', 'rest/device/' +  deviceId + '/firmware/', {}, cb);
            }

            self.devices.realtime = {};

            // http://docs.vehicle.apiary.io/#reference/realtime-statistics/access-device-realtime-data/retrieve-device-realtime-data
            self.devices.realtime.get = function(locationId, deviceId, data, cb){
                self.apiReq('GET', 'rest/statistics/' + locationId + '/' +  deviceId + '/', data, cb);
            }

            self.devices.screenshots = {};

            // http://docs.vehicle.apiary.io/#reference/device-screenshots/request-specified-screenshot/request-a-screenshot
            self.devices.screenshots.get = function(deviceId, data, cb){
                self.apiReq('GET', '/rest/device/' + deviceId + '/screenshot/', data, cb);
            }

            self.devices.logs = {};

            /*============ Apps ============*/

            self.apps = {};

            self.apps.avaliable = {
                graphics: {name: 'Graphics', val: 'graphics', desc: 'Upload and display any graphic'},
                twitter: {name: 'Twitter', val: 'twitter', desc: 'Display any social feed or hashtag'},
                webview: {name: 'Web View', val: 'webview', desc: 'Display any website'}
            };

            self.apps.baseGraphicItemData = function(){
                return {
                    src: null,
                    size: "cover",
                    order: 1,
                    duration: 5,
                    group: true,
                    groupId: 4,
                    created: new Date(),
                    schedule: {
                        days: {
                            mon: true,
                            tue: true,
                            wed: true,
                            thu: true,
                            fri: true,
                            sat: true,
                            sun: true
                        },
                        start: null,
                        stop: null,
                        timeOfDay: []
                    }
                }
            }

            // http://docs.vehicle.apiary.io/#reference/applications/installed-applications-collection/retrieve-all-apps-installed-to-the-device
            self.apps.list = function(deviceId, cb){
                self.apiReq('GET', 'rest/device/' +  deviceId + '/apps/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/applications/installed-applications-collection/install-application-to-the-device
            // active [true|false] - set app to active after installation
            self.apps.install = function(deviceId, appName, data, cb){
                if(typeof data === 'function'){cb = data; data = {}};
                var reqUrl = 'rest/device/' +  deviceId + '/apps/';
                if(data.active === false){
                    reqUrl += '?active=false';
                }
                self.apiReq('POST', reqUrl, {name: appName}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/applications/installed-applications-collection/update-application-dashboard
            self.apps.updateDashboard = function(deviceId, data, cb){
                self.apiReq('PUT', 'rest/device/' +  deviceId + '/apps/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/applications/access-specified-application/retrieve-application-entry
            self.apps.get = function(deviceId, appId, cb){
                self.apiReq('GET', 'rest/device/' +  deviceId + '/apps/' + appId + '/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/applications/access-specified-application/update-application
            self.apps.update = function(deviceId, appId, data, cb){
                self.apiReq('PUT', 'rest/device/' +  deviceId + '/apps/' + appId + '/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/applications/access-specified-application/uninstall-application
            self.apps.remove = function(deviceId, appId, data, cb){
                self.apiReq('DELETE', 'rest/device/' +  deviceId + '/apps/' + appId + '/', data, cb);
            }

            self.apps.schedule = {};

            // http://docs.vehicle.apiary.io/#reference/applications/access-application-schedule/retrieve-application-schedule
            self.apps.schedule.get = function(deviceId, appId, cb){
                self.apiReq('GET', 'rest/device/' +  deviceId + '/apps/' + appId + '/schedule/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/applications/access-application-schedule/update-application-schedule
            self.apps.schedule.update = function(deviceId, appId, data, cb){
                self.apiReq('PUT', 'rest/device/' +  deviceId + '/apps/' + appId + '/schedule/', data, cb);
            }

            self.apps.params = {};

            // http://docs.vehicle.apiary.io/#reference/applications/access-application-parameters/retrieve-app-specific-parameters
            self.apps.params.get = function(deviceId, appId, cb){
                self.apiReq('GET', 'rest/device/' +  deviceId + '/apps/' + appId + '/parameters/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/applications/access-application-parameters/update-app-parameters
            self.apps.params.update = function(deviceId, appId, data, cb){
                self.apiReq('PUT', 'rest/device/' +  deviceId + '/apps/' + appId + '/parameters/', data, cb);
            }

            /*============ Retargeting ============*/

            self.rules = {};

            self.rules.eventTypes = {
                visit: {name: 'Visit', val: 'VISIT'},
                walkby: {name: 'Walkby', val: 'WALKBY'}
            }

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/retargeting-rules-collection/retrieve-collection-of-retargeting-rules-for-the-device
            self.rules.list = function(deviceId, cb){
                self.apiReq('GET', 'rest/device/'+deviceId+'/retarget/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/retargeting-rules-collection/add-retargeting-rule
            self.rules.add = function(deviceId, ruleData, cb){
                self.apiReq('POST', 'rest/device/'+deviceId+'/retarget/', ruleData, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/retargeting-rules-summary/retrieve-device-rules-summary-for-application
            self.rules.summary = function(deviceId, applicationId, cb){
                self.apiReq('GET', 'rest/device/'+deviceId+'/retarget/summary/'+applicationId+'/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/access-specified-retargeting-rule/retrieve-retargeting-rule
            self.rules.get = function(deviceId, ruleId, cb){
                self.apiReq('GET', 'rest/device/'+deviceId+'/retarget/' + ruleId + '/' , {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/access-specified-retargeting-rule/update-retargeting-rule
            self.rules.update = function(deviceId, ruleId, ruleData, cb){
                self.apiReq('PUT', 'rest/device/'+deviceId+'/retarget/' + ruleId + '/' , ruleData, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/access-specified-retargeting-rule/delete-retargeting-rule
            self.rules.remove = function(deviceId, ruleId, cb){
                self.apiReq('DELETE', 'rest/device/'+deviceId+'/retarget/' + ruleId + '/' , {}, cb);
            }

            self.rules.schedule = {};

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/access-rule-schedule/retrieve-rule-schedule
            self.rules.schedule.get = function(deviceId, ruleId, cb){
                self.apiReq('GET', 'rest/device/'+deviceId+'/retarget/' + ruleId + '/schedule/' , {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/access-rule-schedule/update-rule-schedule
            self.rules.schedule.update = function(deviceId, ruleId, scheduleData, cb){
                self.apiReq('PUT', 'rest/device/'+deviceId+'/retarget/' + ruleId + '/schedule/' , scheduleData, cb);
            }

            self.rules.params = {};

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/access-rule-application-parameters/retrieve-rule-app-parameters
            self.rules.params.get = function(deviceId, ruleId, cb){
                self.apiReq('GET', 'rest/device/'+deviceId+'/retarget/' + ruleId + '/parameters/' , {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/retargeting-rules/access-rule-application-parameters/update-rule-app-parameters
            self.rules.params.update = function(deviceId, ruleId, params, cb){
                self.apiReq('PUT', 'rest/device/'+deviceId+'/retarget/' + ruleId + '/parameters/' , params, cb);
            }

            /*============ Company ============*/

            self.company = {};

            self.company.permissions = {
                none: {name: 'None', val: 'NONE'},
                manager: {name: 'Manager', val: 'MANAGER'},
                analytics: {name: 'Analytics', val: 'ANALYTICS'},
                admin: {name: 'Admin', val: 'ADMIN'},
                owner: {name: 'Owner', val: 'OWNER'}
            };

            // http://docs.vehicle.apiary.io/#reference/company/company/add-company
            self.company.create = function(data, cb){
                self.apiReq('POST', 'rest/company/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/company/company/get-company-details
            self.company.get = function(cb){
                self.apiReq('GET', 'rest/company/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/company/company/update-company-details
            self.company.update = function(data, cb){
                self.apiReq('PUT', 'rest/company/', data, cb);
            }

            self.company.billing = {};

            // http://docs.vehicle.apiary.io/#reference/company/company-billing-details/get-billing-details
            self.company.billing.get = function(cb){
                self.apiReq('GET', 'rest/company/billing/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/company/company-billing-details/update-billing-details
            self.company.billing.update = function(data, cb){
                self.apiReq('PUT', 'rest/company/billing/', data, cb);
            }

            self.company.payment = {};

            // http://docs.vehicle.apiary.io/#reference/company/payment-method-verification/get-client-token
            self.company.payment.getAccessToken = function(cb){
                self.apiReq('GET', 'rest/company/billing/payment/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/company/payment-method-verification/submit-payment-method-nonce
            self.company.payment.submitPaymentNonce = function(nonce, cb){
                self.apiReq('POST', 'rest/company/billing/payment/', nonce, cb);
            }
            
            self.company.subscription = {};

            // http://docs.vehicle.apiary.io/#reference/company/subscription-management/get-subscription
            self.company.subscription.get = function(cb){
                self.apiReq('GET', 'rest/company/billing/subscription/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/company/subscription-management/start-subscription
            self.company.subscription.start = function(planId, cb){
                self.apiReq('POST', 'rest/company/billing/subscription/', {planId: planId}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/company/subscription-management/cancel-subscription
            self.company.subscription.cancel = function(data, cb){
                self.apiReq('DELETE', 'rest/company/billing/subscription/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/company/subscription-management/buy-additional-devices
            self.company.subscription.buyDevices = function(devicesCount, cb){
                self.apiReq('PUT', 'rest/company/billing/subscription/', {devicesCount: devicesCount}, cb);
            }

            self.company.integrations = {};

            // http://docs.vehicle.apiary.io/#reference/integrations/company-integrations-collection/active-integrations-collection
            self.company.integrations.list = function(cb){
                self.apiReq('GET', 'rest/company/integrations/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/integrations/access-specified-integration/get-status-of-integration
            self.company.integrations.get = function(providerName, cb){
                self.apiReq('GET', 'rest/company/integrations/' + providerName + '/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/integrations/access-specified-integration/initiate-adding-integration
            self.company.integrations.init = function(providerName, cb){
                self.apiReq('POST', 'rest/company/integrations/' + providerName + '/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/integrations/access-specified-integration/delete-specified-integration
            self.company.integrations.remove = function(providerName, cb){
                self.apiReq('DELETE', 'rest/company/integrations/' + providerName + '/', {}, cb);
            }

            self.company.integrations.statuses = {
                disabled: {name: 'Disabled', val: 'DISABLED'},
                enabled: {name: 'Enabled', val: 'ENABLED'}
            };

            /*============ Static Resources ============*/

            self.resources = {};

            // http://docs.vehicle.apiary.io/#reference/static-resources/upload-resource
            self.resources.upload = function(file, appName, onprogress, cb){
                var xhr = new XMLHttpRequest();
                var apiUrl = prov.config.apiRoot + 'rest/company/resources/' + appName + '/';
                var apiMethod = 'POST';
                var fileType = file.type;
                xhr.open(apiMethod, apiUrl, true);
                xhr.setRequestHeader('X-Pando-Authentication', self.getAccessToken());
                xhr.setRequestHeader('Content-type', file.type);
                xhr.upload.onprogress = onprogress;
                xhr.onload = function(){ 
                    $rootScope.$apply(function(){
                        if(!PandoErr.isCode200(xhr.status)) return cb({name: 'WRONG_STATUS', data: xhr.status});
                        try{
                            var data = JSON.parse(xhr.responseText);
                            return cb(null, data); 
                        }catch(e){
                            return cb({name: 'DATA_PARSING_ERROR', data: e});
                        }
                    });
                };
                xhr.send(file);
                log.trace('api req: ' + JSON.stringify({url: apiUrl, method: apiMethod, fileType: fileType}), m);
            }

            // http://docs.vehicle.apiary.io/#reference/static-resources/list-application-resources/list-resources
            self.resources.list = function(appName, data, cb){
                self.apiReq('GET', 'rest/company/resources/' + appName + '/', data, cb);
            }

            /*============ Analytics ============*/

            self.analytics = {};

            self.analytics.filters = {
                walkbys: 'walkbys',
                bounces: 'bounces',
                visits: 'visits',
                engaged: 'engaged',
                new: 'new',
                returning: 'returning',
                terminal: {
                     opportunity: 'terminal.opportunity',
                    engaged: 'terminal.engaged',
                    new: 'terminal.new',
                    loyal: 'terminal.loyal'
                }
            }

            self.prepareAnalyticsReqData = function(data){
                if(!data) return null;
                if(_.isArray(data)) return null;
                if(_.isNumber(data)) return null;
                if(data.startDate) data.startDate = self.prepareAnalyticsReqDateParam(data.startDate);
                if(data.endDate) data.endDate = self.prepareAnalyticsReqDateParam(data.endDate);
                if(data.device){
                    data.devices = data.device;
                    delete data.device;
                }
                if(data.devices){
                    if(_.isNumber(data.devices)){
                        data.devices = data.devices.toString();
                    }
                    if(_.isArray(data.devices)){
                        data.devices = _.reduce(data.devices, function(memo, item){
                            return memo ? memo+','+item.toString() : item.toString();
                        }, '');
                    }
                }
                if(data.location) data.location = data.location.toString();
                return data;
            }

            self.prepareAnalyticsReqDateParam = function(dateVal){
                if(typeof dateVal === 'string') return dateVal;
                if(dateVal instanceof Date){
                    var dateFormat = 'YYYY-MM-DDTHH:mm:ssZ';
                    return moment(dateVal).format(dateFormat);
                }
                return dateVal;
            }

            // WARNING - information not found

            // self.analyticsPandoIdEventsList = function(pandoId, data, cb){
            //     data = self.prepareAnalyticsReqData(data);
            //     self.apiReq('GET', 'rest/history/'+pandoId+'/', data, cb);
            // }

            // self.analyticsEventsList = function(filterName, data, cb){
            //     data = self.prepareAnalyticsReqData(data);
            //     self.apiReq('GET', 'rest/analytics/'+filterName+'/', data, cb);
            // }

            // self.analyticsEventsCount = function(filterName, resolutionName, data, cb){
            //     data = self.prepareAnalyticsReqData(data);
            //     self.apiReq('GET', 'rest/analytics/'+filterName+'/'+resolutionName+'/count/', data, cb);
            // }

            // self.analyticsEventsDuration = function(filterName, resolutionName, data, cb){
            //     data = self.prepareAnalyticsReqData(data);
            //     self.apiReq('GET', 'rest/analytics/'+filterName+'/'+resolutionName+'/duration/', data, cb);
            // }

            // self.analyticsRatesConversion = function(resolutionName, data, cb){
            //     data = self.prepareAnalyticsReqData(data);
            //     self.apiReq('GET', 'rest/analytics/rates/'+resolutionName+'/conversion/', data, cb);
            // }

            // this.analyticsRatesNew = function(filterName, resolutionName, data, cb){
            //     data = self.prepareAnalyticsReqData(data);
            //     self.apiReq('GET', 'rest/analytics/rates/'+resolutionName+'/new/'+filterName+'/', data, cb);
            // }

            // self.analytics = {
            //     ,resolutions: {
            //          hourOfDay: 'hourOfDay'
            //         ,hour: 'hourOfDay'
            //         ,dayOfWeek: 'dayOfWeek'
            //         ,dayOfMonth: 'dayOfMonth'
            //         ,calendarDay: 'calendarDay'
            //         ,monthOfTheYear: 'monthOfTheYear'
            //         ,month: 'monthOfTheYear'
            //     }
            //     ,pandoId: {
            //         events: {
            //             list: self.analyticsPandoIdEventsList
            //         }
            //     }
            //     ,events: {
            //          list: self.analyticsEventsList
            //         ,count: self.analyticsEventsCount
            //         ,duration: self.analyticsEventsDuration
            //     }
            //     ,rates: {
            //          conversion: self.analyticsRatesConversion
            //         ,new: self.analyticsRatesNew
            //     }
            // }

            /*============ History ============*/

            self.history = {};

            self.history.pandoId = {};

            // http://docs.vehicle.apiary.io/#reference/history/history-for-a-single-pando-id/list-raw-analytics
            self.history.pandoId.rawAnalytics = function(locationId, pandoId, data, cb){
                if(typeof data === 'function'){cb = data; data = pandoId; pandoId = locationId; locationId = null;}
                data = self.prepareAnalyticsReqData(data);
                if(locationId) self.apiReq('GET', 'rest/history/'+locationId+'/'+pandoId+'/', data, cb);
                else self.apiReq('GET', 'rest/history/'+pandoId+'/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/history/history-for-visits-of-a-single-pando-id/list-raw-analytics
            self.history.pandoId.visitsAnalytics = function(locationId, pandoId, data, cb){
                if(typeof data === 'function'){cb = data; data = pandoId; pandoId = locationId; locationId = null;}
                data = self.prepareAnalyticsReqData(data);
                if(locationId) self.apiReq('GET', 'rest/history/'+locationId+'/'+pandoId+'/visits/', data, cb);
                else self.apiReq('GET', 'rest/history/'+pandoId+'/visits/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/history/device-level-history-for-visits-of-a-single-pando-id/list-raw-analytics
            self.history.pandoId.deviceLevelVisitsAnalytics = function(locationId, pandoId, data, cb){
                if(typeof data === 'function'){cb = data; data = pandoId; pandoId = locationId; locationId = null;}
                data = self.prepareAnalyticsReqData(data);
                if(locationId) self.apiReq('GET', 'rest/history/'+locationId+'/'+pandoId+'/visits/devices/', data, cb);
                else self.apiReq('GET', 'rest/history/'+pandoId+'/visits/devices/', data, cb);
            }

            self.history.events = {};

            // http://docs.vehicle.apiary.io/#reference/history/history-of-a-particular-label/list-matching-events
            self.history.events.list = function(locationId, label, data, cb){
                if(typeof data === 'function'){cb = data; data = label; label = locationId; locationId = null;}
                data = self.prepareAnalyticsReqData(data);
                if(locationId) self.apiReq('GET', 'rest/analytics/'+locationId+'/'+label+'/', data, cb);
                else self.apiReq('GET', 'rest/analytics/'+label+'/', data, cb);
            }

            /*============ Admin apps ============*/

            self.admin = {}

            self.admin.apps = {};

            // http://docs.vehicle.apiary.io/#reference/admin-api/upload-application-package/upload-package
            self.admin.apps.upload = function(file, hadwareType, appName, version, onprogress, cb){
                var apiUrl = prov.config.apiRoot + 'admin/appstore/binary/' + hadwareType + '/' + appName + '/' + version + '/';
                var apiMethod = 'POST';
                var fileType = file.type;
                var xhr = new XMLHttpRequest();
                xhr.open(apiMethod, apiUrl, true);
                xhr.setRequestHeader('X-Pando-Authentication', self.getAccessToken());
                xhr.setRequestHeader('Content-type', file.type);
                xhr.upload.onprogress = onprogress;
                xhr.onload = function(){ 
                    if(!PandoErr.isCode200(xhr.status)) return cb({name: 'WRONG_STATUS', data: xhr.status});
                    try{
                        var data = JSON.parse(xhr.responseText);
                        return cb(null, data); 
                    }catch(e){
                        return cb({name: 'DATA_PARSING_ERROR', data: e});
                    }
                };
                xhr.send(file);
                log.trace('api req: ' + JSON.stringify({url: apiUrl, method: apiMethod, fileType: fileType}), m);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/list-application-packages/get-packages-list
            self.admin.apps.list = function(data, cb){
                if(typeof data === 'function'){cb = data; data = {}};
                if(!data.offset) data.offset = 0;
                if(!data.limit) data.limit = 100;
                self.apiReq('GET', 'admin/appstore/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/access-specified-application-package/get-package
            self.admin.apps.get = function(versionId, cb){
                self.apiReq('GET', 'admin/appstore/' + versionId + '/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/access-specified-application-package/update-package
            self.admin.apps.update = function(versionId, data, cb){
                self.apiReq('PUT', 'admin/appstore/' + versionId + '/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/access-specified-application-package/delete-package
            self.admin.apps.remove = function(versionId, cb){
                self.apiReq('DELETE', 'admin/appstore/' + versionId + '/', {}, cb);
            }

            /*============ Admin firmware ============*/

            self.admin.firmwares = {};

            // http://docs.vehicle.apiary.io/#reference/admin-api/upload-device-firmware/upload-firmware-package
            self.admin.firmwares.upload = function(file, hadwareType, version, onprogress, cb){
                var apiUrl = prov.config.apiRoot + 'admin/firmware/binary/' + hadwareType + '/' + version + '/';
                var apiMethod = 'POST';
                var fileType = file.type;
                var xhr = new XMLHttpRequest();
                xhr.open(apiMethod, apiUrl, true);
                xhr.setRequestHeader('X-Pando-Authentication', self.getAccessToken());
                xhr.setRequestHeader('Content-type', file.type);
                xhr.upload.onprogress = onprogress;
                xhr.onload = function(){ 
                    if(!PandoErr.isCode200(xhr.status)) return cb({name: 'WRONG_STATUS', data: xhr.status});
                    try{
                        var data = JSON.parse(xhr.responseText);
                        return cb(null, data); 
                    }catch(e){
                        return cb({name: 'DATA_PARSING_ERROR', data: e});
                    }
                };
                xhr.send(file);
                log.trace('api req: ' + JSON.stringify({url: apiUrl, method: apiMethod, fileType: fileType}), m);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/list-firmware-packages/get-firmware-packages-list
            self.admin.firmwares.list = function(data, cb){
                if(typeof data === 'function'){cb = data; data = {}};
                if(!data.offset) data.offset = 0;
                if(!data.limit) data.limit = 100;
                self.apiReq('GET', 'admin/firmware/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/access-specified-firmware-package/get-firmware-package
            self.admin.firmwares.get = function(versionId, cb){
                self.apiReq('GET', 'admin/firmware/' + versionId + '/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/access-specified-firmware-package/update-firmware-package
            self.admin.firmwares.update = function(versionId, data, cb){
                self.apiReq('PUT', 'admin/firmware/' + versionId + '/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/access-specified-firmware-package/delete-firmware-package
            self.admin.firmwares.remove = function(versionId, cb){
                self.apiReq('DELETE', 'admin/firmware/' + versionId + '/', {}, cb);
            }

            /*============ Admin companies ============*/

            self.admin.companies = {};

            // http://docs.vehicle.apiary.io/#reference/admin-api/access-specified-firmware-package/create-company-with-invitation
            self.admin.companies.invite = function(data, cb){
                self.apiReq('POST', 'admin/company/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/list-all-companies/get-companies-list
            self.admin.companies.list = function(data, cb){
                if(typeof data === 'function'){cb = data; data = {}};
                if(!data.offset) data.offset = 0;
                if(!data.limit) data.limit = 100;
                self.apiReq('GET', 'admin/company/', data, cb);
            }

            self.admin.companies.billing = {};

            // http://docs.vehicle.apiary.io/#reference/admin-api/get-company-billing-details/get-billing-details
            self.admin.companies.billing.get = function(companyId, cb){
                self.apiReq('GET', 'admin/company/' + companyId + '/billing/', {}, cb);
            }

            self.admin.companies.subscription = {};

            // http://docs.vehicle.apiary.io/#reference/admin-api/manage-company-subscriptions/assign-trial-subscription
            self.admin.companies.subscription.get = function(companyId, cb){
                self.apiReq('GET', 'admin/company/' + companyId + '/subscription/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/manage-company-subscriptions/get-subscription-details
            self.admin.companies.subscription.update = function(companyId, data, cb){
                self.apiReq('POST', 'admin/company/' + companyId + '/subscription/', data, cb);
            }

            /*============ Admin users ============*/

            self.admin.users = {};

            // http://docs.vehicle.apiary.io/#reference/admin-api/list-all-users/get-users-list
            self.admin.users.list = function(data, cb){
                if(typeof data === 'function'){cb = data; data = {}};
                if(!data.offset) data.offset = 0;
                if(!data.limit) data.limit = 100;
                self.apiReq('GET', 'admin/user/', data, cb);
            }

            /*============ Admin locations ============*/

            self.admin.locations = {};

            // http://docs.vehicle.apiary.io/#reference/admin-api/list-all-locations/get-locations-list
            self.admin.locations.get = function(data, cb){
                self.apiReq('GET', 'admin/location/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/manage-specific-location/delete-location
            self.admin.locations.remove = function(id, cb){
                self.apiReq('DELETE', 'admin/location/' + id + '/', {}, cb);
            }

            /*============ Admin devices ============*/

            self.admin.devices = {};

            // http://docs.vehicle.apiary.io/#reference/admin-api/list-all-devices/get-devices-list
            self.admin.devices.list = function(data, cb){
                if(typeof data === 'function'){cb = data; data = {}};
                if(!data.offset) data.offset = 0;
                if(!data.limit) data.limit = 100;
                self.apiReq('GET', 'admin/device/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/delete-device-physically/delete-device
            self.admin.devices.remove = function(deviceId, cb){
                self.apiReq('DELETE', 'admin/device/' + deviceId + '/', {}, cb);
            }

            /*============ Admin Device Production Status ============*/

            self.admin.devices.productionStatus = {};

            // http://docs.vehicle.apiary.io/#reference/admin-api/list-all-devices/get-device-production-status
            self.admin.devices.productionStatus.get = function(deviceId, cb){
                self.apiReq('GET', 'admin/device/' + deviceId + '/production/', {}, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/admin-api/manage-device-production-status/change-device-production-status
            self.admin.devices.productionStatus.update = function(deviceId, deviceStatus, cb){
                self.apiReq('PUT', 'admin/device/' + deviceId + '/production/', deviceStatus, cb);
            }

            /*============ Admin logs ============*/

            self.admin.devices.logs = {};

            // http://docs.vehicle.apiary.io/#reference/device-logs/device-logs-collection/list-all-log-entries
            self.admin.devices.logs.list = function(deviceId, data, cb){
                if(typeof data === 'function'){cb = data; data = {}};
                if(!data.offset) data.offset = 0;
                if(!data.limit) data.limit = 100;
                self.apiReq('GET', 'admin/device/' + deviceId + '/logs/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/device-logs/device-logs-request/request-logs-from-the-device
            self.admin.devices.logs.request = function(deviceId, data, cb){
                if(typeof data === 'function'){cb = data; data = {}};
                if(!data.linesCount) data.linesCount = 1000;
                self.apiReq('POST', 'admin/device/' + deviceId + '/logs/', data, cb);
            }

            // http://docs.vehicle.apiary.io/#reference/device-logs/access-specified-log-entry/download-log-entry
            self.admin.devices.logs.get = function(deviceId, entryId, cb){
                self.apiReq('GET', 'admin/device/' + deviceId + '/logs/' + entryId + '/', {}, cb);
            }

            self.admin.devices.logs.getUrl = function(deviceId, entryId){
                return prov.config.apiRoot + 'admin/device/' + deviceId + '/logs/' + entryId + '/';
            }

            self.admin.devices.logs.download = function(device, entry, cb){
                var logUrl = prov.config.apiRoot + 'admin/device/' + device.deviceId + '/logs/' + entry.entryId + '/';
                var apiToken = self.getAccessToken();
                if(!fetch) return alert('Fetch API not supported on your browser.');
                var opt = {headers: {"X-Pando-Authentication": apiToken}};
                fetch(logUrl, opt).then(function(response){
                    if(!PandoErr.isCode200(response.status)) return cb({name: 'WRONG_STATUS', data: response.status});
                    var name = 'log_' + moment(entry.created).format('YYYY-MM-DD_HH-mm-ss') + '.log';
                    response.blob().then(function(blob){
                        if(cb) cb();
                        saveAs(blob, name || "pando.log");
                    });
                });
            }

            // http://docs.vehicle.apiary.io/#reference/device-logs/access-specified-log-entry/delete-log-entry-from-the-storage
            self.admin.devices.logs.remove = function(deviceId, entryId, cb){
                self.apiReq('DELETE', 'admin/device/' + deviceId + '/logs/' + entryId + '/', {}, cb);
            }

            /*============ API ============*/

            self.apiReq = function(method, path, data, opt, cb){
                if(method === undefined) throw new Error('method not set');
                if(path === undefined) throw new Error('path not set');
                if(typeof data === 'function'){ cb = data; data = {}; opt = {}}
                if(typeof opt === 'function'){ cb = opt; opt = {}}

                var reqOpt = self.getApiReqOpt(method, path, data, opt);

                log.trace('api req: ' + JSON.stringify(reqOpt), m);
                log.trace('headers: ' + JSON.stringify($http.defaults.headers.common), m);

                self.emit(self.notify.req.begin, reqOpt);

                $http(reqOpt).then(function successCallback(res){

                    self.emit(self.notify.req.end, reqOpt);

                    // succesfull request
                    if(PandoErr.isCode200(res.status)){
                        self.emit(self.notify.req.success, res);
                        return cb(null, res.data);
                    }

                    // server error
                    const err = self.apiResToError(res);
                    self.emit(self.notify.req.error, err);
                    return cb(err);

                }, function errorCallback(res){
                    self.emit(self.notify.req.end, reqOpt);
                    const err = self.apiResToError(res);
                    self.emit(self.notify.req.error, err);
                    return cb(err);
                });
            }

            self.getApiReqOpt = function(method, path, data, opt){
                if(method === undefined) throw new Error('method not set');
                if(path === undefined) throw new Error('path not set');
                if(data === undefined) data = {};
                if(opt === undefined) opt = {};

                var reqOpt = {};

                reqOpt.method = method;
                reqOpt.url = prov.config.apiRoot + path;

                if(self.isGetMethod(reqOpt.method)){
                    reqOpt.params = data;
                }else{
                    reqOpt.data = data;
                }
                if(opt.res && opt.res && opt.res.transform === false){
                    reqOpt.transformResponse = [function (data) { return data; }];
                }
                return reqOpt;
            }

            self.isGetMethod = function(str){
                if(str === undefined) return false;
                return str.toLowerCase() == 'get';
            }

            // getting error from api response
            self.apiResToError = function(res){
                if(PandoErr.isCode500(res.status)) return PandoErr.serverErr(res.status, res.data);
                // api call error
                if(res.status == 400) return PandoErr.err400(res.data);
                if(res.status == 403) return PandoErr.unauthorizedErr(res.data);
                if(res.status == 404) return PandoErr.err404(res.data);
                // unknow error
                return PandoErr.unknowErr(res.status, res.data);
            }

        }

        prov.$get = function($rootScope, $http, PandoErr){
            "ngInject";
            return new apiServise($rootScope, $http, PandoErr);
        }

    })

    .factory('PandoErr', function(){
        "ngInject";

        var self = this;

        var m = 'PandoErr';

        function PandoErr(code, name, descr, data){
            if(typeof code !== 'number') code = parseInt(code);
            if(code == 0) throw new Error('wrong error code format');

            this.code = code;
            this.name = name ? name : '';
            this.descr = descr ? descr : '';
            this.data = data ? data : null;
        }

        /*============ Class function ============*/

        PandoErr.isCode100 = function(code){
            return this.isCodeInRange(code, 100, 199);
        }

        PandoErr.isCode200 = function(code){
            return this.isCodeInRange(code, 200, 299);
        }

        PandoErr.isCode300 = function(code){
            return this.isCodeInRange(code, 300, 399);
        }

        PandoErr.isCode400 = function(code){
            return this.isCodeInRange(code, 400, 499);
        }

        PandoErr.isCode500 = function(code){
            return this.isCodeInRange(code, 500, 599);
        }

        PandoErr.isCodeInRange = function(code, fromRange, toRange){
            if(code === undefined) return false;
            if((code >= fromRange)&&(code <= toRange)) return true;
            else return false;
        }

        /*============ Base Errors ============*/

        PandoErr.err400 = function(data){
            return new PandoErr(400, 'BAD_REQUEST', 'Bad Request. Some problems at the server side', data);
        }

        PandoErr.err403 = function(data){
            return new PandoErr(403, 'UNAUTHORIZED', 'Access forriben', data);
        }

        PandoErr.err404 = function(data){
            return new PandoErr(404, 'NOT_FOUND', 'API endpoint not found', data);
        }

        /*============ Named errors ============*/

        PandoErr.unknowErr = function(code, descr){
            return new PandoErr(code, 'UNKNOW_ERR', descr);
        };

        PandoErr.serverErr = function(code, data){
            return new PandoErr(code, 'SERVER_ERR', 'Internal server error', data);
        }

        PandoErr.unauthorizedErr = function(data){
            return this.err403(data);
        }

        PandoErr.paramMissed = function(descr, data){
            return PandoErr(400, 'PARAM_MISSED', descr, data);
        }

        PandoErr.userNotFound = function(data){
            return PandoErr(400, 'USER_NOT_FOUND', 'user not found', data);
        }

        /*============ Prototype ============*/

        PandoErr.prototype = {
            toString: function(){
                var str = '';
                if(this.code) str += '[' + this.code + '] ';
                if(this.name) str += this.name + ': ';
                if(this.descr) str += this.descr;
                if(this.data) str += '; data: ' + this.data.toString();
                return  str;
            },

            data: function(){
                return this.toData();
            },

            toData: function(){
                var errData = {code: this.code, name: this.name, descr: this.descr};
                if(this.data) errData.data = this.data;
                return errData;
            }
        }

        return PandoErr;

    });