/*

Params Storage

Service for storagin local params

*/

angular.module('pando.storage', [])
    .service('pandoParamsStorage', function(){
        "ngInject";
        
        var m = 'pandoParamsStorage';
        var debug = false;
        var self = this;

        self.keysPrefix = 'pando';
        
        self.set = function(key, val){
            key = keyToStorageKey(key);
            val = JSON.stringify(val);
            if(debug) log.debug('set key: ' + key + ', val: ' + val, m);
            localStorage.setItem(key, val);
        }

        self.get = function(key){
            key = keyToStorageKey(key);
            var val = localStorage.getItem(key);
            if(!val) return null;
            if(val == 'undefined') return undefined;
            val = JSON.parse(val);
            return val;
        }

        self.remove = function(key){
            key = keyToStorageKey(key);
            if(debug) log.debug('removing key: ' + key, m);
            if(localStorage.getItem(key)){
                localStorage.removeItem(key);
                if(debug) log.debug('key removed', m);
            }
        }

        function keyToStorageKey(key){
            return self.keysPrefix + ':' + key;
        }
        
    });