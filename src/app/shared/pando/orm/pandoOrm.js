angular.module('pando.orm', ['pando.api', 'pando.utils'])
    .service('pandoOrm', function($rootScope, pandoApi, pandoUtils){
        "ngInject";

        // ====================
        // Inheritance
        // ====================

        var inheritsFrom = function (child, parent) {
            child.prototype = Object.create(parent.prototype);
        };

        // ====================
        // Notifications
        // ====================

        var ormNotify = {
            app: {
                data: {
                    saved: 'pando:app:data:saved'
                },
                params: {
                    saved: 'pando:app:params:saved'
                },
                schedule: {
                    saved: 'pando:app:schedule:saved'
                }
            }
        }

        this.notify = ormNotify;

        // ====================
        // Basic object
        // ====================

        // obj.update - populate obj data
        // obj.save - save object data
        // obj.remove - remove object

        function BaseObj(data){
            
        }

        BaseObj.prototype = {

            data: function(newData){
                if(newData !== undefined){
                    this.setData(newData);
                }
                return this.getData();
            },

            getData: function(){
                var self = this;
                var data = {};
                var attrs = self.getAttrs();
                _.each(attrs, function(attr){
                    data[attr] = self[attr] !== undefined ? self[attr] : undefined;
                });
                return pandoUtils.clone(data);
            },

            setData: function(data){
                var self = this;
                var data = pandoUtils.clone(data);
                // setting attributes
                var attrs = self.getAttrs();
                _.each(attrs, function(attr){
                    self[attr] = data[attr] !== undefined ? data[attr] : undefined;
                });
                // setting id field
                var idKeyName = self.getIdKeyName();
                self.id = data[idKeyName] ? data[idKeyName] : null;
            },

            getIdKeyName: function(){
                var self = this;
                if(!self.mapping) throw new Error('Mapping not set');
                if(!self.mapping.id) throw new Error('Mapping id not set');
                return self.mapping.id;
            },

            getAttrs: function(){
                var self = this;
                var self = this;
                if(!self.mapping) throw new Error('Mapping not set');
                if(!self.mapping.attrs) throw new Error('Mapping attrs not set');
                if(!_.isArray(self.mapping.attrs)) throw new Error('Mapping attrs not array');
                return self.mapping.attrs;
            }

        }

        this.BaseObj = BaseObj;

        // ====================
        // User
        // ====================

        function User(data){
            var self = this;
            self.mapping = {
                id: 'userId',
                attrs: [
                    'userId',
                    'created',
                    'firstName',
                    'lastName',
                    'email',
                    'companyId',
                    'permissions',
                    'isAdmin'
                ]
            }
            self.devices = [];
            self.locations = [];
            self.copmany = null;
            // flags
            self.populateLocationsProcessing = false;
            self.populateCompanyProcessing = false;
            // init
            self.setData(data);
        }

        User.get = function(id, cb){
            pandoApi.users.get(id, function(err, data){
                if(err) return cb(err);
                if(!data) return cb();
                return cb(null, new User(data));
            });
        }

        User.list = function(cb){
            pandoApi.users.list(function(err, data){
                if(err) return cb(err);
                data = _.map(data, function(item){ return new User(item); });
                return cb(null, data);
            });
        }

        inheritsFrom(User, BaseObj);

        User.prototype.update = function(cb){
            var self = this;
            pandoApi.users.get(this.id, function(err, data){
                if(err) return cb(err);
                self.setData(data);
                return cb();
            });
        }

        User.prototype.save = function(cb){
            var data = this.data();
            pandoApi.users.update(this.id, data, cb);
        }

        User.prototype.remove = function(cb){
            pandoApi.users.remove(this.id, cb);
        }

        // Password

        User.prototype.changePassword = function(newPass, cb){
            pandoApi.auth.pass.change(newPass, cb);
        }

        // Locations

        User.prototype.locationWithId = function(locId){
            var self = this;
            if(!self.locations) return null;    
            if(!self.locations.length) return null;
            return _.find(self.locations, function(loc){
                return loc.id == locId;
            });
        }

        User.prototype.populateLocations = function(cb){
            var self = this;
            self.populateLocationsProcessing = true;
            self.getLocations(function(err, locations){
                self.populateLocationsProcessing = false;
                if(err) return cb(err)
                self.locations = locations;
                return cb(null, locations);
            });
        }

        User.prototype.getLocations = function(cb){
            Location.list(cb);
        }

        User.prototype.devicesAtLocation = function(location){
            if(!location) return [];
            var self = this;
            return _.filter(self.devices, function(device){
                return device.locationId == location.id;
            });
        }

        // Devices

        User.prototype.deviceWithId = function(deviceId){
            if(!deviceId) return null;
            var self = this;
            return _.find(self.devices, function(device){
                return device.id == deviceId;
            });
        }

        User.prototype.populateDevices = function(cb){
            var self = this;
            self.populateDevicesProcessing = true;
            this.getDevices(function(err, devices){
                self.populateDevicesProcessing = false;
                if(err) return cb(err);
                self.devices = devices;
                return cb();
            });
        }

        User.prototype.getDevices = function(cb){
            Device.list(function(err, devices){
                if(err) return cb(err);
                return cb(null, devices);
            });
        }

        // Users

        User.prototype.populateUsers = function(cb){
            var self = this;
            User.list(function(err, users){
                if(err) return cb(err);
                self.users = users;
                self.updateUsersListPermissions();
                return cb();
            });
        }

        User.prototype.addToUsers = function(data, role, cb){
            if(typeof role === 'function'){cb = role; role = null};
            if(_.isString(data)) data = {email: data};
            if(data.permissions === undefined) data.permissions = [];
            if(role){
                data.permissions.push({companyId: this.companyId, role: role});
            }
            var self = this;
            pandoApi.users.add(data, function(err, userData){
                if(err) return cb(err);
                var user = new User(userData);
                self.addToUsersList(user);
                return cb();
            });
        }

        User.prototype.addToUsersList = function(user){
            if(!this.users) this.users = [];
            this.users.push(user);
            this.updateUsersListPermissions();
        }

        User.prototype.removeFromUsers = function(user, cb){
            if(!user) throw new Error('user not specified');
            var self = this;
            var userId = user.id !== undefined ? user.id : user;
            pandoApi.users.remove(userId, function(err){
                if(err) return cb(err);
                self.removeFromUsersList(user);
                return cb();
            });
        }

        User.prototype.removeFromUsersList = function(user){
            if(!user) throw new Error('user not specified');
            var userId = user.id !== undefined ? user.id : user;
            if(!this.users) return;
            if(!this.users.length) return;
            this.users = _.filter(this.users, function(item){
                return item.id != userId;
            });
        }

        User.prototype.updateUsersListPermissions = function(){
            var self = this;
            _.each(self.users, function(userItem){
                if(!userItem.permissions || !_.isArray(userItem.permissions)){
                    userItem.permissions = [];
                    userItem.currentCompanyPermission = pandoApi.company.permissions.none.val;
                    return;
                }
                if(userItem.permissions.length == 0){
                    userItem.currentCompanyPermission = pandoApi.company.permissions.none.val;
                    return;
                }
                const currentCompanyPermissionItem = _.find(userItem.permissions, function(permissionItem){
                    return permissionItem.companyId == self.companyId;
                });
                if(currentCompanyPermissionItem) userItem.currentCompanyPermission = currentCompanyPermissionItem.role;
                else userItem.currentCompanyPermission = pandoApi.company.permissions.none.val;

                if(!userItem.currentCompanyPermission) userItem.currentCompanyPermission = pandoApi.company.permissions.none.val;
            });
        }

        // Company

        User.prototype.populateCompany = function(cb){
            var self = this;
            self.populateCompanyProcessing = true;
            Company.get(this, function(err, company){
                self.populateCompanyProcessing = false;
                if(err) return cb(err);
                self.company = company;
                return cb();
            });
        }
        
        this.User = User

        // ====================
        // Device
        // ====================

        function Device(data){
            var self = this;
            self.mapping = {
                id: 'deviceId',
                attrs: [
                    'deviceId',
                    'hardwareId',
                    'hardwareType',
                    'firmwareVersion',
                    'autoUpdate',
                    'hasNewVersion',
                    'hardwareConfiguration',
                    'companyId',
                    'created',
                    'lastUpdate',
                    'name',
                    'locationId',
                    'active',
                    'rentEnabled',
                    'approveAdvertisers',
                    'minCPI',
                    'maintenanceTime',
                    'profile',
                    'isDeleted'
                ]
            }
            // external data
            self.apps = [];
            self.connected = false;
            // flags
            self.hardwareConfigurationPopulated = false;
            // init
            self.setData(data);
        }

        Device.create = function(data, location, cb){
            if(!data) throw new Error('data not specified');
            if(typeof location === 'function'){cb = location; location = null}
            // checking tokenPhrase
            if(data.phrase && !data.tokenPhrase){
                data.tokenPhrase = data.phrase;
                delete data.phrase;
            }
            if(!data.tokenPhrase) throw new Error('tokenPhrase required');
            // chekcing name
            if(!data.name) throw new Error('name required');
            // checking location
            if(location && !data.locationId){
                if(location instanceof Location){
                    data.locationId = location.id;
                }else if (typeof location === 'number'){
                    data.locationId = location
                }else if (typeof location === 'string'){
                    var num = parseInt(location);
                    data.locationId = num ? num : null;
                }
            }
            if(!data.locationId) throw new Error('locationId required');
            // adding device
            pandoApi.devices.add(data, function(err, data){
                if(err) return cb(err);
                var obj = new Device(data);
                return cb(null, obj);
            });
        }

        Device.get = function(id, cb){
            pandoApi.devices.get(id, function(err, data){
                if(err) return cb(err);
                return cb(null, new Device(data));
            });
        }

        Device.list = function(cb){
            pandoApi.devices.list(function(err, data){
                if(err) return cb(err);
                data = _.map(data, function(item){ return new Device(item); });
                return cb(null, data);
            });
        }

        Device.listAll = function(cb){
            pandoApi.devices.listAll(function(err, data){
                if(err) return cb(err);
                data = _.map(data, function(item){ return new Device(item); });
                return cb(null, data);
            });
        }

        inheritsFrom(Device, BaseObj);

        Device.prototype.m = 'pandoOrm.Device';

        Device.prototype.populateHardwareConfiguration = function(cb){
            var self = this;
            self.hardwareConfigurationPopulated = false;
            self.update(function(err){
                if(err) return cb(err);
                self.hardwareConfigurationPopulated = true;
                return cb();
            });
        }

        Device.prototype.update = function(cb){
            var self = this;
            pandoApi.devices.get(self.id, function(err, data){
                if(err) return cb(err);
                self.setData(data);
                return cb();
            });
        }

        Device.prototype.save = function(cb){
            var data = this.data();
            pandoApi.devices.update(this.id, data, cb);
        }

        Device.prototype.remove = function(cb){
            pandoApi.devices.remove(this.id, cb);
        }

        Device.prototype.restart = function(cb){
            pandoApi.devices.restart(this.id, cb);
        }

        Device.prototype.resolution = function(){
            if(!this.hardwareConfiguration) return null;
            if(!this.hardwareConfiguration.resolutionCode) return null;
            if(!_.isNumber(this.hardwareConfiguration.resolutionCode)) return null;
            if(!this.hardwareConfiguration.availableResolutions) return null;
            if(!_.isArray(this.hardwareConfiguration.availableResolutions)) return null;
            if(!this.hardwareConfiguration.availableResolutions.length) return null;

            var resolutionCode = this.hardwareConfiguration.resolutionCode;
            var resolutions = this.hardwareConfiguration.availableResolutions;
            return _.find(resolutions, function(resolution){
                return resolution.code == resolutionCode;
            });
        }

        // Realtime

        Device.prototype.realtimeGetLastHourEvents = function(cb){
            this.realtimeGetEvents(cb);
        }

        Device.prototype.realtimeGetCurrentEvents = function(cb){
            this.realtimeGetEvents({currentlySensing: true}, cb);
        }

        Device.prototype.realtimeGetEvents = function(data, cb){
            if(typeof data === 'function'){cb = data; data = {}}
            this.realtimeGet(data, function(err, realtimeData){
                if(err) return cb(err);
                if(!realtimeData.events) return cb(null, []);
                else return cb(null, realtimeData.events);
            });
        }

        Device.prototype.realtimeGet = function(data, cb){
            if(typeof data === 'function'){cb = data; data = {}}
            var self = this;
            pandoApi.devices.realtime.get(this.id, data, function(err, realtimeData){
                if(err) return cb(err);
                if(realtimeData.connected !== undefined){
                    self.connected = realtimeData.connected;
                }
                return cb(null, realtimeData);
            });
        }

        // Apps

        Device.prototype.saveActiveApps = function(cb){
            var self = this;
            async.each(self.activeApps, function(app, cb){
                app.save(cb);
            }, cb);
        }

        Device.prototype.populateAppsAndInstallUninstalled = function(cb){
            var self = this;
            self.populateApps(function(err, loadedApps){
                if(err) return cb(err);
                // getting array of avaliable apps names (eg. ['graphic', 'twitter']
                var avaliableApps = _.map(pandoApi.apps.avaliable, function(item, keys){
                    return item.val;
                });
                // getting array of installed apps names (eg. ['graphic', 'twitter']
                var installedApps = _.map(loadedApps, function(item, keys){
                    return item.name;
                });
                // checking is app is installed 
                async.each(avaliableApps, function(avaliableApp, cb){
                    // if app is installed - moving to next
                    if(_.contains(installedApps, avaliableApp)) return cb();
                    // else - installing app
                    log.debug('installing app: ' + avaliableApp, self.m);
                    self.installApp(avaliableApp, {active: false}, function(err, appData){
                        if(err) return cb(err);
                        log.debug('installing ' + avaliableApp + 'done', self.m);
                        return cb(err, appData);
                    });
                }, function(err){
                    if(err) return cb(err);
                    // returning installed apps
                    return cb(null, self.apps);
                });
            });
        }

        Device.prototype.populateApps = function(cb){
            var self = this;
            self.appsPopulated = false;
            App.list(self, function(err, apps){
                if(err) return cb(err);
                if(!apps || !_.isArray(apps)) apps = [];
                self._setAppsProperty(apps);
                self.appsPopulated = true;
                return cb(null, apps);
            });
        }

        Device.prototype.installApp = function(appName, data, cb){
            if(typeof data === 'function'){cb = data; data = {}};
            var self = this;
            App.install(self, appName, data, function(err, app){
                if(err) return cb(err);
                if(!self.apps) self.apps = [];
                var apps = self.apps;
                apps.push(app);
                self._setAppsProperty(apps);
                return cb(null, app);
            });
        }

        Device.prototype._setAppsProperty = function(apps){
            var self = this;
            self.apps = _.sortBy(apps, function(app){
                return app.name;
            });
            self.sortAppsByActiveAndInactive();
        }

        Device.prototype.sortAppsByActiveAndInactive = function(){
            var self = this;
            var apps = self.apps;
            self.activeApps = _.filter(apps, function(app){
                return app.active;
            });
            self.activeApps = _.sortBy(self.activeApps, function(app){
                return app.name;
            });
            self.inactiveApps = _.filter(apps, function(app){
                return !app.active;
            });
            self.inactiveApps = _.sortBy(self.inactiveApps, function(app){
                return app.name;
            });
        }

        Device.prototype.appWithName = function(appName){
            var self = this;
            return _.find(self.apps, function(item){
                return item.name == appName;
            });
        }

        Device.prototype.appWithId = function(appId){
            var self = this;
            return _.find(self.apps, function(item){
                return item.id == appId;
            });
        }

        // Rules

        Device.prototype.rulesPopulate = function(cb){
            var self = this;
            this.rulesGet(function(err, rules){
                if(err) return cb(err);
                self.rules = rules;
                return cb();
            })
        }

        Device.prototype.rulesGet = function(cb){
            var self = this;
            Rule.list(this, cb);
        }

        this.Device = Device;

        // ====================
        // App
        // ====================

        function App(device, data){
            var self = this;
            self.mapping = {
                id: 'applicationId',
                attrs: [
                    'applicationId',
                    'name',
                    'active',
                    'lastUpdate',
                    'version',
                    'weight',
                    'duration',
                    'applicationVersion',
                    'retargetingAllowed',
                    'rentAllowed',
                    'rentEnabled',
                    'approveContent',
                    'rentWeight',
                    'maxAdDuration'
                ]
            }
            // External object
            self.device = device;
            // Flags
            self.paramsPopulated = false;
            self.paramsPopulateInProcess = false;
            self.schedulePopulated = false;
            // Init
            self.setData(data);
        }

        App.get = function(device, appId, cb){
            pandoApi.apps.get(device.id, function(err, data){
                if(err) return cb(err);
                return cb(null, new App(device, data));
            });
        }

        App.list = function(device, cb){
            pandoApi.apps.list(device.id, function(err, data){
                if(err) return cb(err);
                var apps = _.map(data, function(itemData){ 
                    if(_.contains(ItemsApp.supportedApps, itemData.name)){
                        return new ItemsApp(device, itemData); 
                    }
                    return new App(device, itemData); 
                });
                return cb(null, apps);
            });
        }

        App.install = function(device, appName, data, cb){
            if(typeof data === 'function'){cb = data; data = {}};
            pandoApi.apps.install(device.id, appName, data, function(err, data){
                if(err) return cb(err);
                var app = null;
                if(_.contains(ItemsApp.supportedApps, data.name)){
                    app = new ItemsApp(device, data); 
                }else{
                    app = new App(device, data);
                }
                return cb(null, app);
            });
        }

        inheritsFrom(App, BaseObj);

        App.prototype.update = function(cb){
            var data = this.data();
            var self = this;
            pandoApi.apps.get(this.device.id, this.id, function(err, data){
                if(err) return cb(err);
                self.setData(data);
                return cb();
            });
        }

        App.prototype.save = function(cb){
            var self = this;
            var data = this.data();
            pandoApi.apps.update(this.device.id, this.id, data, function(err, data){
                if(err) return cb(err);
                $rootScope.$broadcast(ormNotify.app.data.saved, self);
                return cb(null, data);
            });
        }

        App.prototype.remove = function(cb){
            pandoApi.apps.remove(this.device.id, this.id, cb);
        }

        // Schedule

        App.prototype.populateSchedule = function(cb){
            var self = this;
            self.schedulePopulated = false;
            self.loadSchedule(function(err, scheduleData){
                if(err) return cb(err);
                self.schedulePopulated = true;
                self.schedule = scheduleData;
                return cb(null, self.schedule);
            });
        }

        App.prototype.loadSchedule = function(cb){
            var self = this;
            pandoApi.apps.schedule.get(this.device.id, this.id, cb);
        }

        App.prototype.saveSchedule = function(cb){
            var self = this;
            pandoApi.apps.schedule.update(this.device.id, this.id, self.schedule, function(err, scheduleData){
                if(err) return cb(err);
                self.schedule = scheduleData;
                $rootScope.$broadcast(ormNotify.app.schedule.saved, self);
                return cb(null, self.schedule);
            });
        }

        // Params

        App.prototype.populateParams = function(cb){
            var self = this;
            self.paramsPopulated = false;
            self.paramsPopulateInProcess = true;
            this.paramsGet(function(err, params){
                self.paramsPopulateInProcess = false;
                if(err) return cb(err);
                self.paramsPopulated = true;
                self.params = params;
                return cb();
            });
        }

        App.prototype.paramsSave = function(cb){
            var self = this;
            self.paramsSet(self.params, function(err, newParams){
                if(err) return cb(err);
                self.params = newParams;
                $rootScope.$broadcast(ormNotify.app.params.saved, self);
                return cb(null, newParams);
            });
        }

        App.prototype.paramsGet = function(cb){
            var self = this;
            pandoApi.apps.params.get(this.device.id, this.id, function(err, data){
                if(err) return cb(err);
                if(!data) return cb(null);
                if(data.content === undefined) return cb(null);
                return cb(null, data.content);
            });
        }

        App.prototype.paramsSet = function(data, cb){
            var self = this;
            data = {content: data}
            pandoApi.apps.params.update(this.device.id, this.id, data, function(err, newData){
                if(err) return cb(err);
                if(!newData) return cb(null);
                if(newData.content === undefined) return cb(null);
                return cb(null, newData.content);
            });
        }

        // Rules

        App.prototype.rulesPopulate = function(cb){
            var self = this;
            this.rulesGet(function(err, rules){
                self.rules = rules;
                return cb();
            });
        }

        App.prototype.rulesGet = function(cb){
            Rule.summary(this.device, this, cb);
        }

        App.prototype.rulesCreate = function(data, params, schedule, cb){
            if(data.application === undefined) data.application = {};
            if(data.application.applicationId === undefined) data.application.applicationId = this.id;
            if(data.application.duration === undefined) data.application.duration = 20;
            Rule.create(this.device, data, params, schedule, cb);
        }

        this.App = App;

        // ====================
        // Items app
        // ====================

        function ItemsApp(device, data){
            App.call(this, device, data);
            this.items = [];
        }

        ItemsApp.supportedApps = [
             'webview'
            ,'graphics'
        ];

        inheritsFrom(ItemsApp, App);

        // Items

        ItemsApp.prototype.itemsPopulate = function(cb){
            var self = this;
            this.itemsGet(function(err, items){
                if(err) return cb(err);
                self.itemsNewVal(items);
                self.itemsRulesPopulate(cb);
            });
        }

        ItemsApp.prototype.itemsSave = function(cb){
            if(!this.items) return;
            if(!_.isArray(this.items)) return;
            var self = this;
            this.itemsCheck();
            this.itemsSet(this.items, function(err, items){
                if(err) return cb(err);
                self.itemsNewVal(items);
                self.removeUnusedRules(function(err){
                    if(err) return cb(err);
                    self.itemsRulesPropertiesUpdate();
                    return cb();
                });
            });
        }

        ItemsApp.prototype.itemsNewVal = function(newItems){
            var inProcessItems = _.filter(this.items, function(item){
                if(item.uploading) return true;
                if(item.processing) return true;
                return false;
            });
            this.items = newItems.concat(inProcessItems);
            this.itemsCheck();
        }

        ItemsApp.prototype.itemsGet = function(cb){
            var self = this;
            this.populateParams(function(err){
                if(err) return cb(err);
                if(!self.params) return cb(null, []);
                if(!self.params.items) return cb(null, []);
                if(!_.isArray(self.params.items)) return cb(null, []);
                return cb(null, self.params.items);
            });
        }

        ItemsApp.prototype.itemsSet = function(items, cb){
            if(!_.isArray(items)) throw new Error('items have to be an array');
            var self = this;
            items = pandoUtils.clone(items);
            items = _.filter(items, function(item){
                if(item.uploading) return false;
                if(item.processing) return false;
                return true;
            });
            _.each(items, function(item){
                if(item.$$hashKey) delete item.$$hashKey;
                if(item.uploading !== undefined) delete item.uploading;
                if(item.processing !== undefined) delete item.processing;
                if(item.rules) delete item.rules;
            });
            var data = {items: items};
            self.params = data;
            self.paramsSave(function(err){
                if(err) return cb(err);
                if(!self.params) return cb(null, []);
                if(!self.params.items) return cb(null, []);
                if(!_.isArray(self.params.items)) return cb(null, []);
                return cb(null, self.params.items);
            });
        }

        // Items manipulate

        ItemsApp.prototype.itemsFind = function(inputItem){
            if(!this.items) return null;
            return _.find(this.items, function(item){
                return item.id == inputItem.id;
            });
        }

        ItemsApp.prototype.itemsAdd = function(itemsData){
            if(!this.items) return this.items = [];
            if(!_.isArray(itemsData)) itemsData = [itemsData];
            this.items = this.items.concat(itemsData);
            this.itemsCheck();
        }

        ItemsApp.prototype.itemsRemove = function(inputItems){
            if(!_.isArray(inputItems)) inputItems = [inputItems];
            this.items = _.filter(this.items, function(item){
                var item = _.find(inputItems, function(inputItem){
                    return item.id == inputItem.id;
                });
                return item ? false : true;
            });
        }

        // Items check

        ItemsApp.prototype.itemsCheck = function(){
            this.itemsCheckId();
            this.itemsCheckOrderId();
        }

        ItemsApp.prototype.itemsCheckId = function(){
            if(!this.items) return;
            if(!_.isArray(this.items)) return;
            this.items = _.map(this.items, function(item){
                if(item.id === undefined){
                    item.id = pandoUtils.randomId();
                }
                return item;
            })
        }

        ItemsApp.prototype.itemsCheckOrderId = function(){
            _.each(this.items, function(item, index){
                item.order = index;
            });
        }

        // Rules

        ItemsApp.prototype.itemsRulesPopulate = function(cb){
            var self = this;
            this.rulesPopulate(function(err){
                if(err) return cb(err);
                self.itemsRulesPropertiesUpdate();
                return cb();
            });
        }

        ItemsApp.prototype.itemsRulesPropertiesUpdate = function(){
            var rules = this.rules;
            var items = this.items;
            _.each(items, function(item){
                item.rules = [];
                _.each(rules, function(rule){
                    if(!rule.params) return;
                    if(!rule.params.id) return;
                    if(rule.params.id == item.id) item.rules.push(rule);
                });
            });
        }

        ItemsApp.prototype.removeUnusedRules = function(cb){
            var self = this;
            if(!this.rules) return cb();
            if(!this.rules.length) return cb();
            var rulesToRemove = _.filter(this.rules, function(rule){
                if(!self.items) return true;
                if(!_.isArray(self.items)) return true;
                if(!self.items.length) return true;

                if(!rule.params) return true;
                if(!rule.params.id) return true;

                var item = _.find(self.items, function(item){
                    return item.id == rule.params.id;
                });
                return item ? false : true;
            });
            if(!rulesToRemove.length) return cb();
            async.each(rulesToRemove, function(rule, cb){
                rule.remove(function(err){
                    if(err) return cb(err);
                    self.removeRuleFromList(rule);
                    return cb();
                });
            }, cb);
        }

        ItemsApp.prototype.removeRuleFromList = function(ruleToRemove){
            this.rules = _.filter(this.rules, function(rule){
                return rule.id != ruleToRemove.id;
            });
        }

        this.ItemsApp = ItemsApp;

        // ====================
        // Rule
        // ====================

        function Rule(device, data){
            var self = this;
            var deviceId = device instanceof Device ? device.id : device;
            sef.mapping = {
                id: 'ruleId',
                attrs: [
                     'ruleId'
                    ,'name'
                    ,'lastUpdate'
                    ,'version'
                    ,'active'
                    ,'criteria'
                    ,'application'
                ]
            }
            self.deviceId = deviceId;
            self.fullVersion = true;
            self.setData(data);
        }

        Rule.create = function(device, data, params, schedule, cb){
            if(typeof params === 'function'){cb = params; params = null; schedule = null;}
            if(typeof schedule === 'function'){cb = schedule; schedule = null;}

            var deviceId = device instanceof Device ? device.id : device;

            if(data.name === undefined) data.name = 'Rule for Noname ' + pandoUtils.randomInt(10000, 99999);
            if(data.criteria === undefined) data.criteria = {};
            if(data.criteria.source === undefined) data.criteria.source = [deviceId];
            if(data.criteria.eventType === undefined) data.criteria.eventType = pandoApi.rules.eventTypes.visit.val;
            if(data.criteria.requireNew === undefined) data.criteria.requireNew = false;
            if(data.criteria.visitorsList === undefined) data.criteria.visitorsList = [];
            if(data.application === undefined) data.application = {};
            if(data.duration){
                data.application.duration = data.duration;
                delete data.duration;
            }
            if(data.applicationId){
                data.application.applicationId = data.applicationId;
                delete data.applicationId;
            }

            pandoApi.rules.add(deviceId, data, function(err, data){
                if(err) return cb(err);
                var rule = new Rule(device, data);
                rule.schedulePopulate(function(err){
                    if(err) return cb(err);
                    rule.schedule = schedule ? schedule : Rule.defSchedule;
                    rule.scheduleSave(function(err){
                        if(err) return cb(err);
                        if(!params) return cb(null, rule);
                        rule.params = params;
                        rule.paramsSave(function(err){
                            if(err) return cb(err);
                            return cb(null, rule);
                        });
                    });
                })
            });
        }

        Rule.summary = function(device, app, cb){
            var deviceId = device instanceof Device ? device.id : device;
            var appId = app instanceof App ? app.id : app;

            pandoApi.rules.summary(deviceId, appId, function(err, data){
                if(err) return cb(err);
                data = _.map(data, function(item){ 
                    // changing ruleName to name
                    if(item.ruleName !== undefined){
                        item.name = item.ruleName;
                        delete item.ruleName;
                    }
                    // getting rule params
                    var params = null;
                    if(item.applicationParameters !== undefined){
                        params = item.applicationParameters;
                        delete item.applicationParameters;
                    }
                    // setting rule application
                    item.application = {
                        applicationId: appId
                    }
                    var rule = new Rule(deviceId, item);
                    rule.params = params;
                    rule.fullVersion = false;
                    return rule;
                });
                return cb(null, data);
            });
        }

        Rule.get = function(device, rule, cb){
            var deviceId = device instanceof Device ? device.id : device;
            var ruleId = rule instanceof Rule ? rule.id : rule;

            pandoApi.rules.get(deviceId, ruleId, function(err, data){
                if(err) return cb(err);
                return cb(null, new Rule(deviceId, data));
            });
        }

        Rule.list = function(device, cb){
            var deviceId = device instanceof Device ? device.id : device;
            pandoApi.rules.list(deviceId, function(err, data){
                if(err) return cb(err);
                data = _.map(data, function(item){ return new Rule(deviceId, item); });
                return cb(null, data);
            });
        }

        Rule.defSchedule = {
            "days": {
                "mon": true, "tue": true, "wed": true, "thu": true, "fri": true, "sat": true, "sun": true
            },
            "start": "-999999999-01-01T00:00:00",
            "stop": "+999999999-12-31T23:59:59.999999999",
            "timeOfDay": []
        }

        inheritsFrom(Rule, BaseObj);

        Rule.prototype.update = function(cb){
            var self = this;
            pandoApi.rules.get(this.deviceId, this.id, function(err, data){
                if(err) return cb(err);
                self.setData(data);
                self.fullVersion = true;
                return cb();
            });
        }

        Rule.prototype.save = function(cb){
            var data = this.data();
            pandoApi.rules.update(this.deviceId, this.id, data, cb);
        }

        Rule.prototype.remove = function(cb){
            pandoApi.rules.remove(this.deviceId, this.id, cb);
        }

        // Schedule

        Rule.prototype.schedulePopulate = function(cb){
            var self = this;
            pandoApi.rules.schedule.get(this.deviceId, this.id, function(err, schedule){
                if(err) return cb(err);
                self.schedule = schedule;
                return cb();
            });
        }

        Rule.prototype.scheduleSave = function(cb){
            var self = this;
            pandoApi.rules.schedule.update(this.deviceId, this.id, self.schedule, function(err, schedule){
                if(err) return cb(err);
                self.schedule = schedule;
                return cb();
            });
        }

        // Params

        Rule.prototype.paramsPopulate = function(cb){
            var self = this;
            pandoApi.rules.params.get(this.deviceId, this.id, function(err, data){
                if(err) return cb(err);
                self.params = data.content;
                return cb();
            });
        }

        Rule.prototype.paramsSave = function(cb){
            var self = this;
            var data = {content: self.params};
            pandoApi.rules.params.update(this.deviceId, this.id, data, function(err, data){
                if(err) return cb(err);
                self.params = data.content;
                return cb();
            });
        }

        this.Rule = Rule;

        // ====================
        // Location
        // ====================

        function Location(data){
            var self = this;
            self.mapping = {
                id: 'locationId',
                attrs: [
                    'locationId',
                    'name',
                    'timezone',
                    'placeForeignId',
                    'address',
                    'lat',
                    'lon',
                    'created',
                    'lastUpdate',
                    'deleted'
                ]
            }
            self.setData(data);
        }

        Location.create = function(locationData, cb){
            pandoApi.locations.create(locationData, function(err, data){
                if(err) return cb(err);
                var obj = new Location(data);
                return cb(null, obj);
            });
        }

        Location.list = function(cb){
            pandoApi.locations.list(function(err, dataArr){
                if(err) return cb(err);
                var dataArr = _.map(dataArr, function(item){
                    return new Location(item);
                });
                return cb(null, dataArr);
            });
        }

        Location.get = function(id, cb){
            pandoApi.locations.get(id, function(err, data){
                if(err) return cb(err);
                var obj = new Location(data);
                return cb(null, obj);
            });
        }

        inheritsFrom(Location, BaseObj);

        Location.prototype.save = function(cb){
            var data = this.data();
            pandoApi.locations.update(this.id, data, cb);
        }

        Location.prototype.remove = function(cb){
            pandoApi.locations.remove(this.id, cb);
        }

        this.Location = Location;

        // ====================
        // Company
        // ====================

        function Company(user, data){
            var self = this;
            self.mapping = {
                id: 'companyId',
                attrs: [
                    "companyId",
                    "created",
                    "email",
                    "companyName",
                    "phone",
                    "fax",
                    "address",
                    "address2",
                    "city",
                    "state",
                    "country",
                    "zip"
                ]
            }
            // related objects
            self.user = user;
            // flags
            self.integrationsPopulated = false;
            self.subscriptionPopulated = false;
            // init
            self.setData(data);
        }

        Company.create = function(user, data, cb){
            if(typeof data === 'function'){cb = data; data = user; user = null;}
            pandoApi.company.create(data, function(err, data){
                if(err) return cb(err);
                var obj = new Company(user, data);
                return cb(null, obj);
            });
        }

        Company.get = function(user, cb){
            if(typeof user === 'function'){cb = user; user = null;}
            pandoApi.company.get(function(err, data){
                if(err) return cb(err);
                var obj = new Company(user, data);
                return cb(null, obj);
            });
        }

        inheritsFrom(Company, BaseObj);

        Company.prototype.update = function(cb){
            var self = this;
            pandoApi.company.get(function(err, data){
                if(err) return cb(err);
                self.setData(data);
                return cb();
            });
        }

        Company.prototype.save = function(cb){
            var data = this.data();
            pandoApi.company.update(data, cb);
        }

        // Integrations

        Company.prototype.populateIntegrations = function(cb){
            var self = this;
            self.integrationsPopulated = false;
            this.getIntegraionts(function(err, integrations){
                if(err) return cb(err);
                self.setIntegrationsList(integrations);
                self.integrationsPopulated = true;
                return cb();
            });
        }

        Company.prototype.getIntegraionts = function(cb){
            pandoApi.company.integrations.list(cb);
        }

        Company.prototype.setIntegrationsList = function(itegrs){
            if(!itegrs) itegrs = [];
            if(!this.integrations || !this.integrations.length){
                this.integrations = itegrs;
                return;
            }
            var self = this;
            // updating integrations list
            _.each(itegrs, function(itegrToAdd){
                var existsItegr = _.find(self.integrations, function(existsItegr){
                    return existsItegr.providerName == itegrToAdd.providerName;
                });
                if(existsItegr) existsItegr.status = itegrToAdd.status;
                else self.integrations.push(itegrToAdd);
            });
        }

        Company.prototype.updateIntegrationsUrls = function(cb){
            var statuses = pandoApi.company.integrations.statuses;
            async.each(this.integrations, function(integr, cb){
                if(integr.status == statuses.enabled.val) return cb();
                if(integr.url !== undefined) return cb();
                integr.loading = true;
                pandoApi.company.integrations.init(integr.providerName, function(err, data){
                    integr.loading = false;
                    if(err) return cb(err);
                    integr.url = data.redirectUrl;
                    return cb();
                })
            }, cb);
        }

        Company.prototype.removeIntegration = function(integration, cb){
            var self = this;
            var statuses = pandoApi.company.integrations.statuses;
            var providerName = integration.providerName ? integration.providerName : integration;
            pandoApi.company.integrations.remove(providerName, function(err){
                if(err) return cb(err);
                var existsItegr = _.find(self.integrations, function(existsItegr){
                    return existsItegr.providerName == providerName;
                });
                if(existsItegr){
                    existsItegr.status = statuses.disabled.val;
                }
                return cb();
            });
        }

        // Subscription

        Company.prototype.populateSubscription = function(cb){
            var self = this;
            self.subscriptionPopulated = false;
            self.getSubscription(function(err, subscriptionData){
                if(err) return cb(err);
                self.subscriptionPopulated = true;
                self.subscription = subscriptionData;
                return cb(null, subscriptionData);
            });
        }

        Company.prototype.getSubscription = function(cb){
            pandoApi.company.subscription.get(cb);
        }

        Company.prototype.subscribeForPlanWithId = function(planId, cb){
            var self = this;
            pandoApi.company.subscription.start(planId, function(err, subscriptionData){
                if(err) return cb(err);
                self.subscription = null;
                self.subscriptionPopulated = false;
                return cb(null, subscriptionData);
            });
        }

        Company.prototype.cancelSubscription = function(cb){
            var self = this;
            pandoApi.company.subscription.cancel(function(err){
                if(err) return cb(err);
                self.subscription = null;
                self.subscriptionPopulated = false;
                return cb();
            });
        }

        Company.prototype.buyAdditionalDevices = function(devicesCount, cb){
            pandoApi.company.subscription.buyDevices(devicesCount, function(err, subscriptionData){
                if(err) return cb(err);
                self.subscription = subscriptionData;
                self.subscriptionPopulated = true;
                return cb(null, subscriptionData);
            });
        }

        this.Company = Company;

    });