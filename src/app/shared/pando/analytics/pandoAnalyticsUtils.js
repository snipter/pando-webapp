angular.module('pando.analytics')
    .service('pandoAnalyticsUtils', function(pandoUtils){
        "ngInject";
        
        var m = 'pandoAnalyticsUtils';

        /*============ Getting realtime events ============*/

        function realtimeGetEventsAndSignalStrengthForDate(realtimeEvents, date){
            var filteredRealtimeEvents = realtimeFilterEventsForDate(realtimeEvents, date);
            if(_.isError(filteredRealtimeEvents)) return filteredRealtimeEvents;
            if(filteredRealtimeEvents.length === 0) return [];
            filteredRealtimeEvents = pandoUtils.clone(filteredRealtimeEvents);
            _.each(filteredRealtimeEvents, function(realtimeEvent){
                var sn = realtimeGetEventSignalStrengthForDate(realtimeEvent, date);
                realtimeEvent.signalStrength = sn;
                if(realtimeEvent.snTimeline) delete realtimeEvent.snTimeline;
            });
            return filteredRealtimeEvents;
        }

        function realtimeGetEventSignalStrengthForDate(realtimeEvent, date){
            if(!realtimeEvent) return new Error('realtimeEvent not specified');
            if(!realtimeEvent.eventStartTime) return new Error('realtimeEvent.eventStartTime not specified');
            if(!realtimeEvent.duration) return new Error('realtimeEvent.duration not specified');
            if(!realtimeEvent.snTimeline) return new Error('realtimeEvent.snTimeline not specified');
            if(!_.isArray(realtimeEvent.snTimeline)) return new Error('wrong realtimeEvent.snTimeline format');
            if(!date) return new Error('date not specified');
            if(!_.isDate(date)) return new Error('wrong date format');

            var timeline = _.sortBy(realtimeEvent.snTimeline, function(timelineItem){
                return timelineItem.ts;
            });

            var timestamp = date.getTime();
            var eventStartDate = realtimeParseDate(realtimeEvent.eventStartTime);
            var eventStartTimestamp = eventStartDate.getTime();
            var eventDuration = realtimeEvent.duration;
            if(timestamp > (eventStartTimestamp + eventDuration)) return 0;

            for(var i = timeline.length - 1; i >= 0; i--){
                var timelineItem = timeline[i];
                if((eventStartTimestamp + timelineItem.ts) <= timestamp) return timelineItem.sn;
            }

            return 0;
        }

        function realtimeFilterEventsForDate(events, date){
            if(events === undefined) return new Error('events not specified');
            if(date === undefined) return new Error('date not specified');
            if(!_.isArray(events)) return new Error('wrong events format, array expected');
            if(!_.isDate(date)) return new Error('wrong date format, date expected');
            return _.filter(events, function(realtimeEvent){
                var timestamp = date.getTime();
                var eventStartDate = realtimeParseDate(realtimeEvent.eventStartTime);
                var eventStartTimestamp = eventStartDate.getTime();
                var eventDuration = realtimeEvent.duration;
                if((timestamp >= eventStartTimestamp) && (timestamp <= (eventStartTimestamp + eventDuration))) return true;
                return false;
            });
        }

        function realtimeParseDate(dateStr){
            if(!dateStr) return new Error('dateStr not specified');
            if(!_.isString(dateStr))  return new Error('dateStr format error');
            var dateFormat = "YYYY-MM-DDTHH:mm:ssZZ";
            return moment(dateStr, dateFormat).toDate();
        }

        /*============ Labels ============*/

        function realtimeGetEventMainLabel(event){
            var visit = 'visit';
            var bounce = 'bounce';
            var walkby = 'walkby';

            if(!event.labels) return walkby;
            if(!_.isArray(event.labels)) return walkby;
            if(_.contains(event.labels, walkby)) return walkby;
            if(_.contains(event.labels, bounce)) return bounce;
            if(_.contains(event.labels, visit)) return visit;
            return walkby;
        }

        /*============ Random float ============*/

        var itemIds = {};

        function realtimeGetRandomFloatForId(id){
            if(!(_.isNumber(id) || _.isString(id))) return null;
            var val = itemIds[id];
            if(val) return val;
            val = Math.random();
            itemIds[id] = val;
            return val; 
        }

        /*============ Export Realtime ============*/

        this.realtime = {
             getEventsAndSignalStrengthForDate: realtimeGetEventsAndSignalStrengthForDate
            ,filterEventsForDate: realtimeFilterEventsForDate
            ,getEventSignalStrengthForDate: realtimeGetEventSignalStrengthForDate
            ,parseDate: realtimeParseDate
            ,getRandomFloatForId: realtimeGetRandomFloatForId
            ,getEventMainLabel: realtimeGetEventMainLabel
        }

    });