angular.module('pando.analytics', ['pando.api', 'pando.utils'])

    .factory('PandoPeriod', function(pandoUtils, pandoApi){
        "ngInject";

        function PandoPeriod(startDate, endDate){
            this.start = startDate;
            this.end = endDate;
            this.check();
        }

        /*
            Creating past period 
            Supported period names: day, week, month
            Offset: how many days, weeks, months move from current date
        */

        PandoPeriod.getPastPeriodWithName = function(periodName, offset){
            var periodEnd = moment();
            if(offset) periodEnd = periodEnd.subtract(offset, periodName);
            var periodStart = offset ? moment().subtract(offset + 1, periodName) : moment().subtract(1, periodName);
            return new PandoPeriod(periodStart.toDate(), periodEnd.toDate());
        }

        PandoPeriod.prototype.toString = function(){
            return 'start: ' + this.start.toString() + ' end: ' + this.end.toString();
        }

        /* Return period type: hour, day, month, year */
        PandoPeriod.prototype.preferedResolution = function(){
            this.check();
            var diff = this.end.getTime() - this.start.getTime();
            if(diff <= pandoUtils.day) return pandoApi.analytics.resolutions.hour;
            else return pandoApi.analytics.resolutions.calendarDay;
        }

        PandoPeriod.prototype.check = function(){
            if(!this.start) throw new Error('period start not set');
            if(!this.end) throw new Error('period end not set');
            if(!_.isDate(this.start)) throw new Error('period start value wrong format');
            if(!_.isDate(this.end)) throw new Error('period start value wrong format');
        }

        return PandoPeriod;
    })

    .factory('PandoAnalytics', function(pandoApi, pandoUtils, PandoPeriod){
        "ngInject";

        var m = 'PandoAnalytics';

        function PandoAnalytics(targets){
            if(!_.isArray(targets)) targets = [targets];
            if(targets === undefined) throw new Error('targets not specified');
            this.targets = targets;
        }

        PandoAnalytics.filters  = pandoApi.analytics.filters;
        PandoAnalytics.events  = pandoApi.analytics.filters;
        PandoAnalytics.eventTypes  = pandoApi.analytics.filters;
        PandoAnalytics.resolutions = pandoApi.analytics.resolutions;

        PandoAnalytics.arrayAverage = function(arr){
            if(!arr) return 0;
            if(!arr.length) return 0;
            return PandoAnalytics.arraySum(arr) / arr.length;
        }

        PandoAnalytics.arraySum = function(arr){
            if(!arr) return 0;
            if(!arr.length) return 0;
            return _.reduce(arr, function(memo, num){ return memo + num; }, 0);
        }

        PandoAnalytics.valuesChangingInProcents = function(prev, cur){
            return PandoAnalytics.valuesChanging(prev, cur) * 100;
        }

        PandoAnalytics.valuesChanging = function(prev, cur){
            return (cur - prev) / prev;
        }

        PandoAnalytics.fixHourOfDayCountData = function(data){
            var sortedValues = {};
            for(var i = 0; i <= 23; i++){
                sortedValues[i] = 0;
                if(data && (data[i] !== undefined)){
                    sortedValues[i] = data[i];
                }
            }
            return sortedValues;
        }

        PandoAnalytics.fixDayOfWeekCountData = function(data){
            var sortedValues = {};
            for(var i = 1; i <= 7; i++){
                sortedValues[i] = 0;
                if(data && (data[i] !== undefined)){
                    sortedValues[i] = data[i];
                }
            }
            return sortedValues;
        }

        PandoAnalytics.fixCalendarDayCountData = function(data, period){
            var startTs = period.start.getTime();
            var endTs = period.end.getTime();
            var sortedValues = {};
            for(var i = startTs; i < endTs; i += pandoUtils.day){
                var dateStr = moment(i).format('YYYY-MM-DD');
                sortedValues[dateStr] = 0;
                if(data && (data[dateStr] !== undefined)){
                    sortedValues[dateStr] = data[dateStr];
                }
            }
            return sortedValues;
        }

        PandoAnalytics.prototype.hourOfDayEventsCount = function(eventName, period, cb){
            var self = this;
            var reqData = self.getTargetAndPeriodReqData(period.start, period.end);
            var resolution = PandoAnalytics.resolutions.hourOfDay;
            pandoApi.analytics.events.count(eventName, resolution, reqData, function(err, data){
                if(err) return cb(err);
                if(!data.values) return cb(new Error('values property not set'));
                var sortedValues = PandoAnalytics.fixHourOfDayCountData(data.values);
                return cb(null, sortedValues);
            });
        }

        PandoAnalytics.prototype.dayOfWeekEventsCount = function(eventName, period, cb){
            var self = this;
            var reqData = self.getTargetAndPeriodReqData(period.start, period.end);
            var resolution = PandoAnalytics.resolutions.dayOfWeek;
            pandoApi.analytics.events.count(eventName, resolution, reqData, function(err, data){
                if(err) return cb(err);
                if(!data.values) return cb(new Error('values property not set'));
                var values = PandoAnalytics.fixDayOfWeekCountData(data.values);
                return cb(null, values);
            })
        }

        PandoAnalytics.prototype.totalEventsCount = function(eventName, period, cb){
            var self = this;
            var reqData = self.getTargetAndPeriodReqData(period.start, period.end);
            var resolution = period.preferedResolution();
            pandoApi.analytics.events.count(eventName, resolution, reqData, function(err, data){
                if(err) return cb(err);
                if(!data.values) return cb(new Error('values property not set'));
                var sum = PandoAnalytics.arraySum(_.values(data.values));
                return cb(null, sum);
            })
        }

        PandoAnalytics.prototype.eventsCount = function(eventName, period, resolution, cb){
            var self = this;
            var reqData = self.getTargetAndPeriodReqData(period.start, period.end);
            pandoApi.analytics.events.count(eventName, resolution, reqData, function(err, data){
                if(err) return cb(err);
                if(!data.values) return cb(new Error('values property not set'));
                var values = data.values;
                if(resolution === PandoAnalytics.resolutions.hourOfDay){
                    values = PandoAnalytics.fixHourOfDayCountData(values);
                }
                if(resolution === PandoAnalytics.resolutions.dayOfWeek){
                    values = PandoAnalytics.fixDayOfWeekCountData(values);
                }
                if(resolution === PandoAnalytics.resolutions.calendarDay){
                    values = PandoAnalytics.fixCalendarDayCountData(values, period);
                }
                return cb(null, values);
            })
        }

        PandoAnalytics.prototype.avarageEventsDuration = function(eventName, period, cb){
            var self = this;
            var reqData = self.getTargetAndPeriodReqData(period.start, period.end);
            var resolution = period.preferedResolution();
            pandoApi.analytics.events.duration(eventName, resolution, reqData, function(err, data){
                if(err) return cb(err);
                if(!data.values) return cb(new Error('values property not set'));
                var average = PandoAnalytics.arrayAverage(_.values(data.values));
                return cb(null, average);
            });
        }

        /*============ Pando id info ============*/

        PandoAnalytics.prototype.pandoIdRawEventsList = function(pandoId, period, cb){
            var self = this;
            var reqData = self.getTargetAndPeriodReqData(period.start, period.end);
            pandoApi.analytics.pandoId.events.list(pandoId, reqData, function(err, resData){
                if(err) return cb(err);
                if(!resData) return cb(null, []);
                if(!resData.history) return cb(null, []);
                return cb(null, resData.history);
            });
        }

        /*============ Basic Functionality ============*/

        PandoAnalytics.prototype.getTargetAndPeriodReqData = function(startDate, endDate){
            var reqData = this.getTargetReqData();
            reqData.startDate = startDate;
            reqData.endDate = endDate;
            return reqData;
        }

        PandoAnalytics.prototype.getTargetReqData = function(){
            var self = this;
            if(!self.targets) throw new Error('targets not specified');
            var reqData = {};
            _.each(self.targets, function(target){
                if(target.deviceId !== undefined){
                    if(!reqData.devices){
                        reqData.devices = [];
                    }
                    reqData.devices.push(target.deviceId);
                }else if(target.locationId !== undefined){
                    if(!reqData.location){
                        reqData.location = [];
                        reqData.location.push(target.locationId);
                    }
                }else{
                    throw new Error('unsupported target type');
                }
            });
            _.each(reqData, function(val, key){
                if(!_.isArray(val)) return;
                reqData[key] = _.reduce(val, function(memo, val){
                    if(memo === '') return val;
                    else return memo + ',' + val;
                }, '');
            });
            return reqData;
        }

        return PandoAnalytics;
    });