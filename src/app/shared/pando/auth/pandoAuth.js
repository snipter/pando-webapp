/*

Pando Auth

Service for managing user auth

*/

angular.module('pando.auth', ['pando.api', 'pando.orm', 'pando.storage'])
	.service('pandoAuth', function($rootScope, pandoApi, pandoOrm, pandoParamsStorage, PandoErr){
		"ngInject";

		var self = this;
		var m = 'pandoAuth';

		/*============ Notify names ============*/

		self.notify = {
			login: 'auth:login',
			logout: 'auth:logout'
		}

		/*============ Local param names ============*/

		self.local = {
			authData: 'data:auth'
		}

		/*============ Auth ============*/

		self.login = function(email, password, cb){
		    log.debug('login with email: ' + email + ', pass: ' + password, m);
		    pandoApi.auth.logIn(email, password, function(err, authData){
		        if(err) return log.err(err, m, cb);
		        self.loginUserWithAuthData(authData, function(err){
		        	if(err) return cb(err);
		        	log.debug('login done', m);
		        	return cb();
		        });
		    });
		}

		self.signup = function(data, cb){
		    log.debug('singup with data: ' + JSON.stringify(data), m);
		    var email = data.email;
		    pandoApi.auth.signUp(data, function(err, authData){
		        if(err) return log.err(err, m, cb);
		        log.debug('singup done', m);
		        self.loginUserWithAuthData(authData, function(err){
		        	if(err) return cb(err);
		        	log.debug('singup done', m);
		        	return cb();
		        });
		    });
		}

		self.signupAndJoinCompany = function(data, cb){
		    log.debug('signup and join company with data: ' + JSON.stringify(data), m);
		    pandoApi.auth.signupAndJoinCompany(data, function(err, authData){
		        if(err) return log.err(err, m, cb);
		        log.debug('singup and join done', m);
		        self.loginUserWithAuthData(authData, function(err){
		        	if(err) return cb(err);
		        	log.debug('singup and join done', m);
		        	return cb();
		        });
		    });
		}

		self.loginUserWithAuthData = function(authData, cb){
			self.initCurrentUserWithAuthData(authData, function(err){
	        	if(err) return cb(err);
	        	emitLogin();
	        	return cb();
	        });
		}

		/*============ Logout ============*/

		self.logout = function(){
			log.debug('logout', m);
			self.removeAuthDataFromStorage();
			self.removeCurrentUser();
			emitLogout();
		    pandoApi.auth.logOut(function(err){
		    	if(err) log.err(err, m);
		    	pandoApi.removeAccessToken();
		    	log.debug('logout done', m);
		    });
		}

		/*============ Password ============*/

		self.resetPass = function(email, cb){
			log.debug('reset pass form email: ' + email, m);
			pandoApi.auth.pass.reset(email, function(err){
			    if(err) return log.err(err, m, cb);
			    log.debug('reset pass done', m);
			    return cb();
			});
		}

		self.restorePass = function(token, newPass, cb){
		    log.debug('restore password with token: ' + token, m);
		    pandoApi.auth.pass.restore(token, newPass, function(err, authData){
		        if(err) return log.err(err, m, cb);
		        log.debug('restore password done', m);
		        return cb();
		    });
		}

		/*============ Current user ============*/

		self.currentUser = null;
		self.currentAuthData = null;

		self.initCurrentUser = function(cb){
		    self.initCurrentUserFromParamsStorage(cb);
		};

		self.initCurrentUserFromParamsStorage = function(cb){
		    var authData = self.loadAuthDataFromStorage();
		    if(!authData) return cb();
		    self.initCurrentUserWithAuthData(authData, cb);
		};

		self.initCurrentUserWithAuthData = function(authData, cb){
			const authErr = self.getAuthDataErr(authData);
			if(authErr) return log.err(authErr, m, cb);

			if(self.isAccessTokenExpired(authData.expiration)){
				log.debug('current access token expired', m);
				return cb();
			}

			log.debug('init current user with auth data: ' + JSON.stringify(authData), m);

			self.setAccessToken(authData.accessToken);

			const checkCurrentAccessToken = function(cb){
				if(!self.requiredRefreshAccessToken(authData.expiration)) return cb();
				log.debug('refreshing access token', m);
				pandoApi.auth.refreshToken(function(err, newAuthData){
					if(err) return cb(err);
					const newAuthErr = self.getAuthDataErr(newAuthData);
					if(newAuthErr) return log.err(newAuthErr, m, cb);
					log.debug('refreshing access token done', m);
					authData = newAuthData;
					return cb();
				});
			}

			const initCurrentUser = function(cb){
				pandoOrm.User.get(authData.userId, function(err, user){
					if(err) return log.err(err, m, cb);
					if(!user) return log.err(PandoErr.userNotFound({userId: authData.userId}), m, cb);

					log.debug('init current user with auth data done', m);

					self.removeCurrentUser();
					self.setCurrentUser(user, authData);

					self.saveAuthDataToStorage(authData);

					return cb();
				});
			}

			async.series([checkCurrentAccessToken, initCurrentUser], cb);
		};

		self.setCurrentUser = function(user, authData){
		    self.currentUser = user;
			self.setCurrentAuthData(authData);
			self.startAccessTokenAutorefresh();
		};

		self.removeCurrentUser = function(){
		    self.currentUser = null;
			self.removeCurrentAuthData();
			self.stopAccessTokenAuthorefresh();
		};

		self.setCurrentAuthData = function(authData){
			self.currentAuthData = authData;
		}

		self.removeCurrentAuthData = function(){
			self.currentAuthData = null;
		}

		self.getAuthDataErr = function(authData){
			if(!authData) return PandoErr.paramMissed('auth data required');
			if(!authData.accessToken) return PandoErr.paramMissed('accessToken required');
			if(!authData.userId) return PandoErr.paramMissed('userId required');
			if(!authData.expiration) return PandoErr.paramMissed('expiration required');
			return null;
		}

		/*============ Storage ============*/

		self.saveAuthDataToStorage = function(authData){
			log.debug('saving auth data to storage', m);
			pandoParamsStorage.set(self.local.authData, authData);
		};

		self.loadAuthDataFromStorage = function(){
		    var authData = pandoParamsStorage.get(self.local.authData);
		    if(!authData) return null;
		    if(!authData.accessToken) return null;
		    if(!authData.userId) return null;
		    if(!authData.expiration) return null;
		    return authData;
		};

		self.removeAuthDataFromStorage = function(){
			log.debug('removing auth data from storage', m);
			pandoParamsStorage.remove(self.local.authData);
		};

		/*============ Access Token ============*/

		// token refresh time = 1 minute
		self.accessTokenRefreshTime = 1 * 60 * 1000;

		self._accessTokenAutorefreshHandler = null;

		self.startAccessTokenAutorefresh = function(){
			if(self._accessTokenAutorefreshHandler){
				self.stopAccessTokenAuthorefresh();
			}
			log.debug('starting access token autorefresh', m);
			self._accessTokenAutorefreshHandler = setInterval(function(){
				self.accessTokenAutorefreshProcessing();
			}, self.accessTokenRefreshTime);
		}

		self.stopAccessTokenAuthorefresh = function(){
			if(!self._accessTokenAutorefreshHandler) return;
			log.debug('stopping access token autorefresh', m);
			clearInterval(self._accessTokenAutorefreshHandler);
			self._accessTokenAutorefreshHandler = null;
		}

		self.accessTokenAutorefreshProcessing = function(){
			// return if current user not set
			if(!self.currentUser) return;
			// return if refresh not reuqired
			log.trace('processing access token autorefresh', m);
			if(!self.currentAuthData) return log.warn('currentAuthData empty', m);
			if(!self.currentAuthData.expiration) return log.warn('currentAuthData.expiration empty', m);
			if(!self.requiredRefreshAccessToken(self.currentAuthData.expiration)){
				return log.trace('not required access token refresh', m);
			}
			// refreshing token
			self.refreshCurrentAccessToken(function(err){
				if(err) return log.err(err, m);
			});
		}

		self.requiredRefreshAccessToken = function(expirationDate){
			if(!expirationDate) return log.err('expirationDate empty', m);
			if(_.isString(expirationDate)){
				expirationDate = new Date(expirationDate);
			};
			if(!expirationDate) return log.err('expirationDate parsing error', m);
			return self.isAccessTokenExpired(expirationDate, self.accessTokenRefreshTime);
		};

		self.isAccessTokenExpired = function(expirationDate, expirationTimeShift){
			if(!expirationDate){ log.err('expirationDate empty', m); return true; }
			if(!expirationTimeShift) expirationTimeShift = 0;
			if(_.isString(expirationDate)){
				expirationDate = new Date(expirationDate);
			};
			if(!expirationDate){ log.err('expirationDate parsing error', m); return true; }
			const currentDate = new Date();
			const currentTimeStamp = currentDate.getTime();
			const expirationDateTimestamp = expirationDate.getTime() - expirationTimeShift;
			if(currentTimeStamp > expirationDateTimestamp) return true;
			else return false;
		}

		self.refreshCurrentAccessToken = function(cb){
			log.debug('refreshing access token', m);
		    pandoApi.auth.refreshToken(function(err, authData){
		        if(err) return cb(err);
		        if(authData.accessToken){
		        	log.debug('refreshing access token done', m);
		        	self.setAccessToken(authData.accessToken);
					self.setCurrentAuthData(authData);
		        	self.saveAuthDataToStorage(authData);
		        }
		        else log.warn('refresh token error - accessToken key not set');
		        return cb();
		    });
		};

		self.setAccessToken = function(accessToken){
		    pandoApi.setAccessToken(accessToken);
		};

		/*============ Emit ============*/

		function emitLogout(){
		    $rootScope.$broadcast(self.notify.logout);
		}

		function emitLogin(){
		    $rootScope.$broadcast(self.notify.login);
		}

		/*============ Events ============*/

		pandoApi.on(pandoApi.notify.req.error, function(err){
			// logout on 403 error
			if(err && err.code && (err.code == 403)){
				log.debug('403 error - need logout', m);
				self.logout();
			}
			
		});

	});