angular.module('pando.subscription', [])
	.service('pandoSubscription', function(){
		"ngInject";

		var self = this;

		self.deviceCost = 200;

		self.plans = {
		     pilot: { name: 'Pilot Plan', price: 0.0, planId: 'pilot'}
		    ,standart: { name: 'Standart Monthly Plan', price: 34.00, planId: 'fsmm'}
		}

		self.subscriptionPlanWithId = function(planId){
		    var plansArray = _.values(self.plans);
		    return _.find(plansArray, function(plan){
		        return plan.planId == planId;
		    });
		}

		self.subscriptionTotalDue = function(plan, devicesCount){
		    var planId = null;
		    if(plan.planId){
		        planId = plan.planId;
		    }else if(plan.id){
		        planId = plan.id;
		    }else if(typeof plan === 'string'){
		        planId = plan;
		    }else{
		        throw new Error('wrong subscription plan format');
		    }
		    var planData = self.subscriptionPlanWithId(planId);
		    if(!planData) throw new Error('plan with id "' + planId + '" not found');
		    return self.subscriptionDevicesCost(devicesCount) + planData.price;
		}

		self.subscriptionDevicesCost = function(devicesCount){
		    return devicesCount * self.deviceCost;
		}

	});