angular.module('app')
    .directive('timetostr', function(){
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, element, attr, ngModel) {
                function fromUser(date) {
                    return moment(date).format('HH:mm');
                }

                function toUser(str) {
                    if(!str) return new Date();
                    var reg = /(\d+)\:(\d+)/g;
                    var match = reg.exec(str);
                    if(!match) return new Date();
                    return dateWithHoursAndMinutes(parseInt(match[1]), parseInt(match[2]));
                }

                function dateWithHoursAndMinutes(hours, minutes){
                    var d = new Date();
                    d.setHours(hours,minutes,0,0);
                    return d;
                }

                ngModel.$parsers.push(fromUser);
                ngModel.$formatters.push(toUser);
            }
        };
    });