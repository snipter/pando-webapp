angular.module('app')
	.directive('companyEditForm', function(){
		return {
			restrict: 'E',
			scope: {company:'=', valid:'=', disabled:'@'},
			templateUrl: 'app/shared/companyEditForm/companyEditFormView.html',
			replace: true,
			link: function($scope, element, attrs, ctrl){

				var m = 'companyEditForm';

				$scope.$watch('companyEditForm.$valid', function(newVal, oldVal){
					$scope.valid = newVal;
				});

				$scope.addressChanged = function(){
					if(!$scope.company || !$scope.company.address || !$scope.company.address.placeId) return;
					log.debug('address changed: ' + JSON.stringify($scope.company.address), m);

					var addrs = $scope.company.address;
					$scope.company.country = addrs.country;
					$scope.company.state = addrs.state;
					$scope.company.city = addrs.city;
					$scope.company.address = addrs.formatted;
				}

			}
		}
	});