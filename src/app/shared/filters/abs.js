angular.module('app')
	.filter('abs', function() {
	    return function(num) { return Math.abs(num); }
	});