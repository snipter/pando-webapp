angular.module('app')
    .constant('consts', {
        local: {
            user: {
                firstAppActivated: 'user:firstAppActivated',
                firstGraphicsUploaded: 'user:firstGraphicsUploaded'
            },
            mirrorEnabled: 'mirrorEnabled',
            urls: {
                redirect: 'urls:redirect'
            },
            auth: {
                email: 'auth:email'
            }
        },
        notify:{
            admin: {
                realtime:{
                    update: 'admin:realtime:update'
                }
            },
            apps: {
                data: {
                    updated: 'app:data:updated'
                },
                action: {
                    add: 'app:action:add',
                    item: {
                        clicked: 'app:action:item:clicked'
                    }
                },
                item: {
                    'add': 'app:item:add'
                },
                items: {
                    'add': 'app:items:add',
                    'updated': 'app:items:updated'
                }
            }
        }
    });