angular.module('app')
	.constant('gmtTimeZones', [
         {name: 'GMT-12', val: 'GMT-12'}
        ,{name: 'GMT-11', val: 'GMT-11'}
        ,{name: 'GMT-10', val: 'GMT-10'}
        ,{name: 'GMT-9', val: 'GMT-9'}
        ,{name: 'GMT-8', val: 'GMT-8'}
        ,{name: 'GMT-7', val: 'GMT-7'}
        ,{name: 'GMT-6', val: 'GMT-6'}
        ,{name: 'GMT-5', val: 'GMT-5'}
        ,{name: 'GMT-4', val: 'GMT-4'}
        ,{name: 'GMT-3', val: 'GMT-3'}
        ,{name: 'GMT-2', val: 'GMT-2'}
        ,{name: 'GMT-1', val: 'GMT-1'}
        ,{name: 'GMT+0', val: 'GMT+0'}
        ,{name: 'GMT+1', val: 'GMT+1'}
        ,{name: 'GMT+2', val: 'GMT+2'}
        ,{name: 'GMT+3', val: 'GMT+3'}
        ,{name: 'GMT+4', val: 'GMT+4'}
        ,{name: 'GMT+5', val: 'GMT+5'}
        ,{name: 'GMT+6', val: 'GMT+6'}
        ,{name: 'GMT+7', val: 'GMT+7'}
        ,{name: 'GMT+8', val: 'GMT+8'}
        ,{name: 'GMT+9', val: 'GMT+9'}
        ,{name: 'GMT+10', val: 'GMT+10'}
        ,{name: 'GMT+11', val: 'GMT+11'}
        ,{name: 'GMT+12', val: 'GMT+12'}
    ]);