angular.module('app', [
    // Angular modules
     'ngAnimate'
    ,'ngAria'
    ,'ngMessages'
    ,'ngCookies'
    ,'ngResource'
    
    // 3rd Party Modules
    ,'ngMaterial'
    ,'ui.router'
    ,'ui.bootstrap'
    ,'duScroll'
    ,'google.places'
    ,'uiGmapgoogle-maps'
    ,'ngFileUpload'
    ,'mdColorPicker'
    ,'ui.sortable'
    ,'checklist-model'
    ,'ngMaterialDatePicker'
    ,'chart.js'

    // Pando Modules
    ,'pando.utils'
    ,'pando.api'
    ,'pando.storage'
    ,'pando.auth'
    ,'pando.subscription'
    ,'pando.orm'
    ,'pando.analytics'
    
    // App Modules
    ,'app.env'
    ,'app.templates'
]);