angular.module('app')
    .controller('locationDashboardCtrl', function($scope, $state, pagesService){
        "ngInject";

        var m = 'locationDashboardCtrl';

       	$scope.currentLoc = null;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            updateCurrentLoc();
        }

        init();
        
        /*============ Events ============*/

        $scope.$watch('userDataLoaded', function(newVal){
            if(!newVal) return resetCurrentLoc();
            else updateCurrentLoc();
        });

        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options){
            updateCurrentLoc();
        });

        /*============ Location ============*/

        function updateCurrentLoc(){
            log.debug('updating currentLoc', m);
            if(!$scope.user) return resetCurrentLoc();
            if($state.params.locId){
                var loc = $scope.user.locationWithId($state.params.locId);
                if(loc) setCurrentLoc(loc);    
                else pagesService.loadRootPage();
            }else{
                if($scope.user.locations && $scope.user.locations.length){
                    var loc = $scope.user.locations[0];
                    $state.go('locationWithId', {locId: loc.id});
                }else{
                    resetCurrentLoc();
                }
            }
        }

        function setCurrentLoc(location){
            $scope.currentLoc = location;
        }

        function resetCurrentLoc(){
            $scope.currentLoc = null;
        }

    });