angular.module('app')
    .controller('locationDashboardDeviceCtrl', function($scope, toastMsg){
        "ngInject";

        var m = 'locationDashboardDeviceCtrl';

        $scope.appsLoading = false;
        $scope.devices = [];
        $scope.currentDevice = null;

        /*============ Watch ============*/

        $scope.$watch('currentLoc', function(newVal){
        	if(newVal) updateDevices();
        	else resetDevices();
        });

        /*============ UI Events ============*/

        $scope.deviceTabClick = function($event, device){
        	log.debug('device tab clicked', m);
            setCurrentDevice(device);
        }

        $scope.deviceAppsWeightChanged = function($event, device){
            log.debug('device apps weight changed', m);
            saveAppsData();
        }

        /*============ Devices ============*/

        function updateDevices(){
        	$scope.devices = $scope.user.devicesAtLocation($scope.currentLoc);
        	if($scope.devices.length){
        		setCurrentDevice($scope.devices[0]);
        	}else{
        	    setCurrentDevice(null);
        	}
        }

        function setCurrentDevice(device){
        	$scope.currentDevice = device;
        	if(!device) return;
            if($scope.currentDevice.appsPopulated) return;
        	log.debug('updating apps list', m);
        	$scope.appsLoading = true;
        	device.populateAppsAndInstallUninstalled(function(err){
        		$scope.appsLoading = false;
        		if(err) { norify.err('Apps data loading error.'); return log.err(err, m); }
        		log.debug('updating apps list done', m);
        	});
        }

        function resetDevices(){
        	$scope.appsLoading = false;
        	$scope.devices = [];
        	$scope.currentDevice = null;
        }

        /*============ Apps ============*/

        function saveAppsData(){
            if(!$scope.currentDevice) return;
            log.debug('saving apps data', m);
            async.each($scope.currentDevice.activeApps, function(app, cb){
                app.save(cb);
            }, function(err){
                if(err) { norify.err('Saving apps data error.'); return log.err(err, m); }
                log.debug('saving apps data', m);
                toastMsg.success('Apps data updated!');
            });
        }

    });