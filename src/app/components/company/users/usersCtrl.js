angular.module('app')
    .controller('companyUsersCtrl', function($scope, pandoApi, toastMsg, dialogs){
        "ngInject";

        var m = 'companyUsersCtrl';

        $scope.loading = false;
        $scope.permissions = _.values(pandoApi.devices.permissions);

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Events ============*/

        $scope.$watch('user', function(newVal, oldVal){
            if(newVal){
                log.debug('updating user data', m);
                updateUserData(function(err){
                    if(err){
                        toastMsg.dataLoadingErr();
                        return log.err(err, m);
                    }
                    log.debug('updating user data done', m);
                });
            }
        });

        $scope.inviteUserClicked = function(){
            log.debug('invite user clicked', m);
            var opt = {
                templateUrl: 'app/components/company/users/inviteDialogView.html',
                controller: 'companyInviteUsersCtrl',
                resolve: {user: $scope.user}
            }
            dialogs.modal(opt);
        }

        $scope.removeUserClicked = function($event, user){
            log.debug('remove user clicked', m);
            var opt = {
                 title: 'Remove user'
                ,content: 'Are you sure you want to remove user with email: ' + user.email
                ,ok: 'Remove'
            }
            dialogs.confirm($event, opt, function(){
                log.debug('remove user confirmed', m);
                $scope.user.removeFromUsers(user, function(err){
                    if(err){
                        log.err(err, m);
                        return toastMsg.err('Removing user err');
                    }
                    toastMsg.success('User removed succesfully!');
                });
            });
        }

        $scope.saveUserClicked = function(user){
            log.debug('save users clicked', m);
            log.debug('updating user', m);
            // updating user permissions
            const currentCompanyId = $scope.user.companyId;
            if(user.currentCompanyPermission){
                const currentCompanyPermissionRole = user.currentCompanyPermission;
                const currentCompanyPermissionItem = _.find(user.permissions, function(item){
                    return item.companyId == currentCompanyId;
                });
                if(currentCompanyPermissionItem){
                    currentCompanyPermissionItem.role = currentCompanyPermissionRole
                }else{
                    user.permissions.push({companyId: currentCompanyId, role: currentCompanyPermissionRole});
                }
            }
            // saving user
            user.save(function(err){
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Saving user err');
                }
                log.debug('updating user done', m);
                toastMsg.success('User saved succesfully!');
            });
        }

        /*============ Functions ============*/

        function updateUserData(cb){
            if(!$scope.user) return cb();
            $scope.user.populateUsers(cb);
        }


    });