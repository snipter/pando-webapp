angular.module('app')
    .controller('companyInviteUsersCtrl', function($scope, $uibModalInstance, user, pandoApi){
        "ngInject";

        var m = 'companyInviteUsersCtrl';

        $scope.processing = false;
        $scope.emailWarning = false;
        $scope.email = '';
        $scope.role = pandoApi.company.permissions.manager.val;
        $scope.roles = _.values(pandoApi.company.permissions);

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Events ============*/

        $scope.ok = function() {
            log.debug('add display clicked', m);
            inviteUserByEmail()
        };

        $scope.cancel = function() {
            log.debug('cancel clicked', m);
            $uibModalInstance.dismiss("cancel");
        };

        $scope.emailKeyPressed = function($event){
            if (($event.which === 13) && ($scope.inviteUserForm.$valid)){
                inviteUserByEmail();
            }
        }

        $scope.dataChaned = function(){
            $scope.emailWarning = false;
        }

        /*============ Functions ============*/

        function inviteUserByEmail(){
            log.debug('inviting user', m);
            var data = {email: $scope.email, permissions: []};
            
            $scope.emailWarning = false;
            $scope.processing = true;
            user.addToUsers($scope.email, $scope.role, function(err){
                $scope.processing = false;
                if(err){
                    $scope.emailWarning = true;
                    return log.err(err, m);
                }
                log.debug('inviting user done', m);
                $uibModalInstance.close();
            });
        } 
    }); 