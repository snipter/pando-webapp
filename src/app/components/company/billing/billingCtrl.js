angular.module('app')
    .controller('companyBillingCtrl', function($scope, pandoApi, toastMsg){
        "ngInject";
        
        var m = 'companyBillingCtrl';

        $scope.loading = false;
        $scope.processing = false;
        $scope.billing = {
            "billingAddress": {
                "billingFirstName": "",
                "billingLastName": "",
                "phone": "",
                "address": "",
                "address2": "",
                "city": "",
                "state": "",
                "country": "",
                "zip": ""
            }
        };

        /*============ Events ============*/

        $scope.$watch('company', function(newVal, oldVal){
            if(!$scope.company) return;
            if((!$scope.company.companyName) || ($scope.company.companyName == "")) return;
            updateCompanyBilling();
        });

        $scope.billingFormSubmited = function($event){
            log.debug('billing form submited', m);
            saveBilling();
        }

        /*============ Functions ============*/

        function updateCompanyBilling(){
            if(!$scope.user) return;
            log.debug('updating company billing', m);
            $scope.loading = true;
            pandoApi.getCompanyBilling(function(err, companyBilling){
                $scope.loading = false;
                if(err){return log.err(err, m);}
                log.debug('updating company billing done', m);
                log.trace(companyBilling, m);
                $scope.billing = companyBilling;
                // setupDropInContainer();
            });
        }

        function setupDropInContainer(){
            log.debug('getting client payment token', m);
            pandoApi.getBillingPaymentToken(function(err, tokenData){
                if(err){return log.err(err, m);}
                log.debug('getting client payment token done', m);
                var token = tokenData.token;
                log.trace(token, m);
                log.debug('setup dropIn container', m);
                braintree.setup(token, "dropin", {
                  container: "dropin-container",
                  onPaymentMethodReceived: function(obj){
                    log.debug('payment method received', m);
                    var nonce = obj.nonce;
                    saveBillingWithNonce(nonce);
                  }
                });
            });
        }

        function saveBillingWithNonce(nonce){
            $scope.processing = true;
            pandoApi.updateCompanyBilling($scope.billing, function(err, companyBilling){
                $scope.processing = false;
                if(err){log.err(err, m); return toastMsg.err('Updating company billing data error')}
                $scope.companyBilling = companyBilling;
                return toastMsg.success('Company billing data updated successfuly');
                // if(!nonce) return toastMsg.error('Payment nonce not set');
                // log.debug('sending nonce to server', m);
                // pandoApi.submitPaymentMethodNonce(nonce, function(err){
                //     if(err){log.err(err, m); return toastMsg.err('Sending nonce to server error')}
                //     log.debug('sending nonce to server done', m);
                //     return toastMsg.success('Company billing data updated successfuly');
                // });
            });
        }

        function saveBilling(){
            $scope.processing = true;
            pandoApi.updateCompanyBilling($scope.billing, function(err, companyBilling){
                $scope.processing = false;
                if(err){log.err(err, m); return toastMsg.err('Updating company billing data error')}
                $scope.companyBilling = companyBilling;
                return toastMsg.success('Company billing data updated successfuly');
                // if(!nonce) return toastMsg.error('Payment nonce not set');
                // log.debug('sending nonce to server', m);
                // pandoApi.submitPaymentMethodNonce(nonce, function(err){
                //     if(err){log.err(err, m); return toastMsg.err('Sending nonce to server error')}
                //     log.debug('sending nonce to server done', m);
                //     return toastMsg.success('Company billing data updated successfuly');
                // });
            });
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

    });