angular.module('app')
   .controller('companyAddDeviceDialog', function($scope, $uibModalInstance, toastMsg, company){
        "ngInject";

        var m = 'companyAddDeviceDialog';

        $scope.devicesCount = 1;
        $scope.processing = false;

        /*============ Modal ============*/

        $scope.cancel = function() {
           log.debug('cancel clicked', m);
           dismissDialog();
        };

        $scope.add = function(){
            log.debug('add clicked', m);
            addDevices();
        }

        $scope.devicesCountPressed = function($event){
            if (($event.which === 13) && ($scope.editForm.$valid)){
                addDevices();
            }
        }

        /*============ Functions ============*/

        function addDevices(){
            log.debug('buying additional devices', m);
            $scope.processing = true;
            company.buyAdditionalDevices($scope.devicesCount, function(err, planData){
                $scope.processing = false;
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Buying additional devices error');
                }
                log.debug('buying additional devices done', m);
                dismissDialog();
            });
        }

        /*============ Dialog ============*/

        function dismissDialogWithData(data){
            $uibModalInstance.close(data);
        }

        function dismissDialog(){
            $uibModalInstance.dismiss("cancel");
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();
    });