angular.module('app')
    .controller('companySubscriprtionCtrl', function($scope, pandoApi, toastMsg, dialogs){
        "ngInject";

        var m = 'companySubscriprtionCtrl';

        $scope.loading = false;
        $scope.processing = false;
        $scope.subscriptionPlans = _.filter(_.values(pandoApi.subscriptionPlans), function(plan){
            return plan.planId != 'pilot';
        });

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Events ============*/

        $scope.$watch('user.company', function(newVal, oldVal){
            if(newVal){
                updateSubscriptionData();
            }
        });

        $scope.subscribeForPlanClicked = function($event, plan){
            log.debug('subscribe clicked', m);
            var opt = {
                title: 'Subscribe for plan', 
                text: 'Are you sure you want to subscribe for plan: ' + plan.name + '?',
                ok: 'Ok'
            };
            dialogs.confirm($event, opt, function(){
                log.debug('subscribe confirmed', m);
                subscribeForPlan(plan.planId);
            });
        }

        $scope.unsubscribeClicked = function($event){
            log.debug('unsubscribe clicked', m);
            var opt = {
                title: 'Unsubscribe', 
                text: 'Are you sure you want cancel your subscription?',
                ok: 'Ok'
            };
            dialogs.confirm($event, opt, function(){
                log.debug('unsubscribe comfirmed', m);
                cancelSubscription();
            });
        }

        $scope.addDevicesClicked = function($event){
            log.debug('add devices clicked', m);
            showAddDevicesDialog();
        }

        /*============ Functions ============*/

        function updateSubscriptionData(){
            if(!$scope.user) return;
            if(!$scope.user.company) return;
            if($scope.user.subscriptionPopulated) return;
            log.debug('updating subsciption', m);
            $scope.loading = true;
            $scope.user.company.populateSubscription(function(err){
                $scope.loading = false;
                if(err){
                    toastMsg.err('Loading subscription data error');
                    return log.err(err, m);
                }
                log.debug('updating subsciption done', m);
            });
        }

        function cancelSubscription(){
            if(!$scope.user) return;
            if(!$scope.user.company) return;
            log.debug('canceling subsciption', m);
            $scope.processing = true;
            $scope.user.company.cancelSubscription(function(err){
                $scope.processing = false;
                if(err){
                    toastMsg.err('Canceling subsciption error');
                    return log.err(err, m);
                }
                log.debug('canceling subsciption done', m);
                toastMsg.success('Canceling subsciption done');
            });
        }

        function subscribeForPlan(planId){
            if(!$scope.user) return;
            if(!$scope.user.company) return;
            log.debug('subscribing for plan', m);
            $scope.loading = true;
            $scope.user.company.subscribeForPlanWithId(planId, function(err){
                $scope.loading = false;
                if(err){
                    toastMsg.err('Starting subsciption error');
                    return log.err(err, m);
                }
                log.debug('subscribing for plan done', m);
                toastMsg.success('Subscription started succesfuly!');
            });
        }

        function showAddDevicesDialog(){
            if(!$scope.user) return;
            if(!$scope.user.company) return;
            var opt = {
                templateUrl: 'app/components/company/subscription/addDeviceDialogView.html',
                controller: 'companyAddDeviceDialog',
                resolve: {
                    company: $scope.user.company
                }
            }
            dialogs.modal(opt);
        }

        $scope.planIdToName = function(planId){
            var plan = _.find(_.values(pandoApi.subscriptionPlans), function(plan){return plan.planId == planId});
            return plan ? plan.name : '';
        }

    });