angular.module('app')
    .controller('companyInfoCtrl', function($scope, toastMsg){
        "ngInject";
        
        var m = 'companyInfoCtrl';
        
        $scope.saving = false;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Events ============*/

        $scope.$watch('user', function(newVal, oldVal){
            if(!newVal){
               $scope.saving = false;
            }
        });

        $scope.saveCompanyClicked = function(){
            log.debug('save company clicked', m);
            $scope.saving = true;
            $scope.user.company.save(function(err){
                $scope.saving = false;
                if(err){
                    toastMsg.err('Updating company data error');
                    return log.err(err, m);
                }
                toastMsg.success('Company data updated');
            });
        }

    });