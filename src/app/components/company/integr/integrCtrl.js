angular.module('app')
    .controller('companyIntegrCtrl', function($scope, $window, $interval, toastMsg, pandoApi) {
        "ngInject";
        
        var m = 'companyIntegrCtrl';
        var statuses = pandoApi.company.integrations.stauses;

        $scope.loading = false;

        /*============ Watch ============*/

        $scope.$watch('user.company', function(newVal, oldVal){
            if(newVal){
                updateCompanyIntegrs();
            }
        });

        /*============ UI Events ============*/

        $scope.disableItegrClicked = function(itegr){
            log.debug('disable integr clicked', m);
            if(!$scope.company) return cb();
            if(!$scope.company.id) return cb();
            itegr.loading = true;
            $scope.company.removeIntegration(itegr, function(err){
                itegr.loading = false;
                if(err){
                    toastMsg.dataSavingErr();
                    return log.err(err, m);
                }
                toastMsg.dataSaved();
                log.debug('updating integrations', m);
                updateCompanyIntegrs();
            });
        };

        $scope.enableItegrClicked = function(itegr){
            log.debug('enable integr clicked', m);
            if(!$scope.user.company) return;
            if(!$scope.user.company.id) return;
            if(!itegr.url) return;
            var win = $window.open(itegr.url);
            win.onbeforeunload = function(){
                log.debug('window closed', m);
                $scope.$apply(function(){
                    log.debug('updating integrations', m);
                    updateCompanyIntegrs();
                });
            }
        }

        /*============ Integrs ============*/

        function updateCompanyIntegrs(){
            if(!$scope.user) return;
            if(!$scope.user.company) return;
            if($scope.user.company.integrationsPopulated) return;
            log.debug('updating company integrations', m);
            $scope.user.company.populateIntegrations(function(err){
                log.debug('updating company integrations done', m);
                if(err){
                    toastMsg.err('Loading integrations error');
                    return log.err(err, m);
                }
                $scope.user.company.updateIntegrationsUrls(function(err){
                    if(err) return log.err(err, m);
                });
            });
        }

        /*============ Autoupdate ============*/

        var updateHandler = null;

        function startIntegrsUpdate(){
            log.debug('starting integrations update', m);
            updateHandler = $interval(function(){
                updateCompanyIntegrs();
            }, 1000 * 10);
        }

        function stopIntegrsUpdate(){
            if(!updateHandler) return;
            log.debug('stopping integrations update', m);
            $interval.cancel(updateHandler);
            updateHandler = null;
        }

        /*============ Providesr data ============*/

        $scope.providerName = function(provider){
            if(!provider.providerName) return;
            var name = provider.providerName;
            if(name == 'square') return 'Square';
            if(name == 'clover') return 'Clover';
            return '';
        }

        $scope.providerDesc = function(provider){
            if(!provider.providerName) return;
            var name = provider.providerName;
            if(name == 'square') return 'Square is one service for your entire business, from secure credit card processing';
            if(name == 'clover') return 'Clover handles the business part of being in business';
            return '';
        }

        $scope.providerUrl = function(provider){
            if(!provider.providerName) return;
            var name = provider.providerName;
            if(name == 'square') return 'https://squareup.com/global/en/pos';
            if(name == 'clover') return 'https://www.clover.com/';
            return '#';
        }

        $scope.providerImgUrl = function(provider){
            if(!provider.providerName) return;
            var name = provider.providerName;
            if(name == 'square') return '/assets/img/company/integr/logo-square.png';
            if(name == 'clover') return '/assets/img/company/integr/logo-clover.png';
            return '';
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            startIntegrsUpdate();
        }

        init();

        /*============ Destroy ============*/

        $scope.$on('$destroy', function(){
            log.debug('destroy', m);
            stopIntegrsUpdate();
        });

    });