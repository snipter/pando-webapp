angular.module('app')
    .controller('SigninCtrl', function ($scope, consts, paramsStorage, pandoAuth, toastMsg){
        "ngInject";

        var m = 'SigninCtrl';

        $scope.data = {
            email: '',
            password: ''
        }

        $scope.focus = {
             email: true
            ,pass: false
        }

        $scope.loading = false;
        $scope.authWarning = false;

        /*============ UI Events ============*/

        $scope.fieldKeyPressed = function($event, filedName){
            if($event.which === 13){
                if(filedName == 'email') $scope.focus.pass = true;
                if(filedName == 'pass') signIn();
            }
            $scope.authWarning = false;
        }

        $scope.submitClicked = function () {
            log.debug('signin clicked', m);
            signIn();
        };

        /*============ Signin ============*/

        function signIn(){
            paramsStorage.set(consts.local.auth.email, $scope.data.email);
            $scope.loading = true;
            pandoAuth.login($scope.data.email, $scope.data.password, function(err, userData){
                $scope.loading = false;
                if(err && err.status == 403){
                    $scope.focus.email = true;
                    return $scope.authWarning = true;
                }
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Unknow error');
                }
                $scope.passwordWarning = false;
            });
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            initEmail();
        }

        function initEmail(){
            var email = paramsStorage.get(consts.local.auth.email);
            if(email){
                $scope.data.email = email;
                $scope.focus.email = false;
                $scope.focus.pass = true;
            }
        }

        init();
    });