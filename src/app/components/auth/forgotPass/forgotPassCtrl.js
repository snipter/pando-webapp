angular.module('app')
    .controller('ForgotPassCtrl', function($scope, $stateParams, pagesService, consts, paramsStorage, pandoAuth, utils, dialogs){
        "ngInject";
        
        var m = 'ForgotPassCtrl';

        $scope.focus = {
             email: false
            ,pass: false
        }

        $scope.err = {
             reset: false
            ,restore: false
        }

        $scope.email = '';
        $scope.pass = '';
        $scope.emailToken = null;

        $scope.loading = false;

        /*============ UI Events ============*/

        $scope.fieldKeyPressed = function($event, filedName){
            if($event.which === 13){
                if(filedName == 'email') resetPassword();
                if(filedName == 'pass') restorePassword();
            }
            $scope.authWarning = false;
        }

        $scope.submitClicked = function(){
            log.debug('submit clicked', m);
            if(!$scope.emailToken) resetPassword();
            else restorePassword();
        }

        /*============ Functions ============*/

        function resetPassword(){
            log.debug('resetting password', m);
            hideErrors();
            paramsStorage.set(consts.local.auth.email, $scope.email);
            $scope.loading = true;
            pandoAuth.resetPass($scope.email, function(err, data){
                $scope.loading = false;
                if(err){
                    $scope.err.reset = true;
                    return log.err(err, m);
                }
                log.debug('resetting password done', m);
                var dialogOpt = {
                     title: 'Resetting Password'
                    ,content: 'The password has been reseted. Please check your email to complete reset.'
                }
                log.debug('showing dialog', m);
                dialogs.message(dialogOpt, function(){
                    log.debug('showing dialog done', m);
                    pagesService.loadSigninPage();
                });
            });
        }

        function restorePassword(){
            log.debug('restoring password', m);
            hideErrors();
            $scope.loading = true;
            pandoAuth.restorePass($scope.emailToken, $scope.pass, function(err, data){
                $scope.loading = false;
                if(err){
                    $scope.err.restore = true;
                    return log.err(err, m);
                }
                log.debug('restoring password done', m);
                pagesService.loadSigninPage();
            });
        }

        function hideErrors(){
            $scope.err.reset = false;
            $scope.err.restore = false;
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            initToken();
            initEmail();
            if($scope.emailToken){
                $scope.focus.pass = true;
            }else{
                $scope.focus.email = true;
            }
        }

        function initEmail(){
            var email = paramsStorage.get(consts.local.auth.email);
            if(email){
                $scope.email = email;
            }
        }

        function initToken(){
            if($stateParams.token) $scope.emailToken = $stateParams.token;
        }

        init();
    });