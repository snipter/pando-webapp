angular.module('app')
    .controller('SignupCtrl', function($scope, pagesService, pandoAuth, $stateParams, utils){
        "ngInject";
        
        var m = 'SignupCtrl';

        $scope.focus = {
             firstName: true
            ,lastName: false
            ,email: false
            ,pass: false
        }

        $scope.data = {
            email: '',
            password: '',
            firstName: '',
            lastName: ''
        }

        $scope.loading = false;

        $scope.emailTakedWarning = false;
        $scope.signUpWarning = false;

        /*============ UI Events ============*/

        $scope.fieldKeyPressed = function($event, filedName){
            if($event.which === 13){
                if(filedName == 'firstName') $scope.focus.lastName = true;
                if(filedName == 'lastName'){
                    if($scope.data.emailToken){
                        $scope.focus.pass = true;
                    }else{
                        $scope.focus.email = true;
                    }
                }
                if(filedName == 'email') $scope.focus.pass = true;
                if(filedName == 'pass') signUp();
            }
            hideErrors();
        }

        $scope.submitClicked = function () {
            log.debug('signin clicked', m);
            signUp();
        };

        /*============ Sign Up ============*/

        function signUp(){
            hideErrors();

            var user = utils.clone($scope.data);
            $scope.loading = true;
            if(user.emailToken){
                delete user.email
                pandoAuth.signupAndJoinCompany(user, singUpCallback);
            }else{
                pandoAuth.signup(user, singUpCallback);
            }

            function singUpCallback(err, data){
                $scope.loading = false;
                if(err) log.err(err, m);
                if(err && err.status == 403) return $scope.emailTakedWarning = true;
                if(err) return $scope.signUpWarning = true;
                hideErrors();
                pagesService.loadRootPage();
            }
        }

        function hideErrors(){
            $scope.emailTakedWarning = false;
            $scope.signUpWarning = false;
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            if($stateParams.token){
                $scope.data.emailToken = $stateParams.token;
                log.debug('signup with email token: ' + $stateParams.token, m);
            }
        }

        init();
    });