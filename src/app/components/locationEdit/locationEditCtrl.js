angular.module('app')
    .controller('locationEditCtrl', function($scope, $state, pagesService, dialogs, toastMsg){
        "ngInject";

        var m = 'locationEditCtrl';

       	$scope.currentLoc = null;
        $scope.locData = null;

        $scope.saveInProcess = false;
        $scope.removeInProcess = false;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            updateCurrentLoc();
        }

        init();
        
        /*============ Events ============*/

        $scope.$watch('userDataLoaded', function(newVal){
            if(!newVal) return resetCurrentLoc();
            else updateCurrentLoc();
        });

        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options){
            updateCurrentLoc();
        });

        /*============ UI Events ============*/

        $scope.saveClicked = function($event){
            log.debug('save clicked', m);
            $scope.currentLoc.setData($scope.locData);
            $scope.saveInProcess = true;
            log.debug('saving location', m);
            $scope.currentLoc.save(function(err){
                $scope.saveInProcess = false;
                if(err){log.err(err, m); toastMsg.err('Location saving error.')}
                log.debug('saving location done', m);

                toastMsg.success('Location saved');
            });
        }

        $scope.removeClicked = function($event){
            log.debug('remove clicked', m);
            var opt = {title: 'Remove location', text: 'Are you really want to remove this location?', ok: 'Remove'}
            dialogs.confirm($event, opt, function(){
                log.debug('location removing confirmed', m);
                log.debug('removing location', m);
                $scope.removeInProcess = true;
                $scope.currentLoc.remove(function(err){
                    $scope.removeInProcess = false;
                    if(err){log.err(err, m); toastMsg.err('Location removing error.')}
                    log.debug('removing location done', m);

                    $scope.user.locations = _.filter($scope.user.locations, function(item){
                        return item.id != $scope.currentLoc.id;
                    });
                    $scope.currentLoc = null;

                    pagesService.loadRootPage();
                });
            });
        }

        /*============ Location ============*/

        function updateCurrentLoc(){
            log.debug('updating currentLoc', m);
            if(!$scope.user) return resetCurrentLoc();
            if($state.params.locId){
                var loc = $scope.user.locationWithId($state.params.locId);
                if(loc) setCurrentLoc(loc);    
                else pagesService.loadRootPage();
            }else{
                if($scope.user.locations && $scope.user.locations.length){
                    var loc = $scope.user.locations[0];
                    $state.go('locationWithId', {locId: loc.id});
                }else{
                    resetCurrentLoc();
                }
            }
        }

        function setCurrentLoc(location){
            $scope.currentLoc = location;
            $scope.locData = location.data();
        }

        function resetCurrentLoc(){
            $scope.currentLoc = null;
        }

    });