angular.module('app')
    .controller('headerCtrl', function($scope, pandoAuth) {
        "ngInject";

        var m = 'HeaderCtrl';
        
        $scope.logout = function () {
            log.debug('logout clicked', m);
            pandoAuth.logout();
        };

        $scope.currentUserEmail = function(){
            if(pandoAuth.currentUser){
                return pandoAuth.currentUser.email;
            }
        }

    });