angular.module('app')
    .controller('locationsBarCtrl', function($scope, $state) {
        "ngInject";

        var m = 'locationsBarCtrl';

        $scope.currentLoc = null;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            updateCurrentLoc();
        }

        init();
        
        /*============ Events ============*/

        $scope.$watch('userDataLoaded', function(newVal){
            if(!newVal) return resetCurrentLoc();
            else updateCurrentLoc();
        });

        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options){
            updateCurrentLoc();
        });

        /*============ Functions ============*/

        function updateCurrentLoc(){
            log.debug('updating currentLoc', m);
            if(!$scope.user) return resetCurrentLoc();
            if(!$state.params.locId) return resetCurrentLoc();
            $scope.currentLoc = $scope.user.locationWithId($state.params.locId);
        }

        function resetCurrentLoc(){
            $scope.currentLoc = null;
        }

    });