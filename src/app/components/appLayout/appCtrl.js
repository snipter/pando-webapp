angular.module('app')
    .controller('AppCtrl', function($rootScope, $scope, $location, $state, activityIndicator, env, mainLoader, pandoApi, pandoAuth, consts, paramsStorage, pagesService, dialogs, UserParamsStorage){
        "ngInject";

        $scope.env = env;
        
        $scope.user = null;
        $scope.userParams = null;
        $scope.userDataLoaded = false;

        var m = 'AppCtrl';

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            initActivitiIndicator();
            initCurrentUser();
        }

        init();

        /*============ Activity indicator ============*/

        function initActivitiIndicator(){
            pandoApi.on(pandoApi.notify.req.begin, function(opt){
                if((opt.method == 'GET') && (opt.url.indexOf('/rest/company/integrations/') > 0)) return;
                if((opt.method == 'GET') && (/\/rest\/device\/\d+\/realtime\//g.test(opt.url))) return;
                activityIndicator.show();
            });
            pandoApi.on(pandoApi.notify.req.end, function(){
                activityIndicator.hide();
            });
        }

        /*============ User ============*/

        function initCurrentUser(){
            log.debug('init current user', m);
            
            pandoAuth.initCurrentUser(function(err){
                if(err) log.err(err, m);
                log.debug('init current user done', m);
                hideMainLoader();
                if(!pandoAuth.currentUser){
                    log.debug('user not signed in', m);
                    var currentUrl = $location.url();
                    log.debug('required sign in: ' + pagesService.isUrlRequiredSignIn(currentUrl), m);
                    if(pagesService.isUrlRequiredSignIn(currentUrl)){
                        pagesService.loadSigninPage();
                    }
                }else{
                    log.debug('user signed in', m);
                    setCurrentUser(pandoAuth.currentUser);
                }
            });
        }

        function setCurrentUser(user){
            $scope.user = user;
            $scope.userParams = new UserParamsStorage(user);
            $scope.userDataLoaded = false;
            populateCurrentUserData(function(err){
                $scope.userDataLoaded = true;
                if(err) return toastMsg.err('Loading user data error');
                log.debug('populating user data done', m);
            });
        }

        function populateCurrentUserData(cb){
            var user = $scope.user;
            log.debug('populating current user data', m);
            async.parallel([function(cb){
                log.debug('loading locations', m);
                user.populateLocations(function(err){
                    if(err) return log.err(err, m, cb);
                    log.debug('loading locations done', m);
                    return cb();
                });
            }, function(cb){
                log.debug('loading devices', m);
                user.populateDevices(function(err){
                    if(err) return log.err(err, m, cb);
                    log.debug('loading devices done', m);
                    return cb();
                });
            }, function(cb){
                log.debug('loading company', m);
                user.populateCompany(function(err){
                    if(err) return log.err(err, m, cb);
                    log.debug('loading company done', m);
                    return cb();
                });
            }], function(err){
                if(err) return cb(err);
                log.debug('populating current user data done', m);
                return cb();
            });
        }

        function cleanCurrentUser(){
            $scope.user = null;
            $scope.userParams = null;
            $scope.userDataLoaded = false;
        }
        
        /*============ Auth Events ============*/

        $scope.$on(pandoAuth.notify.login, function(){
            log.debug('login event', m);
            setCurrentUser(pandoAuth.currentUser);
            pagesService.loadRootPage();
            hideMainLoader();
        });

        $scope.$on(pandoAuth.notify.logout, function(){
            log.debug('logout event', m);
            cleanCurrentUser();
            dialogs.dismissAll();
            pagesService.loadSigninPage();
            hideMainLoader();
        });

        /*============ Main loader ============*/

        function hideMainLoader(){
            mainLoader.hide();
        }

        /*============ Layout events ============*/

        $rootScope.$on("$stateChangeSuccess", function (event, currentRoute, previousRoute) {
            pagesService.scrollToTop();
        });

    });