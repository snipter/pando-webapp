angular.module('app')
    .service('activityIndicator', function($rootScope){
        "ngInject";
        
        var m = 'activityIndicator';

        var c = 0;

        this.show = function() {
            // log.debug('show: ' + c, m);
            c++;
            if(c > 1) return;
            $rootScope.$broadcast('preloader:active');
        }
        this.hide = function() {
            // log.debug('hide: ' + c, m);
            if(c <= 0){c = 0; return;}
            c--;
            if(c == 0) $rootScope.$broadcast('preloader:hide');
        }
    })
    .directive('uiPreloader', function($rootScope){
        "ngInject";
        return {
            restrict: 'A',
            template:'<span class="bar"></span>',
            link: function(scope, el, attrs) {        
                el.addClass('app-preloaderbar hide');
                scope.$on('$stateChangeStart', function(event) {
                    el.removeClass('hide').addClass('active');
                });
                scope.$on('$stateChangeSuccess', function( event, toState, toParams, fromState ) {
                    event.targetScope.$watch('$viewContentLoaded', function(){
                        el.addClass('hide').removeClass('active');
                    })
                });

                scope.$on('preloader:active', function(event) {
                    el.removeClass('hide').addClass('active');
                });
                scope.$on('preloader:hide', function(event) {
                    el.addClass('hide').removeClass('active');
                });                
            }
        };        
    });

    