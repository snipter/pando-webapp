angular.module('app')
    .controller('settingsProfileCtrl', function($scope, toastMsg){
        "ngInject";

        var m = 'settingsProfileCtrl';
        var defPass = 'f8ea2456189555a';

        $scope.processing = false;
        $scope.confUser = {};

        /*============ Events ============*/

        $scope.saveClicked = function(){
            log.debug('save clicked');
            $scope.processing = true;
            saveUserData(function(err){
                $scope.processing = false;
                if(err){
                    toastMsg.dataSavingErr();
                    return log.err(err, m);
                }
                return toastMsg.dataSaved();
            });
        }

        $scope.$watch('user', function(newVal, oldVal){
            if(!newVal){
                $scope.confUser = null;
            }else{
                $scope.confUser = $scope.user.getData();
                $scope.confUser.password = defPass;
            }
        });

        /*============ Functions ============*/

        function saveUserData(cb){
            if(!$scope.user) return;
            async.parallel([
                function(cb){
                    $scope.user.setData($scope.confUser);
                    log.debug('saving user data', m);
                    $scope.user.save(function(err){
                        if(err) return cb(err);
                        log.debug('saving user data done', m);
                        return cb();
                    });
                }, function(cb){
                    var pass = $scope.confUser.password;
                    if(pass == defPass) return cb();
                    log.debug('changing user password', m);
                    $scope.user.changePassword(pass, function(err){
                        if(err) return cb(err);
                        log.debug('changing user password done', m);
                        $scope.confUser.password = defPass;
                        return cb();
                    });
                }
            ], cb);
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();
    });