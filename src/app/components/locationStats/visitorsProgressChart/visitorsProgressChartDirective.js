angular.module('app')
	.directive('visitorsProgressChart', function(){
		"ngInject";
		return {
			restrict: 'E',
			scope: {procent: '='},
			template: "<span><span data-ng-repeat='item in items' class='visitors-progress-chart__item' data-ng-class='{\"showed\": item.showed, \"hidden\": !item.showed}'></span></span>",
			replace: true,
			link: function($scope, element, attrs, ctrl){

				var m = 'visitorsProgressChart';

				/*============ Init ============*/

				log.debug('init', m);

				/*============ Watch model ============*/

				$scope.$watch('procent', function(newValue, oldValue){
	                if(newValue){
	                	showProcent(newValue);
	                }else{
	                	showProcent(0);
	                }
	            }, true);

				/*============ Items ============*/

				$scope.items = [];

				for(var i = 0; i < 10; i++){
					$scope.items.push({showed: false});
				}

				function showProcent(procent){
					_.each($scope.items, function(item, index){
						var val = index * 10 + 5;
						item.showed = val <= procent ? true : false;
					});
				}

		        /*============ Destroy ============*/

		        $scope.$on('$destroy', function(){
		            log.debug('destroy', m);
		        });
		        
			}
		}
	});