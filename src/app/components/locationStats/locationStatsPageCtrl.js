angular.module('app')
    .controller('locationStatsCtrl', function($scope, $state, pagesService){
        "ngInject";
        
        var m = 'locationStatsCtrl';

        $scope.currentLoc = null;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            updateCurrentLoc();
        }

        init();
        
        /*============ Events ============*/

        $scope.$watch('userDataLoaded', function(newVal){
            if(!newVal) return resetCurrentLoc();
            else updateCurrentLoc();
        });

        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options){
            updateCurrentLoc();
        });

        /*============ Functions ============*/

        function updateCurrentLoc(){
            log.debug('updating currentLoc', m);
            if(!$scope.user) return resetCurrentLoc();
            if(!$state.params.locId) return resetCurrentLoc();
            var loc = $scope.user.locationWithId($state.params.locId);
            if(loc) setCurrentLoc(loc);
            else pagesService.loadRootPage();
        }

        function setCurrentLoc(loc){
        	$scope.currentLoc = loc;
        }

        function resetCurrentLoc(){
            $scope.currentLoc = null;
        }

    });