angular.module('app')
    .controller('visitByDayOfWeekChartCtrl', function($scope, appConfig){
        "ngInject";
        
        var m = 'visitByDayOfWeekChartCtrl';

        $scope.chart = {
            series: [
                 'Previous period'
                ,'Current period'
            ]
            ,labels: [
                "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
            ]
            ,colors: [
                 appConfig.color.gray
                ,appConfig.color.greenLight
            ]
            ,options: {
                 scales: {
                    xAxes: [{
                        gridLines: {
                            display: false
                        }
                        ,ticks: {
                            fontColor: '#323232'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                             display: true
                            ,labelString: 'Number of visitors'
                            ,fontColor: appConfig.color.gray
                        }
                        ,ticks: {
                             fontColor: '#323232'
                            ,min: 0
                            ,beginAtZero: true
                        }
                    }]
                }
            }
            ,data: [
                 [0, 0, 0, 0, 0, 0, 0]
                ,[0, 0, 0, 0, 0, 0, 0]
            ]
            ,datasetOverride: [
                {
                    backgroundColor: appConfig.color.gray
                }
                ,{
                    backgroundColor: appConfig.color.greenLight
                }
            ]
        };

        /*============ Watch ============*/

        $scope.$watch('analyticsData', function(newVal){
            updateChart();
        });

        /*============ Chart ============*/

        function updateChart(){
            var dayOfWeek = $scope.analyticsData.dayOfWeek;
            $scope.chart.data = [
                 _.values(dayOfWeek.prev.visits)
                ,_.values(dayOfWeek.cur.visits)
            ];
            log.trace($scope.chart.data, m);
        }

        
    });