angular.module('app')
    .controller('conversionChartCtrl', function($scope, appConfig, PandoPeriod, PandoAnalytics){
    	"ngInject";
    	
	    var m = 'conversionChartCtrl';

	    /*============ Doughnut Chart ============*/

	    $scope.doughnutChart = {
	    	 labels: ['Walkby', 'Bounces', 'Visits']
	    	,colors: [
	    		 appConfig.color.greenLight
	    		,appConfig.color.violet
	    		,appConfig.color.gray
	    	]
	    	,options: {
	    		scales: {
	                xAxes: [{
	                    display: false
	                }],
	                yAxes: [{
	                    display: false
	                }]
	            }
	    	}
	    	,data: [1, 1, 1]
	    }

	    /*============ Line chart ============*/

	    var hoursChartLabels = [
	    	 "00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00"
        	,"10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00"
        	,"20:00" ,"21:00" ,"22:00" ,"23:00"
        ];

        var dayOfWeekChartLabels = [
        	"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
        ];

	    $scope.lineChart = {
	    	 series: ['Bounces', 'Visits', 'Walkby']
	    	,labels: hoursChartLabels
	    	,colors: [
	    		 appConfig.color.violet
	    		,appConfig.color.gray
	    		,appConfig.color.greenLight
	    	]
	    	,options: {
	    		 scales: {
		            xAxes: [{
		                ticks: {
		                	fontColor: appConfig.color.greenLight
		                }
		            }],
		            yAxes: [{
		            	gridLines: {
		            		display: false
		            	}
		            	,ticks: {
		            		 min: 0
                        	,beginAtZero: true
		            		,fontColor: appConfig.color.greenLight
		            	}
		            	,scaleLabel: {
		            		 display: true
		            		,labelString: 'Users'
		            		,fontColor: appConfig.color.greenLight
		            	}
		            }]
		        }
	    	}
	    	,data: [
	    		 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	    		,[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	    		,[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	    	]
	    	,datasetOverride: [
	    		 {fill: false}
	    		,{}
	    		,{}
	    	]
	    }

	    /*============ Watch ============*/

	    $scope.$watch('analyticsData', function(newVal){
	    	updateChart();
	    });

	    /*============ Chart ============*/

	    function updateChart(){
	    	// Dought chart
	    	var eventsCountData = $scope.analyticsData.eventsCount;
	    	var doughnutChartData = [
	    		 eventsCountData.cur.walkbys
	    		,eventsCountData.cur.bounces
	    		,eventsCountData.cur.visits
	    	]
	    	if(!doughnutChartData[0] && !doughnutChartData[1] && !doughnutChartData[2]){
	    		$scope.doughnutChart.data = [1,1,1];
	    	}else{
	    		$scope.doughnutChart.data = doughnutChartData;
	    	}
	    	
	    	// Linear chrt
	    	var periodEventsCount = $scope.analyticsData.periodEventsCount;
	    	var walkbysData = _.values(periodEventsCount.walkbys);
	    	var bouncesData = _.values(periodEventsCount.bounces);
	    	var visitsData = _.values(periodEventsCount.visits);
	    	$scope.lineChart.data = [bouncesData, visitsData, walkbysData];
	    	// Selecting labels
	    	if($scope.period == 'day'){
	    		$scope.lineChart.labels = hoursChartLabels;
	    	}else if($scope.period == 'week'){
	    		$scope.lineChart.labels = dayOfWeekChartLabels;
	    	}else if($scope.period == 'month'){
	    		var datesLabels = _.keys(periodEventsCount.walkbys);
	    		datesLabels = _.map(datesLabels, function(label){
	    			var match = /(\d+)-(\d+)-(\d+)/g.exec(label);
	    			if(match) return match[2] + '/' + match[3] + '/' + match[1];
	    			else return label;
	    		});
	    		$scope.lineChart.labels = datesLabels;
	    	}
	    	
	    }

	});