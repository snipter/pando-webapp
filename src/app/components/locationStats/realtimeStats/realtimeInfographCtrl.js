angular.module('app')
    .controller('realtimeInfographCtrl', function($scope, pandoAnalyticsUtils){
        "ngInject";

        var m = 'realtimeInfographCtrl';
        var self = this;

        /*============ Events ============*/

        $scope.chartRealtimeEvents = [];

        $scope.$watch('realtimeEvents', function(newVal, oldVal){
            $scope.chartRealtimeEvents = [];
            if(newVal){
                if(!_.isArray(newVal)) return;
                if(newVal.length == 0) return;
                $scope.chartRealtimeEvents = self.modificateRealtimeEvents(newVal);
            }
        });

        self.modificateRealtimeEvents = function(events){
            _.each(events, function(event){
                if(event.angle === undefined){
                    event.angle = pandoAnalyticsUtils.realtime.getRandomFloatForId(event.eventId);
                }
                if(!event.eventMainLabel){
                    event.eventMainLabel = pandoAnalyticsUtils.realtime.getEventMainLabel(event);
                }
            });
            return events;
        }

        /*============ UI Functions ============*/

        $scope.realtimeEventsProcentByType = function(eventType){
            var count = $scope.realtimeEventsCountByType(eventType);
            if(count == 0) return 0;
            var procent = Math.round((count / $scope.getRealtimeEvents().length) * 100);
            return procent;
        }

        $scope.realtimeEventsCountByType = function(eventType){
            return $scope.realtimeEventsWithType(eventType).length;
        }

        $scope.realtimeEventsWithType = function(eventType){
            return _.filter($scope.getRealtimeEvents(), function(item){
                if(!item.eventMainLabel) return false;
                return item.eventMainLabel.toLowerCase() == eventType.toLowerCase();
            })
        }

        $scope.getRealtimeEvents = function(){
            if(!$scope.chartRealtimeEvents) return [];
            return $scope.chartRealtimeEvents;
        }

    });