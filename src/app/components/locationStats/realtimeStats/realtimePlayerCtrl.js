angular.module('app')
    .controller('realtimePlayerCtrl', function($scope, $interval){
        "ngInject";
        
        var m = 'realtimePlayerCtrl';

        $scope.realtimePlayValToStr = function(){
            var val = $scope.realtimePlayVal;
            if(!val) return '00:00';
            var minute = 60;
            var hour = minute * 60;
            var hours = Math.floor(val / hour);
            val -= hour * hours;
            var minutes = Math.floor(val / minute);
            var hoursStr = hours > 9 ? hours : '0' + hours;
            var minutesStr = minutes > 9 ? minutes : '0' + minutes;
            return hoursStr + ':' + minutesStr;
        }

    });