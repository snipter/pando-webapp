angular.module('app')
    .controller('realtimeStatsCtrl', function($scope, $interval, $timeout, toastMsg, pandoAnalyticsUtils, dialogs){
        "ngInject";

        var m = 'realtimeStatsCtrl';

        /*============ Watch ============*/

        $scope.$watch('currentLoc', function(newVal, oldVal){
            if(newVal){
                reset();
                playCurrentRealtimeEvents();
            }else{
                reset();
            }
        });

        /*============ UI Events ============*/

        $scope.realtimeEventItemClick = function($event, item){
            log.debug('realtime event item click', m);
            var opt = {
                 templateUrl: 'app/components/locationStats/userHistoryStats/userHistoryDialogView.html'
                ,controller: 'userHistoryDialogCtrl'
                ,backdrop: false
                ,windowClass: 'pando-modal pando-modal_center pando-modal_transp-border'
                ,resolve: {
                     eventItem: item
                    ,device: $scope.display
                }
            }
            dialogs.modal(opt);
        }

        /*============ Realtime ============*/

        $scope.realtimeEvents = null;

        function reset(){
            $scope.realtimeEvents = null;
            resetCurrentRealtimeEventsData();
            resetLastHourRealtimeEventsData();
            resetRealtimePlayer();
        }

        /*============ Player ============*/

        $scope.realtimePlayerLoading = false;
        $scope.realtimePlaying = false;
        $scope.realtimePlayVal = 0;
        $scope.realtimePlayingStart = 0;
        $scope.realtimePlayingEnd = 60 * 60;

        $scope.realtimePlayClick = function(){
            log.debug('realtime play click', m);
            stopCurrentRealtimeAutoupdate();
            showLastHourRealtimeEventsForSecond(0, function(err){
                if(err){
                    return toastMsg.err('Playing last hour error');
                }
                realtimePlayerPlay();
            });
        }

        $scope.realtimePauseClick = function(){
            log.debug('realtime pause click', m);
            realtimePlayerPause();
        }

        $scope.realtimeTimelineHandlerMouseDown = function(){
            realtimePlayerPause();
        }

        $scope.realtimeTimelineValChanged = function($event, val){
            if($scope.realtimePlayerLoading) return;
            log.debug('realtimePlayVal: ' + $scope.realtimePlayVal, m);
            showLastHourRealtimeEventsForSecond($scope.realtimePlayVal);
            stopCurrentRealtimeAutoupdate();
        }

        $scope.realtimePlayValToStr = function(){
            var val = $scope.realtimePlayVal;
            if(!val) return '00:00';
            var minute = 60;
            var hour = minute * 60;
            var hours = Math.floor(val / hour);
            val -= hour * hours;
            var minutes = Math.floor(val / minute);
            var hoursStr = hours > 9 ? hours : '0' + hours;
            var minutesStr = minutes > 9 ? minutes : '0' + minutes;
            return hoursStr + ':' + minutesStr;
        }

        function realtimePlayerPlay(){
            $scope.realtimePlaying = true;
            realtimePlayerIteration();
        }

        function realtimePlayerIteration(){
            if(!$scope.realtimePlaying) return;
            if(!$scope.realtimePlayVal) $scope.realtimePlayVal = 0;
            $scope.realtimePlayVal += 12;
            showLastHourRealtimeEventsForSecond($scope.realtimePlayVal, function(err){
                if(err) return;
                if($scope.realtimePlayVal >= $scope.realtimePlayingEnd){
                    $scope.realtimePlayVal = $scope.realtimePlayingEnd
                    onPlayingFinised();
                    return realtimePlayerPause();
                }
                $timeout(function(){
                    realtimePlayerIteration();
                }, 10);
            });
        }

        function realtimePlayerPause(){
            $scope.realtimePlaying = false;
        }

        function onPlayingFinised(){
            playCurrentRealtimeEvents();
        }

        function resetRealtimePlayer(){
            realtimePlayerPause();
            $scope.realtimePlayerLoading = false;
            $scope.realtimePlaying = false;
            $scope.realtimePlayVal = 0;
            $scope.realtimePlayingStart = 0;
            $scope.realtimePlayingEnd = 60 * 60;
        }

        /*============ Last hour realtime events ============*/

        var savedLastHourRealtimeEvents = null;
        var lastHourUpdateDate = null;

        function resetLastHourRealtimeEventsData(){
            savedLastHourRealtimeEvents = null;
            lastHourUpdateDate = null;
        }

        function showLastHourRealtimeEventsForSecond(secVal, cb){
            if(!lastHourUpdateDate) lastHourUpdateDate = new Date();
            secVal = Math.round(secVal);
            var sec = 60;
            var hour = sec * 60;
            if(secVal > hour) secVal = hour;
            var secToSubtract = hour - secVal;
            var date = moment(lastHourUpdateDate).subtract('seconds', secToSubtract).toDate();
            showLastHourRealtimeEventsForDate(date, cb);
        }

        function showLastHourRealtimeEventsForDate(date, cb){
            getLastHourRealtimeEvents(function(err, events){
                if(err) return log.err(err, m, cb);
                var events = pandoAnalyticsUtils.realtime.getEventsAndSignalStrengthForDate(events, date);
                $scope.realtimeEvents = events;
                if(cb) return cb();
            });
        }

        function getLastHourRealtimeEvents(cb){
            if(!$scope.display) return cb(null, []);
            if(savedLastHourRealtimeEvents) return cb(null, savedLastHourRealtimeEvents);
            if($scope.realtimePlayerLoading) return cb(null, []); 
            $scope.realtimePlayerLoading = true;
            $scope.display.realtimeGetLastHourEvents(function(err, events){
                $scope.realtimePlayerLoading = false;
                if(err) return cb(err);
                savedLastHourRealtimeEvents = events;
                lastHourUpdateDate = new Date();
                return cb(null, events);
            });
        }

        /*============ Current realtime events ============*/

        var savedCurrentRealtimeEvents = null;

        function resetCurrentRealtimeEventsData(){
            savedCurrentRealtimeEvents = null;
            stopCurrentRealtimeAutoupdate();
        }

        function playCurrentRealtimeEvents(){
            showCurrentRealtimeEvents();
            starCurrentRealtimeAutoupdate();
        }

        function showCurrentRealtimeEvents(){
            if(savedCurrentRealtimeEvents){
                $scope.realtimeEvents = savedCurrentRealtimeEvents;
            }
            getCurrentRealtimeEvents(function(err, events){
                if(err) return log.err(err, m);
                savedCurrentRealtimeEvents = events;
                $scope.realtimeEvents = events;
            });
        }

        function getCurrentRealtimeEvents(cb){
            if(!$scope.display) return cb(null, []);
            $scope.display.realtimeGetCurrentEvents(cb);
        }

        /*============ Current realtime autoupdate ============*/

        var currentRealtimeAutoupdateHandler = null;

        function starCurrentRealtimeAutoupdate(){
            log.debug('start statistics autoupdate', m);
            currentRealtimeAutoupdateHandler = $interval(function(){
                showCurrentRealtimeEvents();
            }, 30 * 1000);
        }

        function stopCurrentRealtimeAutoupdate(){
            if(currentRealtimeAutoupdateHandler){
                log.debug('stop statistics autoupdate', m);
                $interval.cancel(currentRealtimeAutoupdateHandler);
                currentRealtimeAutoupdateHandler = null;
            }
        }

        /*============ Events ============*/

        $scope.$on('$destroy', function(){
            reset();
            log.debug('destroy', m);
        });

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

    });