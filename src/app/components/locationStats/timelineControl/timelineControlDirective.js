angular.module('app')
	.directive('timelineControl', function($document){
		"ngInject";
		return {
			restrict: 'E',
			scope: {
				 value:'='
				,valueMin: '@'
				,valueMax: '@'
				,tooltipLeft: '@'
				,tooltipRight: '@'
				,tooltipHandler: '@'
				,onChanged: '&'
				,onHandlerMouseDown: '&'
				,onHandlerMouseUp: '&'
			},
			template:   '<div class="timeline-control">' +
							'<div class="timeline-control__bar">' +
								'<div class="timeline-control-bar">' +
									'<div class="timeline-control-bar__tooltip timeline-control-bar__tooltip_left timeline-control-tooltip timeline-control-tooltip_left" data-ng-show="tooltipLeft">' +
									'{{tooltipLeft}}' +
									'</div>' +
									'<div class="timeline-control-bar__handler">'+
										'<div class="timeline-control-handler" data-ng-style="{\'left\': position + \'%\'}">'+
											'<div class="timeline-control-handler__tip" data-ng-show="tooltipHandler">{{tooltipHandler}}</div>'+
										'</div>' +
									'</div>' +
									'<div class="timeline-control-bar__tooltip timeline-control-bar__tooltip_right timeline-control-tooltip timeline-control-tooltip_right" data-ng-show="tooltipRight">' +
									'{{tooltipRight}}' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>',
			replace: true,
			link: function($scope, element, attrs, ctrl){
				// Module
				var m = 'timelineControl';
				// Bar element
				var timelineBar = angular.element(element[0].getElementsByClassName('timeline-control-bar'));

				// Getting handler element
				var timelineHandler = angular.element(element[0].getElementsByClassName('timeline-control-handler'));

				// Converting functions
				function valueToProcent(value){
					return  (value * 100) / (valueMax() - valueMin())
				}

				function procentToValue(procent){
					return  (procent * (valueMax() - valueMin())) / 100;
				}

				function valueMin(){
					return $scope.valueMin ? parseInt($scope.valueMin) : 0;
				}

				function valueMax(){
					return $scope.valueMax ? parseInt($scope.valueMax) : 0;
				}

				// Init position
				if($scope.value){
					$scope.position = valueToProcent($scope.value);
				}else{
					$scope.position = 0;
				}

				// Handler events
				timelineHandler.on("mousedown", function(event){
					event.preventDefault();

					var startX = event.screenX;
					var startProcent = $scope.position ? parseInt($scope.position) : 0;

					var mouseMove = function (event){
						var offset = event.screenX - startX;
						var offsetProcent = (offset * 100) / timelineBar.prop("clientWidth");
						var newProcent = startProcent + offsetProcent;

						if(newProcent > 100) newProcent = 100;
						if(newProcent < 0) newProcent = 0;
						
						$scope.$apply(function(){
							$scope.position = newProcent;
							$scope.value = procentToValue(newProcent);
							if($scope.onChanged) $scope.onChanged({$event: event, val: $scope.value});
						});
					}
					var mouseUp = function(event){
						$document.unbind("mousemove", mouseMove);
						$document.unbind("mouseup", mouseUp);
						$scope.$apply(function(){
							if($scope.onHandlerMouseUp) $scope.onHandlerMouseUp();
						});
					}

					$document.on("mousemove", mouseMove);
					$document.on("mouseup", mouseUp);

					if($scope.onHandlerMouseDown) $scope.onHandlerMouseDown();
				});

				// Watch
				$scope.$watch('value', function(newVal){
					if(newVal){
						$scope.position = valueToProcent(newVal);
					}else{
						$scope.position = valueToProcent(valueMin());
					}
				});

			}
		}
	});
