angular.module('app')
    .controller('averageShopTimeChartCtrl', function($scope, appConfig, PandoPeriod, PandoAnalytics){
    	"ngInject";
    	
	    var m = 'averageShopTimeChartCtrl';

	    /*============ Chart ============*/

	    $scope.chart = {
	    	 labels: ['New visitors', 'Returning visitors']
	    	,colors: [
	    		 appConfig.color.violetLight
	    		,appConfig.color.violetDark
	    	]
	    	,options: {
	    		scales: {
	                xAxes: [{
	                    display: false
	                }],
	                yAxes: [{
	                    display: false
	                }]
	            }
	    	}
	    	,data: [1, 1]
	    }

	    /*============ Watch ============*/

	    $scope.$watch('analyticsData', function(newVal){
	    	updateChart();
	    });

	    /*============ Chart ============*/

	    function updateChart(){
	    	var data = $scope.analyticsData;
	    	if((data.eventsCount.cur.new == 0) && (data.eventsCount.cur.returning == 0)){
	    		$scope.chart.data = [1, 1];
	    	}else{
	    		$scope.chart.data =  [data.eventsCount.cur.new, data.eventsCount.cur.returning];
	    	}
	    }

	});