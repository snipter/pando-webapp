angular.module('app')
    .controller('userHistoryDialogCtrl', function($scope, $uibModalInstance, eventItem, device){
		"ngInject";
		
	    var m = 'userHistoryDialogCtrl';

	    $scope.eventItem = eventItem;
	    $scope.id = eventItem.pandoId;
	    $scope.device = device;

	    $scope.cancel = function() {
	        log.debug('cancel clicked', m);
	        $uibModalInstance.dismiss("cancel");
	    };

	});