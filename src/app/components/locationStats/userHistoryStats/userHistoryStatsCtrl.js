angular.module('app')
    .controller('userHistoryStatsCtrl', function($scope, appConfig, PandoPeriod, PandoAnalytics, pandoUtils){
    	"ngInject";
    	
	    var m = 'userHistoryStatsCtrl';
	    var self = this;

	    var dayOfWeekChartLabels = [
	    	"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
	    ];

	    $scope.chartResolutions = [
	    	 {name: 'Week', val: 'week'}
	    	,{name: 'Month', val: 'month'}
	    ];

	    $scope.currentChartResolution = $scope.chartResolutions[0];

	    $scope.loading = false;
	    $scope.loadingErr = false;

	    /*============ Data changes ============*/

	    $scope.$watch('eventItem', function(newVal){

	    });

	    /*============ UI Events ============*/

	    $scope.chartResolutionClick = function($event, resolution){
	    	$scope.currentChartResolution = resolution;
	    	showChartDataForResolution(resolution);
	    }

	    $scope.retryClick = function($event){
	    	showChartDataForResolution($scope.currentChartResolution);
	    }

	    /*============ Chart ============*/

	    $scope.userHistoryChart = {
	    	 series: ['Walkby', 'Bounces', 'Visits']
	    	,labels: ["January", "February", "March", "April", "May", "June", "July"]
	    	,colors: [
	    		 appConfig.color.greenLight
	    		,appConfig.color.violet
	    		,appConfig.color.gray
	    	]
	    	,options: {
	    		 scales: {
		            xAxes: [{
		                 display: false
		                ,gridLines: {
		                	display: false
		                }
		            }],
		            yAxes: [{
		            	scaleLabel: {
		            		 display: true
		            		,labelString: 'Number of visits'
		            	}
		            	,ticks: {
		            		 min: 0
                        	,beginAtZero: true
		            	}
		            }]
		        }
	    	}
	    	,data: [
	    		 [65, 59, 80, 81, 56, 55, 40]
	    		,[28, 48, 40, 19, 86, 27, 90]
	    		,[16, 13, 22, 22, 11, 14, 27]
	    	]
	    	,datasetOverride: [
	    		 {
	    		 	 fill: false
                    ,borderCapStyle: 'butt'
		            ,borderDash: [5, 5]
		            ,borderDashOffset: 0
		            ,borderJoinStyle: 'miter'
	    		}
	    		,{
	    			 fill: false
                    ,borderCapStyle: 'butt'
 		            ,borderDash: [5, 5]
 		            ,borderDashOffset: 0
 		            ,borderJoinStyle: 'miter'
	    		}
	    		,{
	    			 fill: false
	    		}
	    	]
	    }

	    $scope.chartPeriodStr = '';

	    function showChartDataForResolution(resolution){
	    	if(resolution.data) return showChartData(resolution.data);
	    	var period = PandoPeriod.getPastPeriodWithName(resolution.val);
	    	$scope.loadingErr = false;
	    	$scope.loading = true;
	    	$scope.device.analytics.pandoIdRawEventsList($scope.id, period, function(err, eventsList){
	    		$scope.loading = false;
	    		if(err){
	    			$scope.loadingErr = false;
	    			return log.err(err, m);
	    		}
	    		var eventsSublists = eventsListToEventsCountForPeriod(eventsList, period);
	    		resolution.data = eventsSublists;
	    		log.trace(period.toString(), m);
	    		log.trace(eventsSublists, m);
	    		return showChartData(resolution.data);
	    	});	
	    }

	    function showChartData(data){
	    	var labels = _.keys(data.walkbys);
	    	$scope.userHistoryChart.labels = labels
	    	$scope.chartPeriodStr = _.first(labels) + ' - ' + _.last(labels);
	    	log.trace($scope.userHistoryChart.labels, m);
	    	$scope.userHistoryChart.data = [
	    		 _.values(data.walkbys)
	    		,_.values(data.bounces)
	    		,_.values(data.visits)
	    	];
	    	log.trace($scope.userHistoryChart.data, m);
	    }

	    function eventsListToEventsCountForPeriod(eventsList, period){
	    	var eventsSublists = sortEventsByTypes(eventsList);
	    	_.each(eventsSublists, function(eventsSingleSublist, groupName){
	    		eventsSublists[groupName] = sortEventsByDate(eventsSingleSublist, period);
	    		_.each(eventsSublists[groupName], function(val, key){
	    			eventsSublists[groupName][key] = val.length;
	    		});
	    	});
	    	return eventsSublists;
	    }

	    function sortEventsByTypes(eventsList){
	    	var groupedEvents = _.groupBy(eventsList, function(event){
	    		if(event.eventType == 'WALKBY') return PandoAnalytics.eventTypes.walkbys;
	    		var duration = event.totalDuration;
	    		if((duration > pandoUtils.minute) && (duration < pandoUtils.minute * 5)){
	    			return PandoAnalytics.eventTypes.bounces;
	    		}
	    		return PandoAnalytics.eventTypes.visits;
	    	});
	    	if(groupedEvents[PandoAnalytics.eventTypes.walkbys] == undefined){
	    		groupedEvents[PandoAnalytics.eventTypes.walkbys] = [];
	    	}
	    	if(groupedEvents[PandoAnalytics.eventTypes.bounces] == undefined){
	    		groupedEvents[PandoAnalytics.eventTypes.bounces] = [];
	    	}
	    	if(groupedEvents[PandoAnalytics.eventTypes.visits] == undefined){
	    		groupedEvents[PandoAnalytics.eventTypes.visits] = [];
	    	}
	    	return groupedEvents;
	    }

	    function sortEventsByDate(eventsList, period){
	    	var dateFormat = 'YYYY-MM-DD';
	    	var gropedList = _.groupBy(eventsList, function(event){
	    		return moment(event.eventDate).format(dateFormat);
	    	});
	    	var startTs = period.start.getTime() + pandoUtils.day;
	    	var endTs = period.end.getTime();
	    	var newGroupedList = {};
	    	for(var i = startTs; i <= endTs; i += pandoUtils.day){
	    		var dateStr = moment(i).format(dateFormat);
	    		if(gropedList[dateStr] === undefined) newGroupedList[dateStr] = [];
	    		else newGroupedList[dateStr] = gropedList[dateStr];
	    	}
	    	return newGroupedList;
	    }

	    function fixEventsDateRangeForPeriod(eventsList, period){
	    	return eventsList;
	    }

	    /*============ Init ============*/

	    function init(){
	    	showChartDataForResolution($scope.currentChartResolution);
	    }

	    init();

	});