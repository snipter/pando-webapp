angular.module('app')
    .controller('visitByHourOfDayChartCtrl', function($scope, appConfig){
        "ngInject";
        
        var m = 'visitByHourOfDayChartCtrl';

        $scope.chart = {
            series: [
                 'Previous period'
                ,'Current period'
            ]
            ,labels: [
                 "00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00"
                ,"10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00"
                ,"20:00" ,"21:00" ,"22:00" ,"23:00"
            ]
            ,colors: [
                 appConfig.color.gray
                ,appConfig.color.greenLight
            ]
            ,options: {
                 scales: {
                    xAxes: [{
                        gridLines: {
                            display: false
                        }
                        ,ticks: {
                            fontColor: '#323232'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                             display: true
                            ,labelString: 'Number of visitors'
                            ,fontColor: appConfig.color.gray
                        }
                        ,ticks: {
                             fontColor: '#323232'
                            ,min: 0
                            ,beginAtZero: true
                        }
                    }]
                }
            }
            ,data: [
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                ,[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            ]
            ,datasetOverride: [
                {
                    backgroundColor: appConfig.color.gray
                }
                ,{
                    backgroundColor: appConfig.color.greenLight
                }
            ]
        };

        /*============ Watch ============*/

        $scope.$watch('analyticsData', function(newVal){
            updateChart();
        });

        /*============ Chart ============*/

        function updateChart(){
            var hourOfDay = $scope.analyticsData.hourOfDay;
            $scope.chart.data = [
                 _.values(hourOfDay.prev.visits)
                ,_.values(hourOfDay.cur.visits)
            ];
            log.trace($scope.chart.data, m);
        }


    });