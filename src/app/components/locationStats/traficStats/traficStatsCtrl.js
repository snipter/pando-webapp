angular.module('app')
    .controller('traficStatsCtrl', function($scope, PandoAnalytics, PandoPeriod, pandoUtils) {
    	"ngInject";
    	
	    var m = 'traficStatsCtrl';

	    /*============ Analytics ============*/

	    var analyticsProvider = null;

	    /*============ Periods ============*/

	    $scope.startPeriods = [
	    	 {name: 'Yesterday', val: 'day'}
	    	,{name: 'Last week', val: 'week'}
	    	,{name: 'Last month', val: 'month'}
	    ]

	    $scope.endPeriods = [
	    	 {name: 'Today', val: 'day'}
	    	,{name: 'This week', val: 'week'}
	    	,{name: 'This month', val: 'month'}
	    ]

	    $scope.period = $scope.startPeriods[0].val;

	    /*============ Charts data ============*/

	    var defHourOfDay = {
	    	"0":0,"1":0,"2":0,"3":0,"4":0,"5":0,"6":0,"7":0,"8":0,"9":0,
	    	"10":0,"11":0,"12":0,"13":0,"14":0,"15":0,"16":0,"17":0,"18":0,"19":0,
	    	"20":0,"21":0,"22":0,"23":0
	    }

	    var defDayOfWeek = {
	    	"1":0,"2":0,"3":0,"4":0,"5":0,"6":0,"7":0
	    }

	    var defAnalyticsData = {
	    	eventsCount: {
	    		 cur: {new: 0, returning: 0, walkbys: 0, bounces: 0, visits: 0, terminalOpportunity: 0}
	    		,prev: {new: 0, returning: 0, walkbys: 0, bounces: 0, visits: 0, terminalOpportunity: 0}
	    	},
    		aveDurations: {
    			 cur: {visits: 0}
    			,prev: {visits: 0}
    		},
    		hourOfDay: {
    			 cur: {visits: defHourOfDay}
    			,prev: {visits: defHourOfDay}
    		},
    		dayOfWeek: {
    			 cur: {visits: defDayOfWeek}
    			,prev: {visits: defDayOfWeek}
    		},
    		periodEventsCount: {visits: defHourOfDay, walkbys: defHourOfDay, bounces: defHourOfDay}
	    };

	    $scope.analyticsData = defAnalyticsData;

	    $scope.loading = true;
	    $scope.loadingErr = false;

	    /*============ Watch ============*/

	    $scope.$watch('currentLoc', function(newVal){
	    	$scope.period = $scope.startPeriods[0].val;
	    	if(newVal){
	    		analyticsProvider = new PandoAnalytics(newVal);
	    		updateAnalyticsData();
	    	}else{
	    		resetAnalyticsData();
	    	}
	    });

	    /*============ UI Events ============*/

	    $scope.periodChanged = function(){
	    	log.debug('period changed', m);
	    	updateAnalyticsData();
	    }

	    $scope.retryClick = function($event){
	    	log.debug('retry click', m);
	    	updateAnalyticsData();
	    }

	    /*============ UI Functions ============*/

	    $scope.valuesChangingInProcents = function(prev, cur, useAbs){
	    	var val = PandoAnalytics.valuesChangingInProcents(prev, cur);
	    	if(!_.isFinite(val) || (val === null)) return 'N/A';
	    	return !useAbs ? Math.round(val) + '%' : Math.abs(Math.round(val)) + '%';
	    }

	    $scope.valuesChangingArrowClass = function(prev, cur){
	    	var val = PandoAnalytics.valuesChangingInProcents(prev, cur);
	    	if(!_.isFinite(val) || (val === null)) return 'g-hidden';
	    	if(val > 0) return 'icon-up-small';
	    	if(val < 0) return 'icon-down-small';
	    }

	    /*============ Updating data ============*/

	    function updateAnalyticsData(){
	    	if(!$scope.currentLoc) return;
	    	if(!analyticsProvider) return;

	    	var curPeriod = PandoPeriod.getPastPeriodWithName($scope.period);
	    	var prevPeriod = PandoPeriod.getPastPeriodWithName($scope.period, 1);

	    	var data = {};
	    	$scope.loading = true;
	    	$scope.loadingErr = false;
	    	log.debug('updating analytics', m);
	    	log.trace('current period: ' + curPeriod.toString(), m);
	    	log.trace('previous period: ' + prevPeriod.toString(), m);
	    	getAnalyticsDataPrevAndCurPeriods(prevPeriod, curPeriod, function(err, analyticsData){
	    		$scope.loading = false;
	    		if(err){
	    			log.err(err, m); 
	    			return $scope.loadingErr = true;
	    		}
	    		$scope.analyticsData = analyticsData;
	    		log.trace(analyticsData, m);
	    		log.debug('updating analytics done', m);
	    	});
	    }

	    function getAnalyticsDataPrevAndCurPeriods(prev, cur, cb){
	    	var fn = {
	    		eventsCount: function(cb){
	    			eventsCountForPrevAndCurrPeriods(prev, cur, cb);
	    		},
	    		aveDurations: function(cb){
	    			aveDurationsForPrevAndCurrPeriods(prev, cur, cb);
	    		},
	    		hourOfDay: function(cb){
	    			hourOfDayForPeriods(prev, cur, cb);
	    		},
	    		dayOfWeek: function(cb){
	    			dayOfWeekForPeriods(prev, cur, cb);
	    		},
	    		periodEventsCount: function(cb){
	    			periodEventsCount(cur, $scope.period, cb);
	    		}
	    	};
	    	async.parallel(fn, cb);
	    }

	    function resetAnalyticsData(){
	    	analyticsProvider = null;
	    	$scope.analyticsData = defAnalyticsData;
	    }

	    /* Events count */

	    function eventsCountForPrevAndCurrPeriods(prev, cur, cb){
	    	var fn = {
	    		prev: function(cb){
	    			eventsCountForPeriod(prev, cb);
	    		},
	    		cur: function(cb){
	    			eventsCountForPeriod(cur, cb);
	    		}
	    	}
	    	async.parallel(fn, cb);
	    }

	    function eventsCountForPeriod(period, cb){
	    	var fn = {
	    		new: function(cb){
	    			analyticsProvider.totalEventsCount(PandoAnalytics.events.new, period, cb);
	    		},
	    		returning: function(cb){
	    			analyticsProvider.totalEventsCount(PandoAnalytics.events.returning, period, cb);
	    		},
	    		walkbys: function(cb){
	    			analyticsProvider.totalEventsCount(PandoAnalytics.events.walkbys, period, cb);
	    		},
	    		bounces: function(cb){
	    			analyticsProvider.totalEventsCount(PandoAnalytics.events.bounces, period, cb);
	    		},
	    		visits: function(cb){
	    			analyticsProvider.totalEventsCount(PandoAnalytics.events.visits, period, cb);
	    		},
	    		terminalOpportunity: function(cb){
	    			analyticsProvider.totalEventsCount(PandoAnalytics.events.terminal.opportunity, period, cb);
	    		}
	    	}
	    	async.parallel(fn, cb);
	    }

	    /* Average duration */

	    function aveDurationsForPrevAndCurrPeriods(prev, cur, cb){
	    	var fn = {
	    		prev: function(cb){
	    			aveDurationsForPeriod(prev, cb);
	    		},
	    		cur: function(cb){
	    			aveDurationsForPeriod(cur, cb);
	    		}
	    	}
	    	async.parallel(fn, cb);
	    }

	    function aveDurationsForPeriod(period, cb){
	    	var fn = {
	    		visits: function(cb){
	    			analyticsProvider.avarageEventsDuration(PandoAnalytics.events.visits, period, cb);
	    		}
	    	}
	    	async.parallel(fn, cb);
	    }

	    /* Hour of day */

	    function hourOfDayForPeriods(prev, cur, cb){
	    	var fn = {
	    		prev: function(cb){
	    			hourOfDayForSinglePeriod(prev, cb);
	    		},
	    		cur: function(cb){
	    			hourOfDayForSinglePeriod(cur, cb);
	    		}
	    	}
	    	async.parallel(fn, cb);
	    }

	    function hourOfDayForSinglePeriod(period, cb){
	    	var fn = {
	    		visits: function(cb){
	    			analyticsProvider.hourOfDayEventsCount(PandoAnalytics.events.visits, period, cb);
	    		}
	    	}
	    	async.parallel(fn, cb);
	    }

	    /* Day of week */

	    function dayOfWeekForPeriods(prev, cur, cb){
	    	if((cur.end.getTime() - cur.start.getTime()) < pandoUtils.week){
	    		cur = PandoPeriod.getPastPeriodWithName('week');
	    	}
	    	if((prev.end.getTime() - prev.start.getTime()) < pandoUtils.week){
	    		prev = PandoPeriod.getPastPeriodWithName('week', 1);
	    	}
	    	var fn = {
	    		prev: function(cb){
	    			dayOfWeekForSinglePeriod(prev, cb);
	    		},
	    		cur: function(cb){
	    			dayOfWeekForSinglePeriod(cur, cb);
	    		}
	    	}
	    	async.parallel(fn, cb);
	    }

	    function dayOfWeekForSinglePeriod(period, cb){
	    	var fn = {
	    		visits: function(cb){
	    			analyticsProvider.dayOfWeekEventsCount(PandoAnalytics.events.visits, period, cb);
	    		}
	    	}
	    	async.parallel(fn, cb);
	    }

	    /* Period events count */

	    function periodEventsCount(period, resolutionName , cb){
	    	var resolution = PandoAnalytics.resolutions.hour;
	    	if(resolutionName == 'week'){
	    		resolution = PandoAnalytics.resolutions.dayOfWeek;
	    	}
	    	if(resolutionName == 'month'){
	    		resolution = PandoAnalytics.resolutions.calendarDay;
	    	}
	    	var fn = {
	    		walkbys: function(cb){
	    			analyticsProvider.eventsCount(PandoAnalytics.events.walkbys, period, resolution, cb);
	    		},
	    		bounces: function(cb){
	    			analyticsProvider.eventsCount(PandoAnalytics.events.bounces, period, resolution, cb);
	    		},
	    		visits: function(cb){
	    			analyticsProvider.eventsCount(PandoAnalytics.events.visits, period, resolution, cb);
	    		}
	    	}
	    	async.parallel(fn, cb);
	    }

	});