angular.module('app')
	.directive('circlesChart', function(){
		"ngInject";
		return {
			restrict: 'E',
			scope: {
				items: '=', 
				typeField: '@', 
				sizeField: '@', 
				angleField: '@', 
				distanceField: '@', 
				idField: '@',
				itemClicked: '&'},
			replace: true,
			link: function($scope, element, attrs, ctrl){

				var m = 'circlesChart';

				/*============ Init ============*/

				log.debug('init', m);

				/*============ Configurations ============*/

				var canv = {
					width: 400,
					height: 400,
					item: {
						circle: {
							r: {
								min: 3,
								max: 30
							},
							stroke: {
								width: .7
							}
						}
					},
					logo: {
						width: 32.46,
						height: 42.45
					}
				}
				canv.center = {
					x: canv.width / 2,
					y: canv.height / 2
				}

				/*============ Canvas ============*/

				var canvas = d3.select(element[0])
					.append("div")
						.classed("circles-chart__svg-container", true) //container class to make it responsive
							.append("svg")
								//responsive SVG needs these 2 attributes and no width and height attr
								.attr("preserveAspectRatio", "xMinYMin meet")
								.attr("viewBox", "0 0 " + canv.width + " " + canv.height)
								//class to make it responsive
								.classed("circles-chart__svg-content-responsive", true); 

				/*============ Adding circles ============*/

				for(var i = 1; i <= 4; i++){
					var maxR = canv.width / 4;
					var r = maxR - 15 * (i - 1);
					var w = 0.25 * i;
					canvas.append('circle')
						.attr('cx', canv.center.x)
						.attr('cy', canv.center.y)
						.attr('stroke', '#0D42EA')
						.attr('stroke-width', w)
						.attr('fill-opacity', 0)
						.attr('r', r)
				}

				/*============ Adding logo ============*/

				var logoX = canv.center.x - canv.logo.width / 2;
				var logoY = canv.center.y - canv.logo.height / 2;
				canvas.append('g')
					.attr('transform', 'translate(' + logoX + ', ' + logoY + ')')
					// .attr('fill', '#01416d')
					.attr('fill', '#0D42EA')
					.html('<path d="M11.1374,25.4033 L6.8474,23.8533 L6.8474,15.5203 C10.6264,16.8763 13.8244,18.2333 13.9214,22.1633 C14.0184,26.0933 11.1374,25.4033 11.1374,25.4033" id="Fill-6"></path> <path d="M7.2353,30.8296 L7.2353,30.3776 L13.2193,32.6386 C23.4693,36.5756 23.2043,26.3666 23.2043,26.3666 C24.0763,14.9936 13.9363,10.8976 13.9363,10.8976 L0.0473,5.9476 L0.2833,5.7936 L14.0183,10.6756 C14.0183,10.6756 24.2893,14.8536 23.4173,26.3296 C23.4173,26.3296 23.5723,37.0526 13.3233,33.0796 L7.9093,31.0966 L7.2353,30.8296" id="Fill-8"></path> <path d="M7.2353,30.8296 L7.2353,43.1406 L15.5473,37.2246 L15.5473,33.6606 C15.5473,33.6606 14.5183,33.5876 7.2353,30.8296" id="Fill-10"></path> <path d="M20.6228,32.7402 C23.3558,30.6632 23.2818,26.1362 23.2818,26.1362 C24.1538,14.6592 13.8828,10.6752 13.8828,10.6752 L0.2208,5.8302 L8.9408,0.6952 L10.1808,1.1262 C13.4778,2.1982 22.6578,5.2882 22.6578,5.2882 C22.6578,5.2882 31.3138,8.0652 31.8298,17.7552 C31.8298,17.7552 32.5088,22.4702 29.8278,25.9272 L20.6348,32.9982 L20.6228,32.7402 Z" id="Fill-12"></path>');

				/*============ Data canvas ============*/

				var dataCanvas = canvas.append('g');

				/*============ Displayng data ============*/

				function redrawItemsChart(items){

					// Calculaiting polar radius (from center to point)
					var circlePolarRmin = canv.logo.height / 2 + canv.item.circle.r.max;
					var circlePolarRmax = canv.height / 2 - canv.item.circle.r.max;
					var circlePolarR = d3.scale.linear()
					    .domain([100, 0])
					    .range([circlePolarRmin, circlePolarRmax]);

					// Calculating angle
					var circlePolarAngle = d3.scale.linear()
					    .domain([0, 1])
					    .range([0, Math.PI * 2]);

					// Scale circles radius depends on amount of circles
					var circleRadiusFactorScale = d3.scale.linear()
						.domain([20, 500])
						.range([1, 0.1]);
					var circleRadiusFactor = circleRadiusFactorScale(items.length);
					if(circleRadiusFactor < 0.1) circleRadiusFactor = 0.1;
					if(circleRadiusFactor > 1.0) circleRadiusFactor = 1.0;

					var circleRadius = d3.scale.linear()
					    .domain([0, d3.max(items, function(item){return getItemSize(item)}) * 1.1])
					    .range([canv.item.circle.r.min, canv.item.circle.r.max]);

					var itemCircleData = function(item){
						var data = {stroke: {width: 0}};
						var polarR = circlePolarR(getItemDistance(item));
						var angle = circlePolarAngle(getItemAngle(item)) + Math.PI / 2;
						data.x = polarR * Math.cos(angle);
						data.y = polarR * Math.sin(angle);
						data.x = data.x + canv.center.x;
						data.y = canv.height - canv.center.x - data.y;
						data.r = circleRadius(getItemSize(item)) * circleRadiusFactor;
						// calculating stroke
						data.stroke.width = data.r * canv.item.circle.stroke.width;
						// decrease radius to half of stroke width
						data.r -= data.stroke.width / 2;
						if(getItemType(item).toLowerCase() == 'visit') data.class = 'circles-chart__circle_visitor';
						if(getItemType(item).toLowerCase() == 'bounce') data.class = 'circles-chart__circle_bonuced';
						if(getItemType(item).toLowerCase() == 'walkby') data.class = 'circles-chart__circle_walkby';
						return data;
					}

					_.each(items, function(item){
						item.circle = itemCircleData(item);
					})

					var itemCircles = dataCanvas
						.selectAll("circle")
						.data(items, function(item){
							return getItemId(item);
						});

					configitemCircles(itemCircles);

					var newCircle = itemCircles
						.enter().append('circle');
					configitemCircles(newCircle);


					itemCircles.exit().remove();

					function configitemCircles(circles){
						circles
							.attr('class', function(item){
								return item.circle.class;
							})
							.on('click', function(item, event){
								if($scope.itemClicked) $scope.itemClicked({$event: event, item: item})
							})
							.transition()
								.attr('cx', function(item){
									return item.circle.x;
								})
								.attr('cy', function(item){
									return item.circle.y;
								})
								.attr('r', function(item){
									return item.circle.r;
								})
								.attr('stroke-width', function(item){
									return item.circle.stroke.width;
								});
					}
				}

				/*============ Item fields ============*/

				function getItemId(item){
					if($scope.idField) return item[$scope.idField];
					else return item.id;
				}

				function getItemType(item){
					if($scope.typeField) return item[$scope.typeField];
					else return item.type;
				}

				function getItemSize(item){
					if($scope.sizeField) return item[$scope.sizeField];
					else return item.size;
				}

				function getItemAngle(item){
					if($scope.angleField) return item[$scope.angleField];
					else return item.angle;
				}

				function getItemDistance(item){
					if($scope.distanceField) return item[$scope.distanceField];
					else return item.distance;
				}

				/*============ Watch model ============*/

				$scope.$watch('items', function(newValue, oldValue){
	                if(newValue && newValue.length){
	                	redrawItemsChart(newValue);
	                }else{
	                	redrawItemsChart([]);
	                }
	            }, true);

		        /*============ Destroy ============*/

		        $scope.$on('$destroy', function(){
		            
		            log.debug('destroy', m);
		        });
		        
			}
		}
	});