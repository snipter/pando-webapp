angular.module('app')
    .filter('appToName', function(){
	    return function(appName) {
	    	if(appName == 'webview') return 'Web View';
	    	if(appName == 'graphics') return 'Graphics';
	    	if(appName == 'twitter') return 'Twitter';
	    	if(appName == 'instagram') return 'Instagram';
	        return appName;
	    }
	});