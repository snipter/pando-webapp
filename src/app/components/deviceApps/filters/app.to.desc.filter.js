angular.module('app')
    .filter('appToDesc', function(){
	    return function(appName) {
	    	if(appName == 'graphics') return 'Upload and display any graphic';
	    	if(appName == 'webview') return 'Display any website';
	    	if(appName == 'twitter') return 'Display any social feed or hashtag';
	    	return 'Increase your followers by using this app';
	    }
	});