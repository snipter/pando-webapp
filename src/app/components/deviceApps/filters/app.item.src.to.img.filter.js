angular.module('app')
    .filter('appItemSrcToImg', function(urlToImg){
		"ngInject";

	    return function(itemSrc, appName) {
	    	if(!itemSrc) return '';
	    	if(!appName) return itemSrc;
	    	if(appName == 'graphics') return itemSrc;
    		if(appName == 'webview') return urlToImg(itemSrc);
    		return itemSrc;
	    }
	    
	});