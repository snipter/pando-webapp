(function(){
	'use strict';

	angular.module('app')
	    .filter('appToImg', [appToImgFilter])
	    .filter('appsToImgs', [appsToImgsFilter])



	function appToImgFilter(){
	    return function(app) {
	    	return appToImg(app);
	    }
	}

	function appsToImgsFilter(){
	    return function(apps) {
	    	return appsArrToImg(apps);
	    }
	}

	function appsArrToImg(arr){
		return _.map(arr, appToImg);
	}

	function appToImg(app){
		return appNameToImg(app.name);
	}

	function appNameToImg(appName){
		if(appName == 'webview') return '/assets/img/app/icon-app-webview@3x.png';
		if(appName == 'graphics') return '/assets/img/app/icon-app-graphics@3x.png';
		if(appName == 'twitter') return '/assets/img/app/icon-app-twitter@3x.png';
	    return '/assets/img/app/icon-app-graphics@3x.png';
	}
})();