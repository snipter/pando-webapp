angular.module('app')
	.directive('appControl', function(toastMsg, localNotifyCenter){
		"ngInject";
		
		return {
			restrict: 'E',
			scope: {app:'=', device:'=', showActivationTooltip:'=', activeChanged: '&'},
			templateUrl: 'app/components/deviceApps/appControl/appControlView.html',
			replace: true,
			link: function($scope, element, attrs, ctrl){

				var m = 'appControl';

				$scope.durationChanged = function(){
		            saveApp();
		        }

		        $scope.activeChangedInnerEvent = function(){
		            log.debug('applicaiton aciteve changed: ' + $scope.app.active + ", appId: " + $scope.app.applicationId, m);
		            if($scope.activeChanged) $scope.activeChanged($scope.app);
		            saveApp();
		        }

		        function saveApp(){
		            log.debug('saving app: ' + $scope.app.applicationId, m);
		            $scope.app.save(function(err){
		            	if(err){ toastMsg.err('Saving application data error.'); return log.err(err, m, cb); }
		            	log.debug('saving app done', m);
		            	toastMsg.success('App data updated!');
		            });
		        }

		        /*============ Notifications ============*/

		        var unbindAppActionAdd = localNotifyCenter.onAppActionAdd(function(appName){
		            if($scope.app.name != appName) return;
		            $scope.editorDisplayed = true;
		        });

		        var unbindAppItemClicked = localNotifyCenter.onAppItemClicked(function(appName, itemData){
		        	if($scope.app.name !== appName) return;
		        	$scope.editorDisplayed = true;
		        });

		        /*============ Destroy ============*/

		        $scope.$on('$destroy', function(){
		            unbindAppActionAdd();
		            unbindAppItemClicked();
		            log.debug('destroy', m);
		        });
		        
			}
		}
	});