angular.module('app')
    .controller('appTwitterCtrl', function($scope, $timeout, $mdConstant, toastMsg, localNotifyCenter, pandoApi, uiGmapIsReady){
        "ngInject";

        var m = 'appTwitterCtrl';
        var self = this;

        $scope.queries = [];
        $scope.separatorKeys = [
            $mdConstant.KEY_CODE.ENTER, 
            $mdConstant.KEY_CODE.COMMA, 
            $mdConstant.KEY_CODE.SPACE,
            $mdConstant.KEY_CODE.TAB];
        $scope.title = '';
        $scope.duration = 5;
        $scope.postsCount = 50;
        $scope.headerColor = '#1E1E2C';
        $scope.bodyColor = '#38384E';
        $scope.textColor = '#F3F6F7';

        // {"latitude":49.098495,"longitude":33.43834100000004,"number":"82","street":"Vatutina Street","city":"Kremenchuk","state":"Poltavs'ka oblast","country":"Ukraine","zip":"36000","formatted":"Vatutina St, 82, Kremenchuk, Poltavs'ka oblast, Ukraine, 36000","placeId":"ChIJWUVIV1JS10AREbdsale8CR4"}
        $scope.locationEnabled = false;
        $scope.location = null;
        $scope.map = { 
            center: { latitude: 40.7143528, longitude: -74.0059731 }, 
            zoom: 11
        };
        $scope.circle = {
            center: { latitude: 40.7143528, longitude: -74.0059731 }, 
            radius: 10000,
            stroke: {
                color: '#08B21F',
                weight: 2,
                opacity: 1
            },
            fill: {
                color: '#08B21F',
                opacity: 0.5
            }
        }

        /*============ Events ============*/

        $scope.transformChip = function(q){
            if(!q) return null;
            q = q.trim();
            if(/^#.+/g.test(q)) return q;
            if(/^@.+/g.test(q)) return q;
            return '#' + q;
        }

        $scope.placeChanged = function(){
            if(!$scope.location || !$scope.location.placeId) return;
            log.debug('location changed: ' + JSON.stringify($scope.location), m);
            $scope.map.center.latitude = $scope.location.latitude;
            $scope.map.center.longitude = $scope.location.longitude;
            $scope.circle.center.latitude = $scope.location.latitude;
            $scope.circle.center.longitude = $scope.location.longitude;
        }

        $scope.useLocationChanged = function(){
            log.debug('use location changed', m);
            if($scope.locationEnabled){
                updateMapView();
            }
        }

        $scope.saveClicked = function(){
            log.debug('save clicked', m);
            saveData();
        }

        $scope.cancelClicked = function(){
            log.debug('reset clicked', m);
            setQuery($scope.app.params);
        }

        $scope.$watch('editorDisplayed', function(newVal, oldVal){
            if(newVal) updateMapView();
        });

        /*============ Map ============*/

        function updateMapView(){
            if(!$scope.locationEnabled) return;
            uiGmapIsReady.promise().then(function (maps) {
                google.maps.event.trigger(maps[0].map, 'resize');
                $timeout(function(){
                    $scope.map.center.latitude = $scope.circle.center.latitude;
                    $scope.map.center.longitude = $scope.circle.center.longitude;
                }, 500);
            });
        }

        /*============ Query ============*/

        function getQuery(){
            var data = {};
            data.query = _.reduce($scope.queries, function(memo, q){
                return memo + ' ' + q.trim();
            }, '').trim();
            data.title = $scope.title;
            data.duration = parseInt($scope.duration) * 1000;
            data.postsCount = parseInt($scope.postsCount);
            data.headerColor = $scope.headerColor ? $scope.headerColor : '#1E1E2C';
            data.bodyColor = $scope.bodyColor ? $scope.bodyColor : '#38384E';
            data.textColor = $scope.textColor ? $scope.textColor : '#F3F6F7';
            if($scope.locationEnabled){
                var loc = {};
                loc.lat = $scope.circle.center.latitude;
                loc.lon = $scope.circle.center.longitude;
                loc.radius = Math.floor($scope.circle.radius * 0.001);
                loc.zoom = $scope.map.zoom;
                if(loc.radius <= 0) loc.radius = 1;
                data.location = loc;
            }
            return data;
        }

        function setQuery(data){
            $scope.queries = data.query ? data.query.split(' ') : [];
            $scope.title = data.title ? data.title : '';
            $scope.duration = data.duration ? data.duration / 1000 : 5;
            $scope.postsCount = data.postsCount ? data.postsCount : 50;
            $scope.headerColor = data.headerColor ? data.headerColor : '#1E1E2C';
            $scope.bodyColor = data.bodyColor ? data.bodyColor : '#38384E';
            $scope.textColor = data.textColor ? data.textColor : '#F3F6F7';
            if(data.location){
                $scope.locationEnabled = true;
                var loc = data.location;
                if(loc.lat){
                    $scope.circle.center.latitude = loc.lat;
                    $scope.map.center.latitude = loc.lat;
                }
                if(loc.lon){
                    $scope.circle.center.longitude = loc.lon;
                    $scope.map.center.longitude = loc.lon;
                }
                if(loc.radius){
                    $scope.circle.radius = Math.floor(loc.radius / 0.001);
                }
                $scope.map.zoom = loc.zoom ? parseInt(loc.zoom) : 11;
            }
            updateMapView();
        }

        /*============ Backend ============*/

        function saveData(){
            log.debug('saving app data', m);
            $scope.app.params = getQuery();
            $scope.app.paramsSave(function(err){
                if(err){
                    toastMsg.dataSavingErr();
                    return log.err(err, m);
                }
                log.debug('saving app data done', m);
                $scope.changed = false;
                setQuery($scope.app.params);
                toastMsg.success('Data saved!');
                notifyAppUpdated();
            });
        }

        function loadData(){
            log.debug('loading app data', m);
            $scope.app.populateParams(function(err){
                if(err){
                    toastMsg.dataLoadingErr();
                    return log.err(err, m);
                }
                if($scope.app.params) setQuery($scope.app.params);
                else setQuery({});
                $scope.changed = false;
            });
        }

        /*============ Notify center ============*/

        function notifyAppUpdated(data){
            localNotifyCenter.broadcastAppUpdated('twitter', data);
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            loadData();
        }

        init();
    });