angular.module('app')
    .controller('appEditGraphicsCtrl', function($scope, consts, toastMsg, localNotifyCenter, pandoApi, UserParamsStorage, pandoAuth){
        "ngInject";

        var m = 'appEditGraphicsCtrl';
        var self = this;

        $scope.resolution = null;

        /*============ Watch ============*/

        $scope.$watch('app', function(newApp){
            $scope.resolution = null;
            if(newApp){
                var device = newApp.device;
                device.update(function(err){
                    if(err) return log.err(err, m);
                    if(device.id == $scope.app.device.id){
                        $scope.resolution = device.resolution();
                    }
                });
            };
        });

        /*============ Events ============*/

        $scope.uploadItemsEvent = function($files){
            log.debug('upload files event', m);
            uploadFiles($files);
        }

        function uploadFiles($files){
            log.debug('uploading files', m);
            async.eachSeries($files, function($file, cb){
                uploadSingleFile($file, cb);
            }, function(err){
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Files upload error.');
                }
                log.debug('uploading files done', m);
            })
        }

        function uploadSingleFile($file, cb){
            var app = $scope.app;

            var itemData = pandoApi.apps.baseGraphicItemData();
            itemData.uploading = true;
            app.itemsAdd(itemData);

            getLocalFileSrc($file, function(err, src){
                if(err) return cb(err);
                itemData.src = src;
            })

            pandoApi.resources.upload($file, 'graphics', function(){
                var procent = Math.floor((event.loaded / event.total) * 100);
                log.debug('upload progress: ' + procent + '%', m);
            }, function(err, uploadData){
                itemData.uploading = false;
                if(err) return cb(err);
                log.trace(uploadData, m);
                if(!uploadData.uploadUrl) return cb();
                itemData.src = uploadData.uploadUrl;
                delete itemData.uploading;
                if($scope.app && ($scope.app.id == app.id)) $scope.app.itemsSave(cb);
                else return cb();
            });
        }

        function getLocalFileSrc(file, cb){
            var reader = new FileReader();
            reader.onload = function (e) {
                $scope.$apply(function(){
                    var url = e.target.result;
                    return cb(null, url);
                });
            }
            reader.readAsDataURL(file);
        }

        /*============ Notifications ============*/

        var unbindAppActionAdd = localNotifyCenter.onAppActionAdd(function(appName){
            if(!$scope.app) return;
            if($scope.app.name != appName) return;
            $('#app-graphics-add-btn').click();
        });

        var unbindAppUpdated = localNotifyCenter.onAppUpdated(function(appName){
            if(!$scope.app) return;
            if($scope.app.name != appName) return;
            if(!requiredTouploadExampleGraphics()) return log.debug("don't required to add example graphics", m);
            log.debug('required to add example graphics', m);
            uploadExampleGraphics(function(err){
                if(err) return log.err(err, m);
                setUserFirstGraphicsUploaded(true);
            });
        });

        /*============ Example images ============*/

        function uploadExampleGraphics(cb){
            log.debug('adding example graphics', m);
            $scope.app.items = getExampleGraphicsData();
            log.debug('saving example items', m);
            $scope.app.itemsSave(function(err){
                if(err) return cb(err);
                log.debug('saving example items done', m);
                return cb();
            });
        }

        function requiredTouploadExampleGraphics(){
            if(!$scope.app) return false;
            if(!$scope.app.active) return false;
            if(getUserFirstGraphicsUploaded()) return false;
            if(!$scope.app.items) return true;
            if(!$scope.app.items.length) return true;
            return false;
        }

        function getExampleGraphicsData(){
            var imgsPath = location.origin + '/assets/img/apps/graphics/examples/';
            var imgsNames = ['image_00.jpg'];
            return _.map(imgsNames, function(imgName){ 
                var itemData = pandoApi.apps.baseGraphicItemData();
                itemData.src = imgsPath + imgName;
                return itemData;
            });
        }

        function getUserFirstGraphicsUploaded(){
            var paramsStorage = getUserParamsStorage();
            return paramsStorage ? paramsStorage.get(consts.local.user.firstGraphicsUploaded) : null;;
        }

        function setUserFirstGraphicsUploaded(val){
            var paramsStorage = getUserParamsStorage();
            if(paramsStorage){
                paramsStorage.set(consts.local.user.firstGraphicsUploaded, val);
            }
        }

        function getUserParamsStorage(){
            var user = pandoAuth.currentUser;
            if(!user) return null;
            return new UserParamsStorage(user);
        }
        
        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Destroy ============*/

        $scope.$on('$destroy', function(){
            unbindAppActionAdd();
            unbindAppUpdated();
            log.debug('destroy', m);
        });

    });