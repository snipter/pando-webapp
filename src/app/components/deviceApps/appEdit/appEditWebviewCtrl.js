angular.module('app')
    .controller('appWebviewCtrl', function($scope, localNotifyCenter, pandoApi, dialogs){
        "ngInject";

        var m = 'appWebviewCtrl';

        /*============ Events ============*/

        $scope.addItemClicked = function($event){
            log.debug('add item clicked', m);
            var opt = {
                templateUrl: 'app/components/deviceApps/appEdit/appEditWebviewAddView.html',
                controller: 'addWebviewModalCtrl'
            }
            dialogs.modal(opt, function(url){
                var itemData = pandoApi.apps.baseGraphicItemData();
                itemData.src = url;
                localNotifyCenter.broadcastAppItemAdd('webview', itemData);
            });
        }

        /*============ Notifications ============*/

        var unbindAppActionAdd = localNotifyCenter.onAppActionAdd(function(appName){
            if($scope.app.name != appName) return;
            $scope.addItemClicked();
        });


        /*============ Init ============*/

        function init(){
            log.debug('init');
        }

        init();

        /*============ Destroty ============*/

        $scope.$on('$destroy', function(){
            unbindAppActionAdd();
            log.debug('destroy', m);
        });

    });