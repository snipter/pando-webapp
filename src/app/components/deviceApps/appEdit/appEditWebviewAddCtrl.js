angular.module('app')
    .controller('addWebviewModalCtrl', function($scope, $uibModalInstance){
        "ngInject";
        
        var m = 'addWebviewModalCtrl';

        $scope.url = 'http://';

        /*============ Events ============*/

        $scope.ok = function(){
            log.debug('ok clicked', m);
            $uibModalInstance.close($scope.url);
        }

        $scope.cancel = function(){
            log.debug('cancel clicked', m);
            $uibModalInstance.dismiss("cancel");
        }

        $scope.urlKeyPress = function($event){
            if($event.which === 13) {
                $event.preventDefault();
                $uibModalInstance.close($scope.url);
            }
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Destroy ============*/

        $scope.$on('$destroy', function(){
            log.debug('destroy', m);
        });
        
    });