angular.module('app')
	.directive('appWidgetGraphics', function(localNotifyCenter){
		"ngInject";
		
		return {
			restrict: 'E',
			scope: {app: '=', itemName: '@'},
			template: "\
			<div class='app-widget-graphics'> \
                <ul class='app-widget-graphics-list'> \
                    <li data-ng-repeat='item in items'> \
                    	<a href='#' data-ng-click='itemClicked(item)' style='background-image: url({{ item.src | appItemSrcToImg: app.name }})'></a> \
                    </li><li><a href='#' data-ng-click='addItemClicked()' class='app-widget-graphics-add icon-plus'></a></li> \
                </ul>  \
                <div class='app-widget-graphics-descr'> \
                    {{itemsCountText()}} \
                </div> \
            </div> \
			",
			replace: true,
			link: function($scope, element, attrs, ctrl){

				var m = 'appWidgetGraphics';
				var maxItemsCount = 3;
				
				$scope.items = [];

				$scope.$watch('app.items', function(newVal){
					$scope.items = [];
					if(newVal){
						$scope.items = getItems();
					}
				});

				function getItems(){
					if(!$scope.app) return [];
					if(!$scope.app.items) return [];
					if(!$scope.app.items.length) return [];
					if($scope.app.items.length < maxItemsCount) return $scope.app.items;
					return $scope.app.items.slice(0, maxItemsCount);
				}

				$scope.itemClicked = function(item){
					localNotifyCenter.broadcastAppItemClicked($scope.app.name, item);
				}

				$scope.addItemClicked = function(){
					localNotifyCenter.broadcastAppActionAdd($scope.app.name);
				}

				$scope.itemsCountText = function(){
					var itemName = $scope.itemName ? $scope.itemName : 'item';
					var itemsName = itemName + 's';
					if(!$scope.app.items) return 'No ' + itemsName + ' found';
					if(!$scope.app.items.length) return 'No ' + itemsName + ' found';
					if($scope.app.items.length == 1) return '1 ' + itemName;
					else return $scope.app.items.length + ' ' + itemsName;
				}

			}
		}
	});