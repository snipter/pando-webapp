angular.module('app')
	.directive('appWidgetTwitter', function(localNotifyCenter){
		"ngInject";
		
		return {
			restrict: 'E',
			scope: {app: '='},
			template: "\
				<div class='app-widget-twitter'> \
	                <div class='app-widget-twitter-container' style='background-color: {{app.params.bodyColor}}; color: {{app.params.textColor}}'> \
	                    <span class='app-widget-twitter-header' style='background-color: {{app.params.headerColor}}'></span> \
	                    #Hastag \
	                </div> \
	            </div> \
			",
			replace: true,
			link: function($scope, element, attrs, ctrl){

			}
		}
		
	});