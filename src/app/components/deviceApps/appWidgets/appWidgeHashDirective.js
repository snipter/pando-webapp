angular.module('app')
	.directive('appWidgetHash', function(localNotifyCenter){
		"ngInject";

		return {
			restrict: 'E',
			scope: {app: '='},
			template: "\
				<ul class='app-widget-hash'> \
				    <li data-ng-repeat='item in items'>{{item}}</li> \
				</ul> \
			",
			replace: true,
			link: function($scope, element, attrs, ctrl){

				$scope.items = [];

				$scope.$watch('app.params', function(newVal){
					$scope.items = [];
					if(newVal){
						if($scope.app.params.query){
							var queryStr = $scope.app.params.query;
							var queryArr = queryStr.split(' ');
							$scope.items = queryArr;
						}
					}
				});
				
			}
		}
		
	});