angular.module('app')
    .controller('enableMirroringDialogCtrl', function($scope, $uibModalInstance){
        "ngInject";
        
        var m = 'enableMirroringDialogCtrl';

        /*============ Events ============*/

        $scope.ok = function(){
            log.debug('ok clicked', m);
        $uibModalInstance.close();
        }

        $scope.cancel = function(){
            log.debug('cancel clicked', m);
            $uibModalInstance.dismiss("cancel");
        }

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Destroy ============*/

        $scope.$on('$destroy', function(){
            log.debug('destroy', m);
        });
        
    });