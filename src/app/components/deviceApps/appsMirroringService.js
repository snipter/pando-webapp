angular.module('app')
    .service('appsMirroringService', function(){
    	var m = 'appsMirroringService';
    	var self = this;
        var debug = true;

        /*============ Mirror Devices Apps ============*/

        // opt.name - app name to sync (all by default)
        // opt.sync - type of data to sync ['data', 'params', 'schedule'] (all by default)
        self.mirrorDevicesApps = function(srcDevice, destDevices, opt, cb){
            if(typeof opt === 'function'){cb = opt; opt = {};}
            var allDevices = _.flatten([srcDevice, destDevices]);
            checkDevicesPopulatedApps(allDevices, function(err){
                if(err) return cb(err);
                // src apps
                var srcApps = [];
                if(opt.name){
                    var app = srcDevice.appWithName(opt.name);
                    if(!app) throw new Error('wrong app name: ' + opt.name);
                    srcApps.push(app);
                }else{
                    srcApps = srcDevice.apps;
                }
                if(!srcApps.length) throw new Error('src apps array is empty');
                
                // syncing apps
                async.each(srcApps, function(srcApp, cb){
                    var destApps = _.map(destDevices, function(device){
                        return device.appWithName(srcApp.name);
                    });
                    destApps = _.compact(destApps);
                    var syncDataFn = function(cb){
                        self.mirrorData(srcApp, destApps, cb);
                    }
                    var syncParamsFn = function(cb){
                        self.mirrorParams(srcApp, destApps, cb);
                    }
                    var syncScheduleFn = function(cb){
                        self.mirrorScedule(srcApp, destApps, cb);
                    }
                    var syncFunctions = [];
                    if(!opt.sync){
                        syncFunctions.push(syncDataFn);
                        syncFunctions.push(syncParamsFn);
                        syncFunctions.push(syncScheduleFn);
                    }else{
                        if(!_.isArray(opt.sync)) throw new Error('sync is not array');
                        if(opt.sync.indexOf('data') >= 0) syncFunctions.push(syncDataFn);
                        if(opt.sync.indexOf('params') >= 0) syncFunctions.push(syncParamsFn);
                        if(opt.sync.indexOf('schedule') >= 0) syncFunctions.push(syncScheduleFn);
                    }

                    if(!syncFunctions.length) throw new Error('sync functions not set');
                    async.parallel(syncFunctions, cb);
                }, cb);
            });
        }

        function checkDevicesPopulatedApps(devices, cb){
            var devicesWithouAppsPopulated = _.filter(devices, function(item){
                return !item.appsPopulated;
            });
            if(!devicesWithouAppsPopulated.length) return cb();
            if(debug) log.debug('loading apps for not populated devices', m);
            async.each(devicesWithouAppsPopulated, function(device){
                device.populateAppsAndInstallUninstalled(cb);
            }, function(err){
                if(err) return cb(err);
                if(debug) log.debug('loading apps for not populated devices done', m);
                return cb();
            });
        }

        /*============ Mirror Apps ============*/

    	self.mirrorAllData = function(sourceApp, destApps, cb){
    		async.parallel([
                function(cb){
        			self.mirrorData(sourceApp, destApps, cb);
        		}
                ,function(cb){
        			self.mirrorParams(sourceApp, destApps, cb);
        		}
                ,function(cb){
        			self.mirrorScedule(sourceApp, destApps, cb);
        		}
            ], cb);
    	}

    	self.mirrorData = function(sourceApp, destApps, cb){
    		async.each(destApps, function(item, cb){
    			item.active = sourceApp.active;
    			item.weight = sourceApp.weight;
    			item.duration = sourceApp.duration;
    			item.rentEnabled = sourceApp.rentEnabled;
    			item.approveContent = sourceApp.approveContent;
    			item.rentWeight = sourceApp.rentWeight;
    			item.maxAdDuration = sourceApp.maxAdDuration;
    			item.save(cb);
    		}, cb);
    	}

    	self.mirrorParams = function(sourceApp, destApps, cb){
            if(debug) log.debug('mirror app params', m);
            if(debug) log.trace(sourceApp.data(), m);
            if(debug) log.trace(sourceApp.params, m);
    		async.each(destApps, function(item, cb){
    			item.params = sourceApp.params;
    			item.paramsSave(cb);
    		}, cb);
    	}

    	self.mirrorScedule = function(sourceApp, destApps, cb){
            sourceApp.populateSchedule(function(err){
                if(err) return cb(err);
                async.each(destApps, function(item, cb){
                    item.populateSchedule(function(err){
                        if(err) return cb(err);
                        item.schedule = sourceApp.schedule;
                        item.saveSchedule(cb);
                    });
                }, cb);
            });
    	}
        
    });