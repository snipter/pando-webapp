angular.module('app')
    .filter('devicesWithLocation', function(){
        return function(devices, location){
            if(!devices) return null;
            if(!location) return null;
            var locId = null;
            if(typeof location === "string") locId = parseInt(location);
            if(typeof location === "number") locId = location;
            if(location.id) locId = parseInt(location.id);
            if(!locId) return null;
            var filteredDevices =  _.filter(devices, function(item){
                return item.locationId == locId;
            });
            return filteredDevices;
        }
    });