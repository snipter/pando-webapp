angular.module('app')
    .controller('deviceAppsCtrl', function($scope, $state, pagesService, pandoOrm, toastMsg, consts, dialogs, paramsStorage, appsMirroringService, localNotifyCenter){
        "ngInject";

        var m = 'deviceAppsCtrl';

        $scope.currentLoc = null;
        $scope.currentDevice = null;
        $scope.mirrorDevices = false;

        $scope.appsLoading = false;
        $scope.firstAppActivated = false;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            updateCurrentData();
        }

        init();
        
        /*============ Events ============*/

        $scope.$watch('userDataLoaded', function(newVal){
            if(!newVal) return resetCurrentData();
            else updateCurrentData();
        });

        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options){
            updateCurrentData();
        });

        /*============ UI Events ============*/

        $scope.deviceAppsWeightChanged = function($event, device){
            if(!device) return;
            log.debug('saving apps data', m);
            device.saveActiveApps(function(err){
                if(err) { norify.err('Saving apps data error.'); return log.err(err, m); }
                log.debug('saving apps data', m);
                toastMsg.success('Apps data updated!');
            });
        }

        $scope.deviceAppActiveChanged = function(app){
            log.debug('app state changed', m);
            $scope.currentDevice.sortAppsByActiveAndInactive();
            // first activated
            if(!$scope.firstAppActivated){
                $scope.userParams.set(consts.local.user.firstAppActivated, true);
                $scope.firstAppActivated = true;
            }
        }

        $scope.mirrorDevicesChanged = function($event){
            log.debug('mirror devices changed', m);
            if($scope.currentLoc){
                setMirrorEnablForLoc($scope.currentLoc, $scope.mirrorDevices);
            }
            if($scope.mirrorDevices){
                var opt = {
                    templateUrl: 'app/components/deviceApps/enableMirroring/enableMirroringDialogView.html',
                    controller: 'enableMirroringDialogCtrl'
                }
                dialogs.modal(opt, function(){
                    log.debug('enable mirroring confirmed', m);
                    $scope.appsLoading = true;
                    mirrorDevices(function(err){
                        $scope.appsLoading = false;
                        if(err){
                            toastMsg.err('Mirroring devices activation error.');
                            return log.err(err, m);
                        }
                        toastMsg.success('Mirroring enabled!');
                    });
                }, function(){
                    log.debug('enable mirroring canceled', m);
                    $scope.mirrorDevices = false;
                });
                
            }
        }

        /*============ Location ============*/

        function updateCurrentData(){
            log.debug('updating currentLoc', m);
            if(!$scope.user) return resetCurrentData();
            if($state.params.locId && $state.params.deviceId){
                var loc = $scope.user.locationWithId($state.params.locId);
                if(!loc) return pagesService.loadRootPage();

                var devices = $scope.user.devicesAtLocation(loc);
                if(!devices.length) return pagesService.loadRootPage();

                var device = _.find(devices, function(device){
                    return device.id == $state.params.deviceId;
                });
                if(!device) return pagesService.loadRootPage();
                setCurrentData(loc, device);
            }else{
                pagesService.loadRootPage();
            }
        }

        function setCurrentData(location, device){
            if(!device) return;
            if(!location) return;
            $scope.currentLoc = location;
            $scope.currentDevice = device;

            // updating first activation data
            var firstAppActivatedVal = $scope.userParams.get(consts.local.user.firstAppActivated);
            $scope.firstAppActivated = firstAppActivatedVal ? true : false;

            // updating mirror devices data
            $scope.mirrorDevices = getMirrorEnablForLoc($scope.currentLoc);
            if(typeof $scope.mirrorDevices !== "boolean"){
                $scope.mirrorDevices = false;
                setMirrorEnablForLoc($scope.currentLoc, $scope.mirrorDevices);
            }

            // updating devices apps
            if(device.appsPopulated) return;
            $scope.appsLoading = true;
            log.debug('updating apps list', m);
            device.populateAppsAndInstallUninstalled(function(err){
                $scope.appsLoading = false;
                if(err) { norify.err('Apps data loading error.'); return log.err(err, m); }
                log.debug('updating apps list done', m);

                if(!$scope.firstAppActivated && device.activeApps.length){
                    log.debug('setting firstAppActivated to true cos user already have activated applications', m);
                    $scope.userParams.set(consts.local.user.firstAppActivated, true);
                }else if(!$scope.firstAppActivated){
                    showActivateFirstAppDialog();
                }

            });

        }

        function resetCurrentData(){
            $scope.currentLoc = null;
            $scope.currentDevice = null;
        }

        /*============ Dialogs ============*/

        function showActivateFirstAppDialog(){
            log.debug('showing activate first app dialog', m);
            dialogs.modal({
                templateUrl: 'app/components/deviceApps/activateFirstAppInvite/activateFirstAppInviteDialogView.html',
                controller: 'activateFirstAppInviteDialogCtrl',
            });
        }

        /*============ Mirror Devices ============*/

        function mirrorDevicesAppsData(appName, cb){
            if(typeof appName === 'function'){cb = appName; appName = null;}
            var syncOpt = {
                sync: ['data']
            };
            if(appName) syncOpt.name = appName;
            mirrorDevices(syncOpt, function(err){
                if(err) return cb(err);
                _.each($scope.user.devices, function(device){
                    device.sortAppsByActiveAndInactive();
                });
                return cb();
            });
        }

        function mirrorDevicesAppsParams(appName, cb){
            if(typeof appName === 'function'){cb = appName; appName = null;}
            var syncOpt = {
                sync: ['params']
            };
            if(appName) syncOpt.name = appName;
            mirrorDevices(syncOpt, cb);
        }

        function mirrorDevicesAppsSchedule(appName, cb){
            if(typeof appName === 'function'){cb = appName; appName = null;}
            var syncOpt = {
                sync: ['schedule']
            };
            if(appName) syncOpt.name = appName;
            mirrorDevices(syncOpt, cb);
        }

        function mirrorDevices(syncOpt, cb){
            if(typeof syncOpt === 'function'){cb = syncOpt; syncOpt = {};}
            var srcDevice = $scope.currentDevice;
            var destDevices = devicesToMirror();
            if(destDevices.length == 0) return cb();
            log.debug('mirroring devices apps, syncOpt: ' + JSON.stringify(syncOpt), m);
            appsMirroringService.mirrorDevicesApps($scope.currentDevice, destDevices, syncOpt, function(err){
                if(err) return cb(err);
                log.debug('mirroring devices apps done', m);
                return cb();
            });
        }

        function devicesToMirror(){
            var devices = $scope.user.devicesAtLocation($scope.currentLoc);
            return _.filter(devices, function(item){
                return item.id != $scope.currentDevice.id;
            });
        }

        /*============ Mirror state storage ============*/

        function getMirrorEnablForLoc(loc){
            var key = mirrorEnablKeyForLoc(loc);
            return paramsStorage.get(key);
        }

        function setMirrorEnablForLoc(loc, val){
            var key = mirrorEnablKeyForLoc(loc);
            paramsStorage.set(key, val);
        }

        function mirrorEnablKeyForLoc(loc){
            return consts.local.mirrorEnabled + ':' + loc.id;
        }

        /*============ Local Events ============*/

        var unbindAppUpdated = $scope.$on(pandoOrm.notify.app.data.saved, function(notify, updatedApp){
            if(!updatedApp) return;
            if(!$scope.currentDevice) return;
            // checking if we have reveive event from app of current device
            if(!$scope.currentDevice.appWithId(updatedApp.id)) return;
            // mirror devices
            log.debug('app data updated: ' + updatedApp.name, m);
            if($scope.mirrorDevices) mirrorDevicesAppsData(updatedApp.name, function(err){
                if(err) return log.err(err, m);
            });
        });

        var unbindAppUpdated = $scope.$on(pandoOrm.notify.app.params.saved, function(notify, updatedApp){
            if(!updatedApp) return;
            if(!$scope.currentDevice) return;
            // checking if we have reveive event from app of current device
            if(!$scope.currentDevice.appWithId(updatedApp.id)) return;
            // mirror devices
            log.debug('app params updated: ' + updatedApp.name, m);
            if($scope.mirrorDevices) mirrorDevicesAppsParams(updatedApp.name, function(err){
                if(err) return log.err(err, m);
            });
        });

        var unbindAppUpdated = $scope.$on(pandoOrm.notify.app.schedule.saved, function(notify, updatedApp){
            if(!updatedApp) return;
            if(!$scope.currentDevice) return;
            // checking if we have reveive event from app of current device
            if(!$scope.currentDevice.appWithId(updatedApp.id)) return;
            // mirror devices
            log.debug('app schedule updated: ' + updatedApp.name, m);
            if($scope.mirrorDevices) mirrorDevicesAppsSchedule(updatedApp.name, function(err){
                if(err) return log.err(err, m);
            });
        });

        /*============ Destory ============*/

        $scope.$on('$destroy', function(){
            log.debug('destroy', m);
            unbindAppUpdated();
        });

    });