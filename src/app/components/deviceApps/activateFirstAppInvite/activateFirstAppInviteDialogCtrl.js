angular.module('app')
    .controller('activateFirstAppInviteDialogCtrl', function($scope, $uibModalInstance){
		"ngInject";
		
	    var m = 'activateFirstAppInviteDialogCtrl';

	    $scope.ok = function() {
	        $uibModalInstance.dismiss("cancel");
	    };

	    $scope.cancel = function() {
	        $uibModalInstance.dismiss("cancel");
	    };

	    /*============ Init ============*/

	    function init(){
	        log.debug('init', m);
	    }

	    init();

	    /*============ Destroy ============*/

	    $scope.$on('$destroy', function(){
	        log.debug('destroy', m);
	    });
	    
	});