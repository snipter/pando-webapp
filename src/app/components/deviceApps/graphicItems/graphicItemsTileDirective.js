angular.module('app')
	.directive('graphicItemsTile', function(){
		"ngInject";
		return {
			restrict: 'E',
			scope: {item:'=', image: '=', clicked: '&', editClicked: '&', removeClicked: '&'},
			templateUrl: 'app/components/deviceApps/graphicItems/graphicItemsTileView.html',
			replace: true,
			link: function($scope, element, attrs, ctrl){

		        $scope.getCreatedDate = function(){
		        	var date = $scope.item && $scope.item.created ? moment(new Date($scope.item.created)) : moment();
		        	return 'Created at ' + date.format('D MMM YYYY h:mm A');
		        }

		        $scope.getDurationTime = function(){
		        	var duration = $scope.item && $scope.item.duration ? $scope.item.duration : 0;
		        	if(duration <= 60) return duration + ' sec';
		        	if(duration <= 60 * 60) return Math.round(duration/60) + ' min';
		        	return Math.round(duration/(60 * 60)) + ' hrs';
		        }
		        
			}

		}
	});