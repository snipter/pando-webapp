angular.module('app')
    .controller('graphicItemsEditScheduleCtrl', function($scope){
        "ngInject";
        
        var m = 'graphicItemsEditScheduleCtrl';

        $scope.allTimes = true;
        $scope.allDays = true;

        /*============ Watch ============*/

        $scope.$watch('item', function(newVal){
            if(newVal){
                $scope.allDays = isAllDays($scope.item.schedule.days) ? true : false;
                $scope.allTimes = isAllTimes($scope.item.schedule.timeOfDay) ? true : false;
            }else{
                $scope.allTimes = true;
                $scope.allDays = true;
            }
        });

        /*============ UI Events ============*/

        $scope.allTimesChanged = function(){
            if($scope.allTimes){
                $scope.item.schedule.timeOfDay = [];
            }else{
                $scope.item.schedule.timeOfDay = [{
                    from: '13:00',
                    to: '14:00'
                }];
            }
        }

        $scope.allDaysChanged = function(){
            if($scope.allDays){
                setAllDaysOfCurrentItem(true);
            }
        }

        /*============ Dates ============*/

        function setAllDaysOfCurrentItem(val){
            _.each($scope.item.schedule.days, function(v, key){
                $scope.item.schedule.days[key] = val;
            })
        }

        function isAllDays(days){
            var vals = _.values(days);
            for(var i = 0; i < vals.length; i++){
                if(!vals[i]) return false;
            }
            return true;
        }

        /*============ Times ============*/

        function isAllTimes(arr){
            if(!arr) return true;
            if(!_.isArray(arr)) return true;
            if(!arr.length) return true;
            return false;
        }

    });