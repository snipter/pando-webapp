angular.module('app')
    .controller('graphicItemsEditDialogCtrl', function($scope, $uibModalInstance, dialogs, toastMsg, app, item, consts, pandoApi, pandoUtils){
        "ngInject";

        var m = 'graphicItemsEditDialogCtrl';

        $scope.saving = false;
        $scope.removing = false;

        $scope.item = item;
        $scope.app = app;

        $scope.rule = null;
        $scope.ruleLoading = false;
        $scope.ruleEventTypes = _.values(pandoApi.rules.eventTypes);

        /*============ UI Events ============*/

        $scope.saveClicked = function($event){
            log.debug('save clicked', m);
            $scope.saving = true;
            saveCurrentItem(function(err){
                $scope.saving = false;
                if(err){
                    toastMsg.dataSavingErr();
                    return log.err(err, m);
                }
                toastMsg.dataSaved();
            });
        }

        $scope.removeClicked = function($event){
            log.debug('remove clicked', m);
            app.itemsRemove($scope.item);
            $scope.removing = true;
            saveItems(function(err){
                $scope.removing = false;
                if(err){
                    toastMsg.dataSavingErr();
                    return log.err(err, m);
                }
                toastMsg.dataSaved();
                if($scope.app.items.length) return showPrev();
                else return closeDialog();
            });
        }

        $scope.cancelClicked = function() {
            log.debug('cancel clicked', m);
            if($scope.saving) return;
            if($scope.removing) return;
            closeDialog();
        };

        $scope.nextClicked = function(){
            log.debug('next clicked', m);
            if($scope.saving) return;
            if($scope.removing) return;
            showNext();
        }

        $scope.prevClicked = function(){
            log.debug('prev clicked', m);
            if($scope.saving) return;
            if($scope.removing) return;
            showPrev();
        }

        $scope.enableRetargetignChanged = function(){
            log.debug('enable retargetting changed', m);
            if(!$scope.rule) return;
            // Clear rule if diactivated
            if(($scope.rule.active === false) && !$scope.rule.id){
                $scope.rule = null;
                return;
            }
            // Set rule default event type if activated
            if(($scope.rule.active === true) && !$scope.rule.id){
                if(!$scope.rule.criteria) $scope.rule.criteria = {}; 
                if(!$scope.rule.criteria.eventType) $scope.rule.criteria.eventType = $scope.ruleEventTypes[0].val; 
            }
        }

        /*============ Navigate items ============*/

        function showNext(){
            if(!app.items.length) return;
            if(app.items.length == 1) return showItem(app.items[0]); 
            var index = currentItemIndex();
            if(index == -1) return showItem(app.items[0]);
            if(index >= app.items.length - 1) return showItem(app.items[0]);
            showItem(app.items[index + 1]);
        }

        function showPrev(){
            if(!app.items.length) return;
            if(app.items.length == 1) return showItem(app.items[0]);
            var index = currentItemIndex();
            if(index == -1) return showItem(app.items[0]);
            if(index == 0) return showItem(app.items[app.items.length - 1]);
            showItem(app.items[index - 1]);
        }

        function showItemWithIndex(index){
            if(index >= app.items.length) index = 0;
            if(index < 0) index = 0;
            showItem(app.items[index]);
        }

        function currentItemIndex(){
            var index = _.findIndex(app.items, function(item){
                return item.id == $scope.item.id;
            });
            return index ? index : 0;
        }

        function showItem(item){
            log.debug('showing item: ' + item.src, m);
            $scope.item = item;
            var rule = item.rules && item.rules.length ? item.rules[0] : null;
            setCurrentRule(rule);
        }

        /*============ Saving items ============*/

        function saveItems(cb){
            app.itemsSave(cb);
        }

        function saveCurrentItem(cb){
            app.itemsSave(function(err){
                if(err) return cb(err);
                saveCurrentRule(function(err){
                    if(err) return cb(err);
                    return cb();
                });
            });
        }

        /*============ Setting retarget ============*/

        function setCurrentRule(rule){
            log.debug('setting rule: ' + JSON.stringify(rule), m);
            $scope.rule = rule;
            $scope.ruleLoading = false;
            if(rule && !rule.fullVersion){
                log.debug('updating rule', m);
                $scope.ruleLoading = true;
                rule.update(function(err){
                    $scope.ruleLoading = false;
                    log.debug('updating rule done', m);
                    if($scope.rule != rule) return;
                    if(err){
                        toastMsg.err('Loading rule error');
                        return log.err(err, m);
                    }
                });
            }
        }

        /*============ Saving retarget ============*/

        function saveCurrentRule(cb){
            if(!$scope.rule) return cb();
            if(!$scope.rule.id) return createCurrentRule(cb);
            return updateCurrentRule(cb);
        }

        function createCurrentRule(cb){
            var ruleData = getCurrentRuleData();
            var ruleParams = getCurrentRuleParams();
            var ruleSchedule = getCurrentRuleSchedule();
            log.debug('creating rule', m);
            log.trace('ruleData: ', m);
            log.trace(ruleData, m);
            log.trace('ruleParams: ', m);
            log.trace(ruleParams, m);
            log.trace('ruleSchedule: ', m);
            log.trace(ruleSchedule, m);
            app.rulesCreate(ruleData, ruleParams, ruleSchedule, function(err, rule){
                if(err) return cb(err);
                log.debug('creating rule done', m);
                log.trace(rule, m);
                $scope.rule = rule;
                if(!$scope.item.rules) $scope.item.rules = [];
                $scope.item.rules.push(rule);
                if(!app.rules) app.rules = [];
                app.rules.push(rule);
                return cb();
            });
        }

        function updateCurrentRule(cb){
            var rule = $scope.rule;
            rule.params = getCurrentRuleParams();
            // Updating duration
            if(!rule.application) rule.application = {};
            if($scope.item.duration != rule.application.duration){
                rule.application.duration = $scope.item.duration;
            }
            // Updating rule
            log.debug('updating rule', m);
            rule.save(function(err){
                if(err) return cb(err);
                log.debug('updating rule done', m);
                log.debug('saving params', m);
                rule.paramsSave(function(err){
                    if(err) return cb(err);
                    log.debug('saving params done', m);
                    return cb();
                });
            })
        }

        function getCurrentRuleData(){
            var ruleData = pandoUtils.clone($scope.rule);
            if($scope.item.duration){
                ruleData.application = {duration: $scope.item.duration};
            }
            return ruleData;
        }

        function getCurrentRuleParams(){
            var ruleParams = pandoUtils.clone($scope.item);
            if(ruleParams.rules) delete ruleParams.rules;
            if(ruleParams.rule) delete ruleParams.rule;
            if(ruleParams.$$hashKey) delete ruleParams.$$hashKey;
            if(ruleParams.uploading !== undefined) delete ruleParams.uploading;
            if(ruleParams.processing !== undefined) delete ruleParams.processing;
            return ruleParams;
        }

        function getCurrentRuleSchedule(){
            return {
              "days": {
                "mon": true, "tue": true, "wed": true, "thu": true, "fri": true, "sat": true, "sun": true
              },
              "start": "-999999999-01-01T00:00:00",
              "stop": "+999999999-12-31T23:59:59.999999999",
              "timeOfDay": []
            }
        }

        /*============ Dialog ============*/

        function closeDialog(){
            $uibModalInstance.dismiss("cancel");
        }

        /*============ Init ============*/

        function init(){
            showItem(item);
        }

        init();

    });