angular.module('app')
    .controller('graphicItemsEditIdsCtrl', function($scope, utils){
        "ngInject";
        
        var m = 'graphicItemsEditIdsCtrl';

        $scope.rulePandoIdsEnabled = false;
        $scope.rulePandoIdsStr = '';

        /*============ Watch ============*/

        $scope.$watch('rule', function(newVal){
            updateView();
        });

        $scope.$watch('rule.criteria', function(newVal){
            updateView();
        });

        /*============ View ============*/

        function updateView(){
            $scope.rulePandoIdsEnabled = false;
            $scope.rulePandoIdsStr = '';
            if(!$scope.rule) return;
            var rule = $scope.rule;
            if(utils.checkNested(rule, 'criteria', 'visitorsList')){
                var visitorsList = rule.criteria.visitorsList;
                if(!_.isArray(visitorsList)) return;
                if(!visitorsList.length) return;
                $scope.rulePandoIdsEnabled = true;
                $scope.rulePandoIdsStr = pandoIdsToStr(visitorsList);
            }
        }

        /*============ UI Events ============*/

        $scope.idsEnabledChanged = function(){
            if($scope.rule) return;
            var rule = $scope.rule;
            if($scope.rulePandoIdsEnabled){
                if(!rule.criteria) rule.criteria = {};
                if(!rule.criteria.visitorsList) rule.criteria.visitorsList = strToPandoIds($scope.rulePandoIdsStr);
            }else{
                if(utils.checkNested(rule, 'criteria', 'visitorsList')){
                    delete rule.criteria.visitorsList;
                }
            }
        }

        $scope.idsListChanged = function(){
            if(!$scope.rule) return;
            if(!$scope.rulePandoIdsEnabled) return;
            var rule = $scope.rule;
            if(!rule.criteria) rule.criteria = {};
            rule.criteria.visitorsList = strToPandoIds($scope.rulePandoIdsStr);
        }

        /*============ Pando Ids ============*/

        function pandoIdsToStr(arr){
            return _.reduce(arr, function(memo, item){
                if(!memo) return item;
                else return memo + ', ' + item;
            }, '');
        }

        function strToPandoIds(str){
            str = str.toLowerCase();
            str = str.replace(/,/g, ' ');
            str = str.replace(/\s+/g, ' ');
            str = str.trim();
            var arr = str.split(' ');
            arr = _.compact(arr);
            arr = _.uniq(arr);
            return arr;
        }

    });