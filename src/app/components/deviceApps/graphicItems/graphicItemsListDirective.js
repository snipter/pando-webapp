angular.module('app')
	.directive('graphicItemsList', function(dialogs, toastMsg, localNotifyCenter){
		"ngInject";
		
		return {
			restrict: 'E',
			scope: {
				app:'='
			},
			templateUrl: 'app/components/deviceApps/graphicItems/graphicItemsListView.html',
			replace: true,
			link: function($scope, element, attrs, ctrl){

				var m = "graphicItemsList";

				$scope.loading = false;
				$scope.saving = false;

				/*============ Watch ============*/

				$scope.$watch('app', function(newVal, oldVal){
					if(newVal){
						loadItems();
					}
				});

				/*============ Sortable ============*/

				$scope.sortableOptions = {
				    stop: function(e, ui){
				        saveItems();
				        $scope.$apply(); 
				    }
				};

				/*============ UI Events ============*/

				$scope.editClicked = function($event, item){
					log.debug('edit item clicked', m);
					if(item.uploading) return;
					if(item.processing) return;
					editDialogForItem(item);
				};

				$scope.removeClicked = function($event, item){
					log.debug('remove item clicked', m);
					var dialogOpt = {
						title: 'Removing item', 
						text: 'Are you sure you want to remove this item?'
					};
					dialogs.confirm($event, dialogOpt, function(){
						removeItem(item);
						saveItems();
					});
				};

				/*============ Items ============*/

				function loadItems(){
					if(!$scope.app) return;
					log.debug('loading items', m);
					$scope.loading = true;
					$scope.app.itemsPopulate(function(err){
						$scope.loading = false;
					    if(err){
					        toastMsg.dataLoadingErr();
					        return log.err(err, m);
					    }
					    log.debug('loading items done', m);
					    notifyAppUpdated();
					});
				}

				function saveItems(){
					if(!$scope.app) return;
					log.debug('saving items', m);
					$scope.saving = true;
					$scope.app.itemsSave(function(err){
						$scope.saving = false;
						if(err){
							toastMsg.err('Data saving error');
							toastMsg.dataLoadingErr();
						}
						log.debug('saving items done', m);
						notifyAppUpdated();
						toastMsg.success('Data saved!');
					});
				}

				function removeItem(item){
					if(!$scope.app) return;
					$scope.app.itemsRemove(item);
				}

				/*============ Edit dialog ============*/

				function editDialogForItem(item){
					var opt = {
						templateUrl: 'app/components/deviceApps/graphicItems/graphicItemsEditDialogView.html',
						controller: 'graphicItemsEditDialogCtrl',
						size: 'xlg',
						windowClass: 'pando-modal pando-modal_center',
						resolve: {
						    item: function () { return item; },
						    app: function(){ return $scope.app; },
						}
					};
					log.debug('show edit item modal', m);
					dialogs.modal(opt, function(item){
						log.debug('edit item modal dismissed', m);
					}, function(){
						log.debug('edit item modal dismissed', m);
					});
				}

				/*============ Notify ============*/

				var unbindAppItemClicked = localNotifyCenter.onAppItemClicked(function(appName, itemData){
					if(!$scope.app) return;
					if($scope.app.name !== appName) return;
					var item = $scope.app.itemsFind(itemData);
					if(item) editDialogForItem(item);
				});

				var unbindAppItemAdd = localNotifyCenter.onAppItemAdd(function(appName, itemData){
					if(!$scope.app) return;
					if($scope.app.name !== appName) return;
					$scope.app.itemsAdd(itemData);
					/* Save items if item not in uploading process */
					if(!itemData.uploading) saveItems();
				});

				var unbindAppItemsAdd = localNotifyCenter.onAppItemsAdd(function(appName, itemsData){
					if(!$scope.app) return;
					if($scope.app.name !== appName) return;
					$scope.app.itemsAdd(itemsData);
					saveItems();
				});

				var unbindAppItemsUpdated = localNotifyCenter.onAppItemsUpdated(function(appName){
					if(!$scope.app) return;
					if($scope.app.name !== appName) return;
					saveItems();
				});

				function notifyAppUpdated(){
					if(!$scope.app) return;
					localNotifyCenter.broadcastAppUpdated($scope.app.name, $scope.app.items);
				}

				/*============ Init ============*/

				function init(){
					log.debug('init', m);
				}

				init();

				/*============ Destroy ============*/

				$scope.$on('$destroy', function(){
					unbindAppItemClicked();
					unbindAppItemAdd();
					unbindAppItemsAdd();
					unbindAppItemsUpdated();
				    log.debug('destroy', m);
				});

			}
		}
	});