angular.module('app')
    .controller('deviceConfigCtrl', function($scope, $state, toastMsg, dialogs, pagesService, gmtTimeZones){
        "ngInject";
        
        var m = 'deviceConfigCtrl';

        $scope.device = null;
        $scope.deviceData = null;
        $scope.timeZones = gmtTimeZones;

        $scope.dataProcessing = false;
        $scope.saveProcessing = false;
        $scope.restartProcessing = false;
        $scope.removingProcessing = false;

        /*============ Watch ============*/

        $scope.$watch('userDataLoaded', function(newVal){
            if(!newVal) return resetCurrentDevice();
            else updateCurrentDevice();
        });

        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options){
            updateCurrentDevice();
        });

        /*============ UI Events ============*/

        $scope.saveClicked = function(){
            log.debug('save clicked', m);
            saveDeviceData();
        }

        $scope.cancelClicked = function(){
            log.debug('cancel clicked', m);
            resetDeviceData();
        }

        $scope.restartClicked = function($event){
            log.debug('restart clicked', m);
            var opt = {
                title: 'Restart device',
                content: 'Do you really want to restart this device?',
                ok: 'Restart'
            }
            dialogs.confirm($event, opt, function(){
                log.debug('restart confirmed', m);
                restartDevice();
            });
        }

        $scope.removeClicked = function($event){
            log.debug('remove clicked', m);
            var opt = {
                title: 'Remove device',
                content: 'Do you really want to delete this device?',
                ok: 'Remove'
            }
            dialogs.confirm($event, opt, function(){
                log.debug('remove confirmed', m);
                removeDevice();
            });
        }

        /*============ Device ============*/

        function updateCurrentDevice(){
            if(!$scope.user) return resetCurrentDevice();
            if(!$scope.userDataLoaded) return resetCurrentDevice();
            if(!$state.params.deviceId) return pagesService.loadRootPage();
            var device = _.find($scope.user.devices, function(item){
                return item.id == $state.params.deviceId;
            });
            if(!device) return pagesService.loadRootPage();
            setCurrentDevice(device);
        }

        function resetCurrentDevice(){
            $scope.device = null;
            $scope.deviceData = null;
        }

        function setCurrentDevice(device){
            if(!device) return;
            if(device.hardwareConfigurationPopulated) return setCurrentDeviceData(device);
            log.debug('populating device hardware configuration', m);
            $scope.dataProcessing = true;
            device.populateHardwareConfiguration(function(err){
                $scope.dataProcessing = false;
                if(err){
                    toastMsg.err('Loading device configuration error');
                    return log.err(err, m);
                }
                log.debug('populating device hardware configuration done', m);
                setCurrentDeviceData(device);
            });
        }

        function setCurrentDeviceData(device){
            $scope.device = device;
            $scope.deviceData = device.data();
            log.debug('setting current device' , m);
            log.trace($scope.deviceData, m);
        }

        function saveDeviceData(){
            var data = $scope.deviceData;
            log.debug('saving device', m);
            log.trace(data, m);
            $scope.device.setData($scope.deviceData);
            $scope.saveProcessing = true;
            $scope.device.save(function(err){
                $scope.saveProcessing = false;
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Save device error.');
                }
                log.debug('saving device done', m);
                toastMsg.success('Device data saved successfully!');
            });
        }

        function resetDeviceData(){
            setCurrentDevice($scope.device);
        }

        function restartDevice(){
            log.debug('restarting device', m);
            $scope.restartProcessing = true;
            $scope.device.restart(function(err, data){
                $scope.restartProcessing = false;
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Error during restart');
                }
                log.debug('restarting devie done', m);
                toastMsg.success('Device restarted successfully');
            });
        }

        function removeDevice(){
            log.debug('removing device', m);
            $scope.removingProcessing = true;
            $scope.device.remove(function(err){
                $scope.removingProcessing = false;
                if(err){
                    toastMsg.err('Device removing error.');
                    return log.err(err, m);
                }
                log.debug('removing device done', m);
                $scope.user.devices = _.filter($scope.user.devices, function(item){
                    return item.id != $scope.device.id;
                });
                resetCurrentDevice();
                pagesService.loadRootPage();
            });
        }

        /*============ Init ============*/

        function init(){
            updateCurrentDevice();
            log.debug('init', m);
        }

        init();
        
    });