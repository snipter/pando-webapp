angular.module('app')
    .controller('adminSignleDeviceCtrl', function($scope, $interval, utils, pandoApi, consts, toastMsg, dialogs){
        "ngInject";
        
        var m = 'adminSignleDeviceCtrl';
        var loadingLimit = 100;

        $scope.loading = false;
        $scope.logEntries = [];
        $scope.productionStatus = false;
        $scope.info = null;
        $scope.realtime = null;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Events ============*/

        $scope.$watch('device', function(newVal, oldVal){
            if(!$scope.device){
                $scope.logEntries = [];
                $scope.loading = false;
                return;
            }
        });

        $scope.$watch('hidden', function(newVal, oldVal){
            if(newVal === false) updateData();
        });

        $scope.productionStatusChanged = function($event){
            log.debug('production status changed', m);
            var opt = {
                title: 'Changing produciton status', 
                text: 'Do you really want to change device production status to: ' + JSON.stringify($scope.productionStatus) + '?',
                ok: 'Change'
            };
            dialogs.confirm($event, opt, function(){
                log.debug('changing production stauts confirmed', m);
                changeProductionStatus($scope.productionStatus);
            }, function(){
                $scope.productionStatus = !$scope.productionStatus;
            });
        }

        $scope.requestLogsClicked = function($event){
            if(!$scope.device) return;
            log.debug('request device logs clicked', m);
            var linesCount = prompt("Please enter lines count", "1000");
            if(!linesCount) return;
            linesCount = parseInt(linesCount);
            if(linesCount) requestDeviceLogs(linesCount);
        };

        $scope.logEntryClicked = function($event, entry){
            log.debug('log entry clicked: ' + JSON.stringify(entry), m);
            log.debug('downloading log file', m);
            entry.downloading = true;
            pandoApi.admin.devices.logs.download($scope.device, entry, function(err){
                entry.downloading = false;
                if(err){
                    log.err(err, m);
                    toastMsg.err('Downloading log error');
                }else{
                    log.debug('downloading log file done', m);
                }
                $scope.$apply();
            });
        }

        $scope.removeLogEntryClicked = function($event, entry){
            log.debug('remove log entry clicked: ' + JSON.stringify(entry), m);
            var opt = {
                title: 'Removing log entry', 
                text: 'Are you sure you want to remove log entry: ' + entry.entryId + '?',
                ok: 'Remove'
            };
            dialogs.confirm($event, opt, function(){
                log.debug('remove log entry confirmed', m);
                removeLogEntry(entry);
            });
        }

        /*============ Functions ============*/

        function updateData(){
            var tasks = [
                updateLogs,
                updateProductionStatus,
                updateInfo,
                updateRealtime
            ];
            $scope.loading = true;
            async.parallel(tasks, function(err){
                $scope.loading = false;
            })
        }

        function updateLogs(cb){
            log.debug('getting device logs', m);
            pandoApi.admin.devices.logs.list($scope.device.deviceId, {limit: loadingLimit}, function(err, logEntriesData){
                if(err) {
                    toastMsg.err('Getting device logs error');
                    return log.err(err, m, cb);
                }
                log.debug('getting device logs done', m);
                log.trace(logEntriesData, m);
                $scope.logEntries = logEntriesData.content;
                return cb();
            })
        }

        function updateProductionStatus(cb){
            log.debug('updating production status', m);
            pandoApi.admin.devices.productionStatus.get($scope.device.deviceId, function(err, productionStatus){
                if(err) {
                    toastMsg.err('Getting production status error');
                    return log.err(err, m, cb);
                }
                log.debug('updating production status done', m);
                log.trace(productionStatus, m);
                $scope.productionStatus = productionStatus;
                return cb();
            });
        }

        function updateInfo(cb){
            log.debug('updating device info', m);
            pandoApi.devices.get($scope.device.deviceId, function(err, deviceData){
                if(err) {
                    toastMsg.err('Getting device info error');
                    return log.err(err, m, cb);
                }
                log.debug('updating device info done', m);
                log.trace(deviceData, m);
                $scope.info = deviceData;
                return cb();
            });
        }

        function updateRealtime(cb){
            log.debug('updating device realtime', m);
            pandoApi.devices.realtime.get($scope.device.locationId, $scope.device.deviceId, function(err, realtimeData){
                if(err) {
                    toastMsg.err('Getting device realtime data` error');
                    return log.err(err, m, cb);
                }
                log.debug('updating device realtime done', m);
                log.trace(realtimeData, m);
                $scope.realtime = realtimeData;
                return cb();
            });
        }

        function changeProductionStatus(productionStatus){
            log.debug('changing production status to: ' + JSON.stringify(productionStatus), m);
            pandoApi.admin.devices.productionStatus.update($scope.device.deviceId, productionStatus, function(err){
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Changing production status error');
                }
                log.debug('changing production status done', m);
                toastMsg.success('Production status changed');
            })
        }

        function requestDeviceLogs(linesCount){
            log.debug('requesting logs', m);
            var reqData = {};
            reqData.linesCount = linesCount ? linesCount : 1000;
            pandoApi.admin.devices.logs.request($scope.device.deviceId, reqData, function(err, entryData){
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Requesting logs error');
                }
                log.debug('requesting logs done', m);
                log.trace(entryData, m);
                $scope.logEntries.unshift(entryData);
                return toastMsg.success('Requesting logs succeed');
            })
        }

        function removeLogEntry(entry){
            log.debug('removing log entry: ' + entry.entryId, m);
            pandoApi.admin.devices.logs.remove($scope.device.deviceId, entry.entryId, function(err){
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Removing log entry error');
                }
                $scope.logEntries = _.filter($scope.logEntries, function(item){
                    return item.entryId != entry.entryId;
                });
                log.debug('removing log entry done', m);
                toastMsg.success('Removing log entry done');
            });
        }

        /*============ Notifications ============*/

        var unbindAdminRealtimeUpdate = $scope.$on(consts.notify.admin.realtime.update, function(){
            if($scope.hidden) return;
            updateRealtime(function(){
                log.debug('updating realtime iteration done', m);
            });
        })

        /*============ Destroy ============*/

        $scope.$on('$destroy', function(){
            unbindAdminRealtimeUpdate();
            log.debug('destroy', m);
        });

    });