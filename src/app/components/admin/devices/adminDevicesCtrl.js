angular.module('app')
    .controller('adminDevicesCtrl', function($scope, $interval, utils, pandoApi, consts, toastMsg, dialogs) {
        "ngInject";
        
        var m = 'adminDevicesCtrl';
        var loadingLimit = 100;

        $scope.devices = [];
        $scope.loading = false;
        $scope.loadingMore = false;
        $scope.loadingMoreAvaliable = true;

        var realtimeAutoupdateHandler = null;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
            runRealtimeAutoupdate();
        }

        init();

        /*============ Events ============*/

        $scope.$watch('user', function(newVal, oldVal){
            if(!$scope.user) return;
            if(!$scope.user.isAdmin) return;
            updateData();
        });

        $scope.removeDeviceClicked = function($event, device){
            log.debug('remove device clicked', m);
            var opt = {
                title: 'Removing device', 
                text: 'Are you sure you want to remove device: ' + device.name + '?',
                ok: 'Remove'
            };
            dialogs.confirm($event, opt, function(){
                log.debug('remove device confirmed', m);
                removeDevice(device);
            });
        }

        $scope.loadMoreClicked = function(){
            log.debug('load more clicked', m);
            getDevices($scope.devices.length);
        }

        /*============ Functions ============*/

        function updateData(){
            log.debug('updating devices list', m);
            getDevices();
        }

        function getDevices(offset){
            log.debug('updating devices list', m);
            var opt = {limit: loadingLimit};
            if(offset){
                opt.offset = offset;
                $scope.loadingMore = true;
            }else{
                $scope.loading = true;
            }
            pandoApi.admin.devices.list(opt, function(err, devicesData){
                if(offset) $scope.loadingMore = false;
                else $scope.loading = false;
                if(err){return log.err(err, m);}
                log.debug('updating devices list done', m);
                log.trace(devicesData, m);
                $scope.devices = $scope.devices.concat(devicesData.content);
                if(devicesData.content.length >= loadingLimit) $scope.loadingMoreAvaliable = true;
                else $scope.loadingMoreAvaliable = false;
            });
        }

        function removeDevice(device){
            log.debug('removing device: ' + device.deviceId, m);
            pandoApi.admin.devices.remove(device.deviceId, function(err){
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Removing device error');
                }
                $scope.devices = _.filter($scope.devices, function(item){
                    return item.deviceId != device.deviceId;
                });
                log.debug('removing device done', m);
                toastMsg.success('Removing device done');
            });
        }

        function runRealtimeAutoupdate(){
            realtimeAutoupdateHandler = $interval(function(){
                $scope.$broadcast(consts.notify.admin.realtime.update);
            }, utils.second * 30);
        }

        /*============ Destroy ============*/

        $scope.$on('$destroy', function(){
            if(realtimeAutoupdateHandler){
                $interval.cancel(realtimeAutoupdateHandler);
            }
            log.debug('destroy', m);
        });

    });