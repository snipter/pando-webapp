angular.module('app')
    .controller('adminFirmwaresCtrl', function($scope, utils, pandoApi, consts, toastMsg, dialogs){
        "ngInject";
        
        var m = 'adminFirmwaresCtrl';
        var loadingLimit = 100;

        $scope.items = [];
        $scope.loading = false;
        $scope.loadingMore = false;
        $scope.loadingMoreAvaliable = true;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Events ============*/

        $scope.$watch('user', function(newVal, oldVal){
            if(!$scope.user) return;
            if(!$scope.user.isAdmin) return;
            updateItems();
        });

        $scope.removeItemClicked = function($event, item){
            log.debug('remove item clicked', m);
            var opt = {
                title: 'Removing firmware', 
                text: 'Are you sure you want to remove firmware for  ' + item.hardwareType + ' (' + item.version + ')?',
                ok: 'Remove'
            };
            dialogs.confirm($event, opt, function(){
                log.debug('remove item confirmed', m);
                removeItem(item);
            });
        }

        $scope.productionAllowedChanged = function($event, item){
            log.debug('production allowed changed', m);
            var opt = {
                title: 'Changing produciton allowed status', 
                text: 'Do you really want to change ' + item.hardwareType + ' (' + item.version + ') allowed status to: ' + JSON.stringify(item.productionAllowed) + '?',
                ok: 'Change'
            };
            dialogs.confirm($event, opt, function(){
                log.debug('production allowed changing confirmed', m);
                updateItem(item);
            }, function(){
                item.productionAllowed = !item.productionAllowed;
            });
        }

        $scope.uploadItemClicked = function(){
            log.debug('upload item clicked', m);
            showUploadItemDialog();
        }

        $scope.loadMoreClicked = function(){
            log.debug('load more clicked', m);
            getItems($scope.items.length);
        }

        /*============ Functions ============*/

        function updateItems(){
            log.debug('updating items list', m);
            getItems();
        }

        function getItems(offset){
            log.debug('updating items list', m);
            var opt = {limit: loadingLimit};
            if(offset){
                opt.offset = offset;
                $scope.loadingMore = true;
            }else{
                $scope.loading = true;
            }
            pandoApi.admin.firmwares.list(opt, function(err, itemsData){
                if(offset) $scope.loadingMore = false;
                else $scope.loading = false;
                if(err){return log.err(err, m);}
                log.debug('updating items list done', m);
                log.trace(itemsData, m);
                $scope.items = $scope.items.concat(itemsData.content);
                if(itemsData.content.length >= loadingLimit) $scope.loadingMoreAvaliable = true;
                else $scope.loadingMoreAvaliable = false;
            });
        }

        function removeItem(item){
            log.debug('removing item: ' + JSON.stringify(item), m);
            pandoApi.admin.firmwares.remove(item.versionId, function(err){
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Removing firmware error');
                }
                $scope.items = _.filter($scope.items, function(arrItem){
                    return item.versionId != arrItem.versionId;
                });
                log.debug('removing item done', m);
                toastMsg.success('Removing firmware done');
            });
        }

        function updateItem(item){
            log.debug('updating item: ' + JSON.stringify(item), m);
            item = utils.clone(item);
            if(item.$$hashKey) delete item.$$hashKey;
            pandoApi.admin.firmwares.update(item.versionId, item, function(err, firmwareData){
                if(err){
                    toastMsg.err('Firmware updating error');
                    return log.err(err, m);
                }
                log.debug('updating item done', m);
                log.trace(firmwareData, m);
                toastMsg.success('Firmware updating done');
            })
        }

        function showUploadItemDialog(){
            var opt = {
                templateUrl: 'app/components/admin/firmwares/adminFirmwaresUploadDialogView.html',
                controller: 'adminFirmwareUploadDialogCtrl',
                resolve: {
                    items: function(){return $scope.items;}
                }
            };
            log.debug('showning upload item dialog', m);
            dialogs.modal(opt, function(item){
                log.debug('upload item dialog confirmed', m);
                $scope.items.unshift(item);
                toastMsg.success('FIrmware have been uploaded successfully!');
            }, function(){
                log.debug('upload item dialog canceled', m);
            });
        }
    });