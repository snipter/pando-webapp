angular.module('app')
    .controller('adminFirmwareUploadDialogCtrl', function($scope, $uibModalInstance, utils, pandoApi, consts, items, toastMsg){
        "ngInject";

        var m = 'adminFirmwareUploadDialogCtrl';

        $scope.hardwareTypes = _.values(pandoApi.hardwareTypes);
        $scope.hardware = $scope.hardwareTypes[0].val;
        $scope.version = '';
        $scope.file = null;
        $scope.processing = false;

        /*============ Events ============*/

        $scope.clearFileClicked = function(){
            $scope.file = null;
        }

        /*============ Modal ============*/

        $scope.cancel = function() {
           log.debug('cancel clicked', m);
           dismissDialog();
        };

        $scope.upload = function(){
            log.debug('upload clicked', m);
            // dismissDialog();
            var file = $scope.file;
            var hardware = $scope.hardware;
            var version = $scope.version;
            log.debug('uploading file', m);
            $scope.processing = true;
            pandoApi.admin.firmwares.upload(file, hardware, version, function(event){
                // on progress
                var procent = Math.floor((event.loaded / event.total) * 100);
                log.debug(procent + '% uploaded', m);
            }, function(err, data){
                $scope.processing = false;
                if(err){log.err(err, m); return notfy.error('Uploading hardware error'); }
                log.debug('upload file done', m);
                log.trace(data, m);
                dismissDialogWithData(data);
            });
        }

        /*============ Dialog ============*/

        function dismissDialogWithData(data){
            $uibModalInstance.close(data);
        }

        function dismissDialog(){
            $uibModalInstance.dismiss("cancel");
        }

        /*============ Init ============*/

        function init(){
            initVersions();
        }

        function initVersions(){
            var versions = _.map(items, function(item){ return item.version; });
            var lastVersion = utils.lastVersion(versions);
            var newVersionStr = utils.increseVersion(lastVersion);
            if(!newVersionStr) return;
            $scope.version = newVersionStr;
        }

        init();
    });