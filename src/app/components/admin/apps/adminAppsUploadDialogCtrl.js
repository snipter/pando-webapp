angular.module('app')
    .controller('adminAppsUploadDialogCtrl', function($scope, $uibModalInstance, utils, pandoApi, consts, items, toastMsg){
        "ngInject";
        
        var m = 'adminAppsUploadDialogCtrl';

        $scope.hardwareTypes = _.values(pandoApi.hardwareTypes);
        $scope.hardwareType = $scope.hardwareTypes[0].val;
        $scope.applicationNames = [];
        $scope.applicationName = '';
        $scope.applicationVersion = '';
        $scope.applicationFile = null;
        $scope.processing = false;

        /*============ Events ============*/

        $scope.applictionNameChanged = function(){
            log.debug('application name changed: ' + $scope.applicationName, m);
            var apps = _.filter(items, function(item){ return item.applicationName == $scope.applicationName; });
            if(!apps.length) return;
            var versions = _.map(apps, function(item){ return item.version; });
            var lastVersion = utils.lastVersion(versions);
            var newVersionStr = utils.increseVersion(lastVersion);
            if(!newVersionStr) return;
            $scope.applicationVersion = newVersionStr;
        }

        $scope.$watch('applicationFile', function(){
            if($scope.applicationFile){
                log.debug($scope.applicationFile, m);
            }
        })

        $scope.clearFileClicked = function(){
            $scope.applicationFile = null;
        }

        /*============ Modal ============*/

        $scope.cancel = function() {
           log.debug('cancel clicked', m);
           dismissDialog();
        };

        $scope.upload = function(){
            log.debug('upload clicked', m);
            // dismissDialog();
            var file = $scope.applicationFile;
            var hardware = $scope.hardwareType;
            var name = $scope.applicationName;
            var version = $scope.applicationVersion;
            $scope.processing = true;
            pandoApi.admin.apps.upload(file, hardware, name, version, function(event){
                // on progress
                var procent = Math.floor((event.loaded / event.total) * 100);
                log.debug(procent + '% uploaded', m);
            }, function(err, data){
                $scope.processing = false;
                if(err){log.err(err, m); return notfy.error('Uploading app error'); }
                log.debug('upload done', m);
                log.trace(data, m);
                dismissDialogWithData(data);
            });
        }

        /*============ Dialog ============*/

        function dismissDialogWithData(data){
            $uibModalInstance.close(data);
        }

        function dismissDialog(){
            $uibModalInstance.dismiss("cancel");
        }

        /*============ Init ============*/

        function init(){
            initAppNames();
        }

        function initAppNames(){
            var names = _.map(items, function(item){return item.applicationName; });
            $scope.applicationNames = _.uniq(names);
        }

        init();
    });