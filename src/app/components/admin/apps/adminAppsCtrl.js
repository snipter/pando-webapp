angular.module('app')
    .controller('adminAppsCtrl', function($scope, utils, pandoApi, consts, toastMsg, dialogs){
        "ngInject";
        
        var m = 'adminAppsCtrl';
        var loadingLimit = 100;

        $scope.items = [];
        $scope.loading = false;
        $scope.loadingMore = false;
        $scope.loadingMoreAvaliable = true;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Events ============*/

        $scope.$watch('user', function(newVal, oldVal){
            if(!$scope.user) return;
            if(!$scope.user.isAdmin) return;
            updateItems();
        });

        $scope.removeItemClicked = function($event, item){
            log.debug('remove item clicked', m);
            var opt = {
                title: 'Removing app', 
                text: 'Are you sure you want to remove app: ' + item.name + '?',
                ok: 'Remove'
            };
            dialogs.confirm($event, opt, function(){
                log.debug('remove item confirmed', m);
                removeItem(item);
            });
        }

        $scope.uploadItemClicked = function(){
            log.debug('upload item clicked', m);
            showUploadItemDialog();
        }

        $scope.loadMoreClicked = function(){
            log.debug('load more clicked', m);
            getItems($scope.items.length);
        }

        $scope.saveItem = function(item){
            log.debug('saving item with data:' + JSON.stringify(item), m);
            var data = utils.clone(item);
            if(data.$$hashKey) delete data.$$hashKey;
            item.processing = true;
            pandoApi.admin.apps.update(item.versionId, data, function(err, data){
                item.processing = false; delete item.processing;
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Saving app error');
                }
                log.debug('saving item done', m);
                toastMsg.success('Saving app done');
            })
        }

        /*============ Functions ============*/

        function updateItems(){
            log.debug('updating items list', m);
            getItems();
        }

        function getItems(offset){
            log.debug('updating items list', m);
            var opt = {limit: loadingLimit};
            if(offset){
                opt.offset = offset;
                $scope.loadingMore = true;
            }else{
                $scope.loading = true;
            }
            pandoApi.admin.apps.list(opt, function(err, itemsData){
                if(offset) $scope.loadingMore = false;
                else $scope.loading = false;
                if(err){return log.err(err, m);}
                log.debug('updating items list done', m);
                log.trace(itemsData, m);
                $scope.items = $scope.items.concat(itemsData.content);
                if(itemsData.content.length >= loadingLimit) $scope.loadingMoreAvaliable = true;
                else $scope.loadingMoreAvaliable = false;
            });
        }

        function removeItem(item){
            log.debug('removing item: ' + JSON.stringify(item), m);
            pandoApi.admin.apps.remove(item.versionId, function(err){
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Removing app error');
                }
                $scope.items = _.filter($scope.items, function(arrItem){
                    return item.versionId != arrItem.versionId;
                });
                log.debug('removing item done', m);
                toastMsg.success('Removing app done');
            });
        }

        function showUploadItemDialog(){
            var opt = {
                templateUrl: 'app/components/admin/apps/adminAppsUploadDialogView.html',
                controller: 'adminAppsUploadDialogCtrl',
                resolve: {
                    items: function(){return $scope.items;}
                }
            };
            log.debug('showning upload item dialog', m);
            dialogs.modal(opt, function(item){
                log.debug('upload item dialog confirmed', m);
                $scope.items.unshift(item);
                toastMsg.success('App have been uploaded successfully!');
            }, function(){
                log.debug('upload item dialog canceled', m);
            });
        }
    });