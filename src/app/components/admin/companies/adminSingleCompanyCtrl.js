angular.module('app')
    .controller('adminSingleCompanyCtrl', function($scope, utils, pandoApi, consts, toastMsg, dialogs){
        "ngInject";
        
        var m = 'adminSingleCompanyCtrl';

        $scope.loading = false;
        $scope.devices = [];
        $scope.users = [];
        $scope.billing = null;
        $scope.subscription = null;

        $scope.trialFormShowed = false;
        $scope.trialDevicesCount = 1;
        $scope.trialExpDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
        $scope.trialExpMinDate = new Date();
        $scope.trialExpMaxDate = new Date();
        $scope.trialInProcess = false;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Events ============*/

        $scope.$watch('hidden', function(newVal, oldVal){
            if(!$scope.user) return;
            if(!$scope.user.isAdmin) return;
            if(newVal !== false) return;
            updateData();
        });

        /*============ UI Events ============*/

        $scope.showTrialFormClicked = function(){
            log.debug('asign trial for item:' + JSON.stringify($scope.item), m);
            $scope.trialFormShowed = true;
        }

        $scope.assignTrialCanceled = function(){
            log.debug('asign trial cancel click', m);
            $scope.trialFormShowed = false;
        }

        $scope.assignTrialSubmited = function(){
            log.debug('asign trial submit click', m);
            var subscriptionData = {       
                "planId": "pilot",
                "devicesCount": parseInt($scope.trialDevicesCount),
                // 1994-11-05T08:15:30
                "expiration": moment($scope.trialExpDate).format("YYYY-MM-DDTHH:mm:ss")
            }
            log.debug('asign trial for item:' + JSON.stringify($scope.item), m);
            $scope.trialInProcess = true;
            pandoApi.admin.companies.subscription.update($scope.item.companyId, subscriptionData, function(err, createdSubscriptionData){
                $scope.trialInProcess = false;
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Setting company trial subscription error');
                }
                log.debug('asign trial done', m);
                log.trace(createdSubscriptionData, m);
                $scope.subscription = createdSubscriptionData;
                $scope.trialFormShowed = false;
                toastMsg.success('Subscription updated successfully!');
            });
        }

        /*============ Functions ============*/

        function updateData(){
            log.debug('updating company data', m);
            $scope.loading = true;
            async.parallel([updateUsers, updateDevices, updateBilling, updateSubscription], function(err){
                $scope.loading = false;
            })
        }

        function updateUsers(cb){
            var reqData = {offset: 0, limit: 100, companyId: $scope.item.companyId};
            log.debug('updating users data', m);
            pandoApi.admin.users.list(reqData, function(err, usersData){
                if(err) return log.err(err, m, cb);
                log.debug('updating users data done', m);
                log.trace(usersData, m);
                if(usersData.content) $scope.users = usersData.content;
                return cb();
            });
        }

        function updateDevices(cb){
            var reqData = {offset: 0, limit: 100, companyId: $scope.item.companyId};
            log.debug('updating devices data', m);
            pandoApi.admin.devices.list(reqData, function(err, devicesData){
                if(err) return log.err(err, m, cb);
                log.debug('updating devices data done', m);
                log.trace(devicesData, m);
                if(devicesData.content) $scope.devices = devicesData.content;
                return cb();
            })
        }

        function updateBilling(cb){
            log.debug('updating billing data', m);
            pandoApi.admin.companies.billing.get($scope.item.companyId, function(err, billingData){
                if(err) return log.err(err, m, cb);
                log.debug('updating billing data done', m);
                log.trace(billingData, m);
                $scope.billing = billingData;
                return cb();
            })
        }

        function updateSubscription(cb){
            log.debug('updating subscription data', m);
            pandoApi.admin.companies.subscription.get($scope.item.companyId, function(err, subscrData){
                if(err) return log.err(err, m, cb);
                log.debug('updating subscription data done', m);
                log.trace(subscrData, m);
                $scope.subscription = subscrData;
                return cb();
            })
        }

    });