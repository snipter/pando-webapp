angular.module('app')
    .controller('adminUsersCtrl', function($scope, utils, pandoApi, consts, toastMsg, dialogs){
        "ngInject";
        
        var m = 'adminUsersCtrl';
        var loadingLimit = 100;

        $scope.users = [];
        $scope.loading = false;
        $scope.loadingMore = false;
        $scope.loadingMoreAvaliable = true;

        /*============ Init ============*/

        function init(){
            log.debug('init', m);
        }

        init();

        /*============ Events ============*/

        $scope.$watch('user', function(newVal, oldVal){
            if(!$scope.user) return;
            if(!$scope.user.isAdmin) return;
            getUsers();
        });

        $scope.loadMoreClicked = function(){
            log.debug('load more clicked', m);
            getUsers($scope.users.length);
        }

        $scope.removeItemClicked = function($event, user){
            log.debug('remove user clicked', m);
            var opt = {
                title: 'Removing user', 
                text: 'Are you sure you want to remove user: ' + user.email + '?',
                ok: 'Remove'
            };
            dialogs.confirm($event, opt, function(){
                log.debug('removing user confirmed', m);
                removeUser(user);
            });
        }

        $scope.saveItemClicked = function(item){
            log.debug('save item clicked', m);
            saveItem(item);
        }

        /*============ Functions ============*/

        function updateData(){
            log.debug('updating devices list', m);
            getUsers();
        }

        function getUsers(offset){
            log.debug('updating users list', m);
            var opt = {limit: loadingLimit};
            if(offset){
                opt.offset = offset;
                $scope.loadingMore = true;
            }else{
                $scope.loading = true;
            }
            pandoApi.admin.users.list(opt, function(err, usersData){
                if(offset) $scope.loadingMore = false;
                else $scope.loading = false;
                if(err){return log.err(err, m);}
                log.debug('updating users list done', m);
                log.trace(usersData, m);
                $scope.users = $scope.users.concat(usersData.content);
                if(usersData.content.length >= loadingLimit) $scope.loadingMoreAvaliable = true;
                else $scope.loadingMoreAvaliable = false;
            });
        }

        function removeUser(user){
            log.debug('removing user: ' + user.email, m);
            pandoApi.users.remove(user.userId, function(err){
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Removing user error');
                }
                $scope.users = _.filter($scope.users, function(item){
                    return item.userId != user.userId;
                });
                log.debug('removing user done', m);
                toastMsg.success('Removing user done');
            });
        }

        function saveItem(item){
            log.debug('saving item with data:' + JSON.stringify(item), m);
            var data = utils.clone(item);
            if(data.$$hashKey) delete data.$$hashKey;
            item.processing = true;
            pandoApi.users.update(item.userId, data, function(err, data){
                item.processing = false; delete item.processing;
                if(err){
                    log.err(err, m);
                    return toastMsg.err('Saving user error');
                }
                log.debug('saving item done', m);
                toastMsg.success('Saving user done');
            })
        }

    });