describe('pandoAnalyticsUtils', function(){

	var pandoAnalyticsUtils = null;
	beforeEach(module('app'));
	beforeEach(inject(function(_pandoAnalyticsUtils_){
		pandoAnalyticsUtils = _pandoAnalyticsUtils_;
	}));

	describe('service tests', function(){
		it('shoud exist', function(){
			expect(pandoAnalyticsUtils).to.exist;
		});
	});

	describe('realtime', function(){

		describe('getEventsAndSignalStrengthForDate()', function(){
			var demoEvents = mockups.api.devices.realtime.lastHour.events;
			it('shoud return correct events and signal strength', function(){
				var demoTimeStr = '2016-07-17T23:43:54-08:00';
				var demoTime = new Date(demoTimeStr);
				var realtimeEvents = pandoAnalyticsUtils.realtime.getEventsAndSignalStrengthForDate(demoEvents, demoTime);
				expect(realtimeEvents).to.be.an('array');
				var demoResults = {
					 "815a68dd-b810-434f-949b-575f7952c2f3": 50
					,"4bc88bc7-096e-4cae-9158-ab696ef0693b": 46
					,"5c5d3070-dee2-46fd-b76a-0d3edaec7ced": 42
					,"cffb7e7a-8362-458c-8a1b-e125df4f4fc7": 58
					,"6225617e-0a8f-4173-9f8d-e67773a1ab66": 58
					,"fccb4818-627e-442c-b898-f4e45ef9c963": 98
					,"d286dff9-b438-45d5-afd5-43f64d495701": 58
				};
				expect(realtimeEvents.length).to.be.equal(_.keys(demoResults).length);
				_.each(realtimeEvents, function(realtimeEvent){
					var expectedSignalStrength = demoResults[realtimeEvent.eventId];
					expect(expectedSignalStrength).to.be.ok;
					expect(realtimeEvent.signalStrength).to.equal(expectedSignalStrength);
				});
			});

		});

		describe('filterEventsForDate()', function(){
			it('shoud return error if events not specified', function(){
				var res = pandoAnalyticsUtils.realtime.filterEventsForDate();
				expect(res).to.be.an('error');
			});
			it('shoud return error if time not specified', function(){
				var res = pandoAnalyticsUtils.realtime.filterEventsForDate([]);
				expect(res).to.be.an('error');
			});
			it('shoud return error if events not array', function(){
				var res = pandoAnalyticsUtils.realtime.filterEventsForDate(' ', new Date());
				expect(res).to.be.an('error');
			});
			it('shoud return error if time not date', function(){
				var res = pandoAnalyticsUtils.realtime.filterEventsForDate([], ' ');
				expect(res).to.be.an('error');
			});
			it('shoud return correct events', function(){
				var demoTimeStr = '2016-07-17T23:43:54-08:00';
				var demoTime = new Date(demoTimeStr);
				var res = pandoAnalyticsUtils.realtime.filterEventsForDate(mockups.api.devices.realtime.lastHour.events, demoTime);
				expect(res).to.be.an('array');
				var expectedEevntIds = [
					 "815a68dd-b810-434f-949b-575f7952c2f3"
					,"4bc88bc7-096e-4cae-9158-ab696ef0693b"
					,"5c5d3070-dee2-46fd-b76a-0d3edaec7ced"
					,"cffb7e7a-8362-458c-8a1b-e125df4f4fc7"
					,"6225617e-0a8f-4173-9f8d-e67773a1ab66"
					,"fccb4818-627e-442c-b898-f4e45ef9c963"
					,"d286dff9-b438-45d5-afd5-43f64d495701"
				];
				expect(res.length).to.be.equal(expectedEevntIds.length);
				_.each(res, function(realtimeEvent){
					expect(expectedEevntIds).to.include(realtimeEvent.eventId);
				});
			});
			it('shoud return empty array for date earlier first event', function(){
				var demoTimeStr = '1992-07-17T23:43:54-08:00';
				var demoTime = new Date(demoTimeStr);
				var res = pandoAnalyticsUtils.realtime.filterEventsForDate(mockups.api.devices.realtime.lastHour.events, demoTime);
				expect(res).to.be.an('array');
				expect(res.length).to.be.equal(0);
			});
			it('shoud return empty array for date later last event', function(){
				var demoTimeStr = '2016-07-18T23:43:54-08:00';
				var demoTime = new Date(demoTimeStr);
				var res = pandoAnalyticsUtils.realtime.filterEventsForDate(mockups.api.devices.realtime.lastHour.events, demoTime);
				expect(res).to.be.an('array');
				expect(res.length).to.be.equal(0);
			});
		});

		describe('getEventSignalStrengthForDate()', function(){

			it('shoud return correct signal strength', function(){
				var demoData = [
					 {date: '2016-07-17T23:43:53-08:00', sn: 0}
					,{date: '2016-07-17T23:43:54-08:00', sn: 50}
					,{date: '2016-07-17T23:43:55-08:00', sn: 46}
					,{date: '2016-07-17T23:43:56-08:00', sn: 46}
					,{date: '2016-07-17T23:44:55-08:00', sn: 46}
					,{date: '2016-07-17T23:44:56-08:00', sn: 49}
					,{date: '2016-07-17T23:44:57-08:00', sn: 0}
				];
				var demoEvent = mockups.api.devices.realtime.lastHourEvent;
				_.each(demoData, function(demoDataItem){
					var demoTime = new Date(demoDataItem.date);
					var signalStrength = pandoAnalyticsUtils.realtime.getEventSignalStrengthForDate(demoEvent, demoTime);
					expect(signalStrength).to.be.equal(demoDataItem.sn);
				});
			});

		});

		describe('parseDate()', function(){
			var demoDateStr = '2016-07-17T23:53:08-08:00[GMT-08:00]';
			var demoDateTimestamp = 1468828388000;
			it('shoud return err if not timeStr specified', function(){
				var dateVal = pandoAnalyticsUtils.realtime.parseDate();
				expect(dateVal).to.be.an('error');
			});
			it('shoud return err if timeStr not string', function(){
				var dateVal = pandoAnalyticsUtils.realtime.parseDate([]);
				expect(dateVal).to.be.an('error');
			});
			it('shoud return correct date', function(){
				var dateVal = pandoAnalyticsUtils.realtime.parseDate(demoDateStr);
				expect(dateVal).to.be.a('date');
				expect(dateVal.getTime()).to.be.equal(demoDateTimestamp);
			});
		});

		describe('getRandomFloatForId()', function(){
			var demoId = 'd9601ffd-64ea-4e2f-9d5d-f887c127e00c';
			it('shoud return null if id not specified', function(){
				var val = pandoAnalyticsUtils.realtime.getRandomFloatForId();
				expect(val).to.be.null;
			});
			it('shoud return null if id is array', function(){
				var val = pandoAnalyticsUtils.realtime.getRandomFloatForId([]);
				expect(val).to.be.null;
			});
			it('shoud return null if id is different object', function(){
				var val = pandoAnalyticsUtils.realtime.getRandomFloatForId(new Date());
				expect(val).to.be.null;
			});
			it('shoud return float for id', function(){
				var val = pandoAnalyticsUtils.realtime.getRandomFloatForId(demoId);
				expect(val).to.be.a('number');
			});
			it('shoud return the same number for same id', function(){
				var val1 = pandoAnalyticsUtils.realtime.getRandomFloatForId(demoId);
				var val2 = pandoAnalyticsUtils.realtime.getRandomFloatForId(demoId);
				expect(val1).to.be.equal(val2);
			});
			it('shoud be different for different id', function(){
				var val1 = pandoAnalyticsUtils.realtime.getRandomFloatForId(demoId);
				var val2 = pandoAnalyticsUtils.realtime.getRandomFloatForId(demoId + '1');
				expect(val1).not.to.be.equal(val2);
			})
		});

		describe('getEventMainLabel()', function(){
			it('shoud return walkby if labels not specified', function(){
				var val = pandoAnalyticsUtils.realtime.getEventMainLabel({});
				expect(val).to.equal('walkby');
			});
			it('shoud return walkby if array is empty', function(){
				var val = pandoAnalyticsUtils.realtime.getEventMainLabel({labels: []});
				expect(val).to.equal('walkby');
			});
			it('shoud return correct value for walkby label', function(){
				var demoEvent = {labels: ['walkby', 'some']};
				var val = pandoAnalyticsUtils.realtime.getEventMainLabel(demoEvent);
				expect(val).to.equal('walkby');
			});
			it('shoud return correct value for visit label', function(){
				var demoEvent = {labels: ['visit', 'some']};
				var val = pandoAnalyticsUtils.realtime.getEventMainLabel(demoEvent);
				expect(val).to.equal('visit');
			});
			it('shoud return correct value for visit label', function(){
				var demoEvent = {labels: ['some', 'bounce']};
				var val = pandoAnalyticsUtils.realtime.getEventMainLabel(demoEvent);
				expect(val).to.equal('bounce');
			});
		});

	});
	
});