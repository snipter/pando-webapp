describe('pando.api', function(){

	// Pando API provider
	var thePandoApiProvider = null;
	var apiRoot = null;
	beforeEach(module('pando.api', function(pandoApiProvider) {
	    thePandoApiProvider = pandoApiProvider;
	    apiRoot = thePandoApiProvider.config.apiRoot;
	}));

	// Pando API
	var pandoApi, $http;
	beforeEach(module('pando.api'));
	beforeEach(inject(function(_pandoApi_, _$http_){
		pandoApi = _pandoApi_;
		$http = _$http_;
	}));

	
	describe('pandoApiProvider', function(){
		it('shoud set config', function(){
			var demoKey = 'someKey';
			var demoVal = 'someVal';
			thePandoApiProvider.setConfig(demoKey, demoVal);
			expect(thePandoApiProvider.config[demoKey]).to.exist;
			expect(thePandoApiProvider.config[demoKey]).to.equal(demoVal);
			expect(thePandoApiProvider.getConfig(demoKey)).to.equal(demoVal);
		});
	});


	describe('pandoApi', function(){

		var $httpBackend;
		beforeEach(inject(function(_$httpBackend_){
			$httpBackend = _$httpBackend_;
			$httpBackend.when('GET', /app\/.+?.html/g).respond(200);
		}));

		// Tokens
		describe('tokens', function(){
			describe('setAccessToken()', function(){
				it('shoud call with empty token', function(){
					pandoApi.setAccessToken();
				});
				it('shoud set token', function(){
					var demoToken = '12345asdf';
					pandoApi.setAccessToken(demoToken);
					expect($http.defaults.headers.common['X-Pando-Authentication']).to.equal(demoToken);
				});
			});

			describe('getAccessToken()', function(){
				it('shoud exists', function(){
					expect(pandoApi.getAccessToken).to.be.a('function');
				});
				it('shoud return correct token', function(){
					var demoToken = '12345asdf';
					pandoApi.setAccessToken(demoToken);
					expect(pandoApi.getAccessToken()).to.equal(demoToken);
				})
			})

			describe('removeAccessToken()', function(){
				it('shoud call when token not set', function(){
					pandoApi.removeAccessToken();
				});
				it('shoud remove token', function(){
					var demoToken = '12345asdf';
					pandoApi.setAccessToken(demoToken);
					pandoApi.removeAccessToken();
					expect($http.defaults.headers.common['X-Pando-Authentication']).to.be.undefined;
				});
			});
		});

		// Events
		describe('events', function(){
			it('shoud have observers array', function(){
				expect(pandoApi.observers).to.be.an('array').with.length(0);
			});
			it('shoud have required functions', function(){
				expect(pandoApi.on).to.be.a('function');
				expect(pandoApi.emit).to.be.a('function');
			})
			it('shoud call events', function(){
				var eventCalled = false;
				var wrongEventCalled = false;
				var eventName = 'someEvent';
				var wrongEventName = 'someAnotherEvent';
				var eventData = {some: 'data'};
				pandoApi.on(eventName, function(data){
					eventCalled = true;
					expect(data).to.deep.equal(eventData);
				});
				pandoApi.on(wrongEventName, function(data){
					wrongEventCalled = true;
				});
				pandoApi.emit(eventName, eventData);
				expect(eventCalled).to.be.true;
				expect(wrongEventCalled).to.be.false;
			});
		});

		// Constsnts
		describe('constants', function(){
			it('shoud have hardwareTypes', function(){
				checkApiConstObj(pandoApi.hardwareTypes);
			});
			it('shoud have visitProfiles', function(){
				checkApiConstObj(pandoApi.visitProfiles);
			});
			it('shoud have subscriptionPlans', function(){
				expect(pandoApi.subscriptionPlans).to.exists;
			});
			it('shoud have rules.eventTypes', function(){
				checkApiConstObj(pandoApi.rules.eventTypes);
			});
			function checkApiConstObj(constObj){
				expect(constObj).to.be.an('object');
				var keys = _.keys(constObj);
				expect(keys).to.have.length.above(0);
				_.each(keys, function(key){
					expect(constObj[key]).to.be.an('object');
					expect(constObj[key].name).to.be.a('string');
					expect(constObj[key].val).to.be.a('string');
				});
			}
		});

		// Api calls
		describe('api calls', function(){
			describe('isGetMethod()', function(){
				it('shoud return correct value', function(){
					expect(pandoApi.isGetMethod()).to.be.false;
					expect(pandoApi.isGetMethod('get')).to.be.true;
					expect(pandoApi.isGetMethod('Get')).to.be.true;
					expect(pandoApi.isGetMethod('GET')).to.be.true;
					expect(pandoApi.isGetMethod('POST')).to.be.false;
				})
			});

			describe('getApiReqOpt()', function(){
				var demoMethod = 'get';
				var demoPath = '/rest';
				var demoReqData = {some: 'data'};

				it('shoud exists', function(){
					expect(pandoApi.getApiReqOpt).to.be.a('function');
				});
				it('shoud throw error when method is empty', function(){
					expect(function(){pandoApi.getApiReqOpt()}).to.throw('method not set');
				})
				it('shoud throw error when method is empty', function(){
					expect(function(){pandoApi.getApiReqOpt('get')}).to.throw('path not set');
				});
				it('shoud return requ opt when method and path specified', function(){
					var reqOpt = pandoApi.getApiReqOpt(demoMethod, demoPath);
					expect(reqOpt.method).to.equal(demoMethod);
					expect(reqOpt.url).to.equal(thePandoApiProvider.config.apiRoot + demoPath);
				});
				it('shoud return correct opt when GET method and data specified', function(){
					var reqOpt = pandoApi.getApiReqOpt('get', '/some', demoReqData);
					expect(reqOpt.params).to.deep.equal(demoReqData);
					expect(reqOpt.data).to.be.undefined;
				});
				it('shoud return correct opt when POST method and data specified', function(){
					var reqOpt = pandoApi.getApiReqOpt('post', '/some', demoReqData);
					expect(reqOpt.data).to.deep.equal(demoReqData);
					expect(reqOpt.params).to.be.undefined;
				});
				it('shoud return correct opt when res.transform = false specifided', function(){
					var reqOpt = pandoApi.getApiReqOpt('post', '/some', demoReqData, {res: {transform: false}});
					expect(reqOpt.transformResponse).to.be.an('array').with.length(1);
					expect(reqOpt.transformResponse[0]).to.be.a('function');
					expect(reqOpt.transformResponse[0](demoReqData)).to.deep.equal(demoReqData);
				});
			});

			describe('apiReq()', function(){

				var demoReqPath = 'rest/';
				var demoReqData = {some: 'data'};

				var $httpBackend;
				beforeEach(inject(function(_$httpBackend_){
					$httpBackend = _$httpBackend_;
					$httpBackend.when('GET', /app\/.+?.html/g).respond(200);
				}));

				it('shoud exists', function(){
					expect(pandoApi.apiReq).to.be.a('function');
				});
				it('shoud throw error when method is empty', function(){
					expect(function(){pandoApi.apiReq()}).to.throw('method not set');
				})
				it('shoud throw error when method is empty', function(){
					expect(function(){pandoApi.apiReq('get')}).to.throw('path not set');
				});
				it('shoud make request when data not specidied', function(){
					var succeeded;
					$httpBackend
						.expectGET(apiRoot + demoReqPath)
						.respond(200);
					pandoApi.apiReq('GET', demoReqPath,function(err){
						expect(err).to.be.null;
						succeeded = true;
					});
					$httpBackend.flush();
					expect(succeeded).to.be.true;
				});
				it('shoud make GET request when opt not specidied', function(){
					var succeeded;
					$httpBackend
						.expectGET(apiRoot + demoReqPath + '?some=data')
						.respond(200);
					pandoApi.apiReq('GET', demoReqPath, demoReqData, function(err){
						expect(err).to.be.null;
						succeeded = true;
					});
					$httpBackend.flush();
					expect(succeeded).to.be.true;
				});
				it('shoud make POST request when opt not specidied', function(){
					var succeeded;
					$httpBackend
						.expectPOST(apiRoot + demoReqPath, demoReqData)
						.respond(200);
					pandoApi.apiReq('POST', demoReqPath, demoReqData, function(err){
						expect(err).to.be.null;
						succeeded = true;
					});
					$httpBackend.flush();
					expect(succeeded).to.be.true;
				});
				it('shoud make request when data and opt specified', function(){
					var succeeded;
					$httpBackend
						.expectGET(apiRoot + demoReqPath)
						.respond(200);
					pandoApi.apiReq('GET', demoReqPath, {}, {some: 'opt'}, function(err){
						expect(err).to.be.null;
						succeeded = true;
					});
					$httpBackend.flush();
					expect(succeeded).to.be.true;
				});
				it('shoud return an error when wrong status code not 200', function(){
					var succeeded;
					$httpBackend
						.expectGET(apiRoot + demoReqPath)
						.respond(300);
					pandoApi.apiReq('GET', demoReqPath, function(err){
						expect(err).not.to.be.null;
						succeeded = true;
					});
					$httpBackend.flush();
					expect(succeeded).to.be.true;
				});

			});

			// Auth
			describe('Auth', function(){

				describe('auth.logIn()', function(){
					var demoToken = '123456789';
					var demoData = {email: 'example@gmail.com', password: 'some pass'};
					var demoResponse = mockups.api.auth.login;
					it('shoud make request', function(){
						var succeeded;
						$httpBackend.expectPOST(apiRoot + 'auth/', demoData).respond(200, demoResponse);
						pandoApi.auth.logIn(demoData.email, demoData.password, function(err, data){
							expect(err).to.be.null;
							expect(data.accessToken).to.equal(demoResponse['X-Pando-Authentication']);
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
					it('shoud return error', function(){
						var succeeded;
						$httpBackend.expectPOST(apiRoot + 'auth/', demoData).respond(300);
						pandoApi.auth.logIn(demoData.email, demoData.password, function(err){
							expect(err).not.to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
				});

				describe('auth.logOut()', function(){
					it('shoud make request', function(){
						var succeeded;
						$httpBackend.expectDELETE(apiRoot + 'auth/').respond(200);
						pandoApi.auth.logOut(function(err){
							expect(err).to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
					it('shoud return error', function(){
						var succeeded;
						$httpBackend.expectDELETE(apiRoot + 'auth/').respond(300);
						pandoApi.auth.logOut(function(err){
							expect(err).not.to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
				});

				describe('auth.signUp()', function(){
					it('shoud make request', function(){
						var succeeded;
						var demoData = {some: 'data'};
						$httpBackend.expectPOST(apiRoot + 'signup/', demoData).respond(200);
						pandoApi.auth.signUp(demoData, function(err){
							expect(err).to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
					it('shoud return error', function(){
						var succeeded;
						var demoData = {some: 'data'};
						$httpBackend.expectPOST(apiRoot + 'signup/', demoData).respond(300);
						pandoApi.auth.signUp(demoData, function(err){
							expect(err).not.to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
				});

				describe('auth.signUpAndJoinCompany()', function(){
					var demoData = {some: 'data'};
					it('shoud make request', function(){
						var succeeded;
						$httpBackend.expectPOST(apiRoot + 'signup/join/', demoData).respond(200);
						pandoApi.auth.signUpAndJoinCompany(demoData, function(err){
							expect(err).to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
					it('shoud return error', function(){
						var succeeded;
						$httpBackend.expectPOST(apiRoot + 'signup/join/', demoData).respond(300);
						pandoApi.auth.signUpAndJoinCompany(demoData, function(err){
							expect(err).not.to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
				});

				describe('auth.pass.reset()', function(){
					var email = 'some@gmail.com';
					it('shoud make request', function(){
						var succeeded;
						$httpBackend.expectPOST(apiRoot + 'auth/restore/', {email: email}).respond(200);
						pandoApi.auth.pass.reset(email, function(err){
							expect(err).to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
					it('shoud return error', function(){
						var succeeded;
						$httpBackend.expectPOST(apiRoot + 'auth/restore/', {email: email}).respond(300);
						pandoApi.auth.pass.reset(email, function(err){
							expect(err).not.to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
				})

				describe('auth.pass.restore()', function(){
					var token = 'sometoken';
					var pass = 'somepass';
					it('shoud make request', function(){
						var succeeded;
						$httpBackend.expectPUT(apiRoot + 'auth/restore/', {emailToken: token, password: pass}).respond(200);
						pandoApi.auth.pass.restore(token, pass, function(err){
							expect(err).to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
					it('shoud return error', function(){
						var succeeded;
						$httpBackend.expectPUT(apiRoot + 'auth/restore/', {emailToken: token, password: pass}).respond(300);
						pandoApi.auth.pass.restore(token, pass, function(err){
							expect(err).not.to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
				})

				describe('auth.pass.change()', function(){
					it('shoud make request', function(){
						var succeeded;
						var pass = 'somepass';
						$httpBackend.expectPUT(apiRoot + 'auth/', {password: pass}).respond(200);
						pandoApi.auth.pass.change(pass, function(err){
							expect(err).to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
					it('shoud return error', function(){
						var succeeded;
						var pass = 'somepass';
						$httpBackend.expectPUT(apiRoot + 'auth/', {password: pass}).respond(300);
						pandoApi.auth.pass.change(pass, function(err){
							expect(err).not.to.be.null;
							succeeded = true;
						});
						$httpBackend.flush();
						expect(succeeded).to.be.true;
					});
				})

			});
			// /Auth

			// Users
			// describe('Users', function(){
			// 	var demoUserData = {email: 'some@gmail.com', name: 'Jon Dow'};
			// 	var demoUserId = 1;
			// 	describe('users.list()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/user/').respond(200);
			// 			pandoApi.users.list(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/user/').respond(300);
			// 			pandoApi.users.list(function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('users.add()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/user/', demoUserData).respond(200);
			// 			pandoApi.users.add(demoUserData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/user/', demoUserData).respond(300);
			// 			pandoApi.users.add(demoUserData, function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('users.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/user/' + demoUserId + '/').respond(200);
			// 			pandoApi.users.get(demoUserId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/user/' + demoUserId + '/').respond(300);
			// 			pandoApi.users.get(demoUserId, function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('users.update()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/user/' + demoUserId + '/', demoUserData).respond(200);
			// 			pandoApi.users.update(demoUserId, demoUserData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/user/' + demoUserId + '/', demoUserData).respond(300);
			// 			pandoApi.users.update(demoUserId, demoUserData, function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('users.remove()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/user/' + demoUserId + '/').respond(200);
			// 			pandoApi.users.remove(demoUserId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/user/' + demoUserId + '/').respond(300);
			// 			pandoApi.users.remove(demoUserId, function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// })
			// /Users

			// Locations
			// describe('Locations', function(){
			// 	var demoId = 1;
			// 	var demoData = {
			// 	    "name": "random store",
			// 	    "placeForeignId": "random string",
			// 	    "address": "1 Infinite Loop, Cupertino, California", 
			// 	    "lat": 37.3317157,
			// 	    "lon": -122.0323722
			// 	};
			// 	describe('locations.list()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/location/').respond(200);
			// 			pandoApi.locations.list(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('locations.create()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/location/', demoData).respond(200);
			// 			pandoApi.locations.create(demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('locations.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/location/' + demoId + '/').respond(200);
			// 			pandoApi.locations.get(demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('locations.update()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/location/' + demoId + '/', demoData).respond(200);
			// 			pandoApi.locations.update(demoId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('locations.remove()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/location/' + demoId + '/').respond(200);
			// 			pandoApi.locations.remove(demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// });
			// /Locations

			// Devices
			// describe('Devices', function(){
			// 	var demoId = 1;
			// 	var demoData = mockups.api.devices.get;
			// 	describe('devices.listAll()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/').respond(200, mockups.api.devices.list);
			// 			pandoApi.devices.listAll(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/').respond(300);
			// 			pandoApi.devices.listAll(function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.list()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			var notDeletedDevices = _.filter(mockups.api.devices.listAll, function(item){ return !item.isDeleted; });
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/').respond(200, mockups.api.devices.listAll);
			// 			pandoApi.devices.list(function(err, data){
			// 				expect(err).to.be.null;
			// 				expect(data).to.deep.equal(notDeletedDevices);
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/').respond(300);
			// 			pandoApi.devices.list(function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.create()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/device/', demoData).respond(200);
			// 			pandoApi.devices.create(demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoId + '/').respond(200);
			// 			pandoApi.devices.get(demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoId + '/').respond(300);
			// 			pandoApi.devices.get(demoId, function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.update()', function(){
			// 		it('shoud throw error when data no specified', function(){
			// 			var fn = function(){pandoApi.devices.update(demoId);}
			// 			expect(fn).to.throw('data not set');
			// 		});
			// 		it('shoud throw error when hardwareConfiguration no specified', function(){
			// 			var fn = function(){pandoApi.devices.update(demoId, {});}
			// 			expect(fn).to.throw('hardwareConfiguration not set');
			// 		});
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoId + '/', demoData).respond(200);
			// 			pandoApi.devices.update(demoId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud convert hardwareConfiguration.rotation to int', function(){
			// 			var succeeded;
			// 			var demoData = _.clone(mockups.api.devices.get);
			// 			var modifiedDemoData = _.clone(mockups.api.devices.get);
			// 			modifiedDemoData.hardwareConfiguration.rotation = "0";
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoId + '/', demoData).respond(200);
			// 			pandoApi.devices.update(demoId, modifiedDemoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud convert hardwareConfiguration.resolutionCode to int', function(){
			// 			var succeeded;
			// 			var demoData = _.clone(mockups.api.devices.get);
			// 			var modifiedDemoData = _.clone(mockups.api.devices.get);
			// 			modifiedDemoData.hardwareConfiguration.resolutionCode = "16";
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoId + '/', demoData).respond(200);
			// 			pandoApi.devices.update(demoId, modifiedDemoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoId + '/', demoData).respond(300);
			// 			pandoApi.devices.update(demoId, demoData, function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.remove()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/device/' + demoId + '/').respond(200);
			// 			pandoApi.devices.remove(demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 		it('shoud return error', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/device/' + demoId + '/').respond(300);
			// 			pandoApi.devices.remove(demoId, function(err){
			// 				expect(err).not.to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.ignore.list()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoId + '/ignore/').respond(200);
			// 			pandoApi.devices.ignore.list(demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.ignore.add()', function(){
			// 		var demoMac = 'aa:bb:cc:11:22:35';
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/device/' + demoId + '/ignore/').respond(200, demoMac);
			// 			pandoApi.devices.ignore.add(demoId, demoMac, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.ignore.remove()', function(){
			// 		var demoMac = 'aa:bb:cc:11:22:35';
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/device/' + demoId + '/ignore/' + demoMac + '/').respond(200);
			// 			pandoApi.devices.ignore.remove(demoId, demoMac, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.restart()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/device/' + demoId + '/maintenance/').respond(200);
			// 			pandoApi.devices.restart(demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.firmware.update()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/device/' + demoId + '/firmware/').respond(200);
			// 			pandoApi.devices.firmware.update(demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.realtime.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoId + '/realtime/').respond(200);
			// 			pandoApi.devices.realtime.get(demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.realtime.eventData()', function(){
			// 		var demoEventId = 4;
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoId + '/realtime/' + demoEventId + '/').respond(200);
			// 			pandoApi.devices.realtime.eventData(demoId, demoEventId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('devices.screenshots.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoId + '/screenshot/').respond(200);
			// 			pandoApi.devices.screenshots.get(demoId, {}, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
				
			// });
			// /Devices

			// Apps
			// describe('Apps', function(){
			// 	var demoAppId = 1;
			// 	var demoDeviceId = 2;
			// 	describe('apps.list()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoDeviceId + '/apps/').respond(200);
			// 			pandoApi.apps.list(demoDeviceId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.install()', function(){
			// 		var demoAppName = 'someappname';
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/device/' + demoDeviceId + '/apps/', {name: demoAppName}).respond(200);
			// 			pandoApi.apps.install(demoDeviceId, demoAppName, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.updateDashboard()', function(){
			// 		var demoData = mockups.api.apps.updateDashboard;
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoDeviceId + '/apps/', demoData).respond(200);
			// 			pandoApi.apps.updateDashboard(demoDeviceId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoDeviceId + '/apps/' + demoAppId + '/').respond(200);
			// 			pandoApi.apps.get(demoDeviceId, demoAppId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.update()', function(){
			// 		var demoData = mockups.api.apps.get;
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoDeviceId + '/apps/' + demoAppId + '/', demoData).respond(200);
			// 			pandoApi.apps.update(demoDeviceId, demoAppId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.remove()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/device/' + demoDeviceId + '/apps/' + demoAppId + '/').respond(200);
			// 			pandoApi.apps.remove(demoDeviceId, demoAppId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.schedule.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoDeviceId + '/apps/' + demoAppId + '/schedule/').respond(200);
			// 			pandoApi.apps.schedule.get(demoDeviceId, demoAppId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.schedule.update()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			var demoData = mockups.api.apps.schedule.get;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoDeviceId + '/apps/' + demoAppId + '/schedule/', demoData).respond(200);
			// 			pandoApi.apps.schedule.update(demoDeviceId, demoAppId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.params.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoDeviceId + '/apps/' + demoAppId + '/parameters/').respond(200);
			// 			pandoApi.apps.params.get(demoDeviceId, demoAppId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.params.update()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			var demoData = mockups.api.apps.params.get;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoDeviceId + '/apps/' + demoAppId + '/parameters/', demoData).respond(200);
			// 			pandoApi.apps.params.update(demoDeviceId, demoAppId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('apps.baseGraphicItemData()', function(){
			// 		it('shoud return object', function(){
			// 			var basicData = pandoApi.apps.baseGraphicItemData();
			// 			expect(basicData).to.be.an('object');
			// 		});
			// 	});
			// });
			// /Apps

			// Rules
			// describe('Rules', function(){
			// 	var demoDeviceId = 2;
			// 	var demoId = 1;
			// 	describe('rules.summary()', function(){
			// 		var demoAppId = 3;
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/summary/' + demoAppId + '/').respond(200);
			// 			pandoApi.rules.summary(demoDeviceId, demoAppId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('rules.add()', function(){
			// 		var demoData = mockups.api.rules.get;
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/', demoData).respond(200);
			// 			pandoApi.rules.add(demoDeviceId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('rules.list()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/').respond(200);
			// 			pandoApi.rules.list(demoDeviceId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('rules.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/' + demoId + '/').respond(200);
			// 			pandoApi.rules.get(demoDeviceId, demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('rules.update()', function(){
			// 		var demoData = mockups.api.rules.get;
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/' + demoId + '/', demoData).respond(200);
			// 			pandoApi.rules.update(demoDeviceId, demoId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('rules.remove()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/' + demoId + '/').respond(200);
			// 			pandoApi.rules.remove(demoDeviceId, demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('rules.schedule.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/' + demoId + '/schedule/').respond(200);
			// 			pandoApi.rules.schedule.get(demoDeviceId, demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('rules.schedule.update()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			var demoData = mockups.api.rules.schedule.get;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/' + demoId + '/schedule/', demoData).respond(200);
			// 			pandoApi.rules.schedule.update(demoDeviceId, demoId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('rules.params.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/' + demoId + '/parameters/').respond(200);
			// 			pandoApi.rules.params.get(demoDeviceId, demoId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('rules.params.update()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			var demoData = mockups.api.rules.params.get;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/device/' + demoDeviceId + '/retarget/' + demoId + '/parameters/', demoData).respond(200);
			// 			pandoApi.rules.params.update(demoDeviceId, demoId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// });
			// /Rules

			// Company
			// describe('Company', function(){
			// 	describe('company.create()', function(){
			// 		var demoData = mockups.api.company.get;
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/company/', demoData).respond(200);
			// 			pandoApi.company.create(demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/company/').respond(200);
			// 			pandoApi.company.get(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.update()', function(){
			// 		var demoData = mockups.api.company.get;
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/company/', demoData).respond(200);
			// 			pandoApi.company.update(demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/company/billing/').respond(200);
			// 			pandoApi.company.billing.get(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.update()', function(){
			// 		var demoData = mockups.api.company.billing.get;
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/company/billing/', demoData).respond(200);
			// 			pandoApi.company.billing.update(demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.payment.getToken()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/company/billing/payment/').respond(200);
			// 			pandoApi.company.billing.payment.getToken(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.payment.submitPaymentNonce()', function(){
			// 		var demoNonce = 'somenonce';
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/company/billing/payment/', demoNonce).respond(200);
			// 			pandoApi.company.billing.payment.submitPaymentNonce(demoNonce, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.subscription.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/company/billing/subscription/').respond(200);
			// 			pandoApi.company.billing.subscription.get(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.subscription.get()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/company/billing/subscription/').respond(200);
			// 			pandoApi.company.billing.subscription.get(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.subscription.start()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			var demoData = mockups.api.company.billing.subscription.start;
			// 			$httpBackend.expectPOST(apiRoot + 'rest/company/billing/subscription/', {planId: demoData.planId}).respond(200);
			// 			pandoApi.company.billing.subscription.start(demoData.planId, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.subscription.cancel()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/company/billing/subscription/').respond(200);
			// 			pandoApi.company.billing.subscription.cancel(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.subscription.cancel()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectDELETE(apiRoot + 'rest/company/billing/subscription/').respond(200);
			// 			pandoApi.company.billing.subscription.cancel(function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('company.billing.subscription.byDevices()', function(){
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			var devicesCount = 4;
			// 			$httpBackend.expectPUT(apiRoot + 'rest/company/billing/subscription/', {devicesCount: devicesCount}).respond(200);
			// 			pandoApi.company.billing.subscription.buyDevices(devicesCount, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// });
			// /Company

			// Analytics
			// describe('Analytics', function(){
			// 	describe('analyticsPrepareDate()', function(){
			// 		it('shoud return undefined', function(){
			// 			expect(pandoApi.analyticsPrepareDate()).to.be.undefined;
			// 		});
			// 		it('shoud return the same string', function(){
			// 			expect(pandoApi.analyticsPrepareDate('2016-14-03')).to.be.equal('2016-14-03');
			// 		});
			// 		it('shoud convert date', function(){
			// 			var date = new Date();
			// 			var dateStr = moment(date).format('YYYY-MM-DDTHH:mm:ssZ');
			// 			expect(pandoApi.analyticsPrepareDate(date)).to.be.equal(dateStr);
			// 		});
			// 	})
			// 	describe('analyticsPrepareData()', function(){
			// 		it('shoud return null if data is undefined', function(){
			// 			expect(pandoApi.analyticsPrepareData()).to.be.null;
			// 		});
			// 		it('shoud return null if data is not an object', function(){
			// 			expect(pandoApi.analyticsPrepareData(1)).to.be.null;
			// 			expect(pandoApi.analyticsPrepareData([])).to.be.null;
			// 		});
			// 		it('shoud return correct start date', function(){
			// 			var startDate = '2016-06-03T14:29:44+03:00';
			// 			var data = pandoApi.analyticsPrepareData({startDate: startDate});
			// 			expect(data.startDate).to.be.equal(startDate);
			// 		});
			// 		it('shoud return correct end date', function(){
			// 			var endDate = '2016-06-03T14:29:45+03:00';
			// 			var data = pandoApi.analyticsPrepareData({endDate: endDate});
			// 			expect(data.endDate).to.be.equal(endDate);
			// 		});
			// 		it('shoud returnt correct data when device specified', function(){
			// 			var data = pandoApi.analyticsPrepareData({device: 123});
			// 			expect(data.devices).to.equal("123");
			// 		})
			// 		it('shoud return correct location', function(){
			// 			var data;
			// 			data = pandoApi.analyticsPrepareData({location: "123"});
			// 			expect(data.location).to.be.equal("123");
			// 			data = pandoApi.analyticsPrepareData({location: 123});
			// 			expect(data.location).to.be.equal("123");
			// 		});
			// 		it('shoud return correct devices', function(){
			// 			var data;
			// 			data = pandoApi.analyticsPrepareData({devices: 1});
			// 			expect(data.devices).to.be.equal("1");
			// 			data = pandoApi.analyticsPrepareData({devices: "123"});
			// 			expect(data.devices).to.be.equal("123");
			// 			data = pandoApi.analyticsPrepareData({devices: [1,2,3]});
			// 			expect(data.devices).to.be.equal("1,2,3");
			// 		});
			// 	});
			// 	describe('analytics.pandoId.events.list()', function(){
			// 		var demoPandoId = '123';
			// 		var demoData = {};
			// 		it('shoud make request', function(){
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/history/' + demoPandoId + '/').respond(200);
			// 			pandoApi.analytics.pandoId.events.list(demoPandoId, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('analytics.events.list()', function(){
			// 		var demoData = {};
			// 		it('shoud make request', function(){
			// 			var demoFilter = pandoApi.analytics.filters.walkbys;
			// 			var demoResolution = pandoApi.analytics.resolutions.hourOfDay;
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/analytics/' + demoFilter + '/').respond(200);
			// 			pandoApi.analytics.events.list(demoFilter, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('analytics.events.count()', function(){
			// 		var demoData = {};
			// 		it('shoud make request', function(){
			// 			var demoFilter = pandoApi.analytics.filters.walkbys;
			// 			var demoResolution = pandoApi.analytics.resolutions.hourOfDay;
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/analytics/' + demoFilter + '/' + demoResolution + '/count/').respond(200);
			// 			pandoApi.analytics.events.count(demoFilter, demoResolution, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('analytics.events.duration()', function(){
			// 		var demoData = {};
			// 		it('shoud make request', function(){
			// 			var demoFilter = pandoApi.analytics.filters.walkbys;
			// 			var demoResolution = pandoApi.analytics.resolutions.hourOfDay;
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/analytics/' + demoFilter + '/' + demoResolution + '/duration/').respond(200);
			// 			pandoApi.analytics.events.duration(demoFilter, demoResolution, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('analytics.rates.conversion()', function(){
			// 		var demoData = {};
			// 		it('shoud make request', function(){
			// 			var demoResolution = pandoApi.analytics.resolutions.hourOfDay;
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/analytics/rates/' + demoResolution + '/conversion/').respond(200);
			// 			pandoApi.analytics.rates.conversion(demoResolution, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// 	describe('analytics.rates.new()', function(){
			// 		var demoData = {};
			// 		it('shoud make request', function(){
			// 			var demoResolution = pandoApi.analytics.resolutions.hourOfDay;
			// 			var demoFilter = pandoApi.analytics.filters.walkbys;
			// 			var succeeded;
			// 			$httpBackend.expectGET(apiRoot + 'rest/analytics/rates/' + demoResolution + '/new/' + demoFilter + '/').respond(200);
			// 			pandoApi.analytics.rates.new(demoFilter, demoResolution, demoData, function(err){
			// 				expect(err).to.be.null;
			// 				succeeded = true;
			// 			});
			// 			$httpBackend.flush();
			// 			expect(succeeded).to.be.true;
			// 		});
			// 	});
			// });
			// /Analytics


		});
	});

});