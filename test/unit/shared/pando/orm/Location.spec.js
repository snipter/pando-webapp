describe('pando.orm', function(){

	describe('Location', function(){

		beforeEach(module('pando.orm'));

		var pandoOrm;
		beforeEach(inject(function(_pandoOrm_){
			pandoOrm = _pandoOrm_;
		}));

	    it('shoud exists', function(){
	        expect(pandoOrm.Location).to.be.a('function');
	        expect(pandoOrm.Location.prototype).to.be.an.instanceof(pandoOrm.BaseObj);
	    });

	});

});