describe('pando.orm', function(){

	describe('BaseObj', function(){
		// Demo data
		var demoMapping = {
			id: 'someId',
			attrs: [
				'someId',
				'key1',
				'key2',
				'key3'
			]
		}
		var demoData = {someId: 1234, key1: 'val', key2: [1, 2, 3, 4], key3: 3};

		// Loading module
		beforeEach(module('pando.orm'));
		var pandoOrm;
		beforeEach(inject(function(_pandoOrm_){
			pandoOrm = _pandoOrm_;
		}));

		describe('getIdKeyName()', function(){
			it('shoud rise errors', function(){
				var obj = new pandoOrm.BaseObj();
				var errFn = function(){ obj.getIdKeyName(); }
				expect(errFn).to.throw(Error);
				obj.mapping = {};
				expect(errFn).to.throw(Error);
			});
		});

		describe('getAttrs()', function(){
			it('shoud rise errors', function(){
				var obj = new pandoOrm.BaseObj();
				var errFn = function(){ obj.getAttrs(); }
				expect(errFn).to.throw(Error);
				obj.mapping = {};
				expect(errFn).to.throw(Error);
				obj.mapping.attrs = 'some';
				expect(errFn).to.throw(Error);
			});
		});

		describe('setData()', function(){
			it('shoud set data', function(){
				var obj = new pandoOrm.BaseObj();
				obj.mapping = demoMapping;
				obj.setData(demoData);
				expect(obj.id).to.equal(demoData.someId);
				var keys = Object.keys(demoData);
				_.forEach(keys, function(key){
					expect(obj[key]).to.deep.equal(demoData[key]);
				});
			});
		});

		describe('getData()', function(){
			it('shoud get data', function(){
				var obj = new pandoOrm.BaseObj();
				obj.mapping = demoMapping;
				obj.setData(demoData);
				var newData = obj.getData();
				expect(newData).to.deep.equal(demoData);
			});
		});

		describe('data()', function(){
			it('shoud get data', function(){
				var obj = new pandoOrm.BaseObj();
				obj.mapping = demoMapping;
				obj.setData(demoData);
				var exportedData = obj.data();
				expect(exportedData).to.deep.equal(demoData);
			});
			it('shoud set data', function(){
				var obj = new pandoOrm.BaseObj();
				obj.mapping = demoMapping;
				obj.data(demoData);
				var exportedData = obj.data();
				expect(exportedData).to.deep.equal(demoData);
			});
		});
		
		
	});

});