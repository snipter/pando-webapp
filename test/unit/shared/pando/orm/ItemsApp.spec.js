describe('pando.orm', function(){

	describe('ItemsApp', function(){

		beforeEach(module('pando.orm'));

		var pandoOrm;
		beforeEach(inject(function(_pandoOrm_){
			pandoOrm = _pandoOrm_;
		}));

	    it('shoud exists', function(){
	        expect(pandoOrm.ItemsApp).to.be.a('function');
	        expect(pandoOrm.ItemsApp.prototype).to.be.an.instanceof(pandoOrm.App);
	    });

	});

});