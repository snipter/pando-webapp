describe('pando.orm', function(){

	describe('Company', function(){

		beforeEach(module('pando.orm'));

		var pandoOrm;
		beforeEach(inject(function(_pandoOrm_){
			pandoOrm = _pandoOrm_;
		}));

	    it('shoud exists', function(){
	        expect(pandoOrm.Company).to.be.a('function');
	        expect(pandoOrm.Company.prototype).to.be.an.instanceof(pandoOrm.BaseObj);
	    });

	});

});