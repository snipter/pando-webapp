describe('pando.orm', function(){

	describe('Rule', function(){

		beforeEach(module('pando.orm'));

		var pandoOrm;
		beforeEach(inject(function(_pandoOrm_){
			pandoOrm = _pandoOrm_;
		}));

	    it('shoud exists', function(){
	        expect(pandoOrm.Rule).to.be.a('function');
	        expect(pandoOrm.Rule.prototype).to.be.an.instanceof(pandoOrm.BaseObj);
	    });

	});

});