describe('pando.orm', function(){

	describe('User', function(){

		beforeEach(module('pando.orm'));

		var pandoOrm;
		beforeEach(inject(function(_pandoOrm_){
			pandoOrm = _pandoOrm_;
		}));

		it('shoud exists', function(){
	        expect(pandoOrm.User).to.be.a('function');
	        expect(pandoOrm.User.prototype).to.be.an.instanceof(pandoOrm.BaseObj);
	    });

		describe('get()', function(){
			var demoData = mockups.api.users.get;
			var demoUserId = demoData.userId;
			it('shoud make request', function(){
				pandoOrm.User.get(demoUserId, function(err, user){
					expect(err).to.not.exist;
					expect(user).to.be.an.instanceof(pandoOrm.User);
					expect(user.id).to.equal(demoUserId);
					expect(user.userId).to.equal(demoData.userId);
					expect(user.created).to.equal(demoData.created);
					expect(user.firstName).to.equal(demoData.firstName);
					expect(user.lastName).to.equal(demoData.lastName);
					expect(user.email).to.equal(demoData.email);
					expect(user.companyId).to.equal(demoData.companyId);
					expect(user.permissions).to.deep.equal(demoData.permissions);
					expect(user.isAdmin).to.equal(demoData.isAdmin);
				});
			});
		});

		describe('list()', function(){

		});

	});

});