describe('pando.orm', function(){

	describe('App', function(){

		beforeEach(module('pando.orm'));

		var pandoOrm;
		beforeEach(inject(function(_pandoOrm_){
			pandoOrm = _pandoOrm_;
		}));

	    it('shoud exists', function(){
	        expect(pandoOrm.App).to.be.a('function');
	        expect(pandoOrm.App.prototype).to.be.an.instanceof(pandoOrm.BaseObj);
	    });

	});

});