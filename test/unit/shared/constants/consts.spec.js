describe('consts', function(){

	var consts = null;
	beforeEach(module('app'));
	beforeEach(inject(function(_consts_){
		consts = _consts_;
	}));

	describe('basic test', function(){
		it('shoud exist', function(){
			expect(consts).to.exist;
		});
		it('shoud be a an object', function(){
			expect(consts).to.be.an('object');
		});
	});
	
})