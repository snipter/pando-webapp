describe('paramsStorage', function(){

	var paramsStorage = null;
	var keysPrefix = null;

	beforeEach(module('app'));
	beforeEach(inject(function(_paramsStorage_){
		paramsStorage = _paramsStorage_;
		keysPrefix = paramsStorage.keysPrefix;
	}));

	var demoData = {some: 'val'};
	var demoDataStr = JSON.stringify(demoData);
	var demoKey = 'someKey';

	describe('basic test', function(){
		it('shoud exist', function(){
			expect(paramsStorage).to.exist;
		});
	});

	describe('set()', function(){
		it('shoud save data to localStorage', function(){
			paramsStorage.set(demoKey, demoData);
			expect(localStorage.getItem(keysPrefix + ':' + demoKey)).to.be.equal(demoDataStr);
		});
	});

	describe('get()', function(){
		it('shoud load data from localStorage', function(){
			paramsStorage.set(demoKey, demoData);
			var loadedData = paramsStorage.get(demoKey);
			expect(loadedData).to.deep.equal(demoData);
		});
		it('shoud return null if data not found', function(){
			expect(paramsStorage.get(demoKey + '123')).to.be.null;
		});
		it('shoud return undefined if data equal to undefined str', function(){
			localStorage.setItem(keysPrefix + ':' + demoKey, 'undefined');
			expect(paramsStorage.get(demoKey)).to.be.undefined;
		});
	});

	describe('remove()', function(){
		it('shoud remove data from localStorage', function(){
			paramsStorage.set(demoKey, demoData);
			paramsStorage.remove(demoKey);
			expect(localStorage.getItem(keysPrefix + ':' + demoKey)).to.be.null;
		});
	})

});