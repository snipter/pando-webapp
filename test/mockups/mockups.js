var mockups = {
	api: {
		auth: {
			login: readJSON('test/mockups/api.auth.login.json')
		}
		,users: {
			get: readJSON('test/mockups/api.users.get.json')
		}
		,devices: {
			 get: readJSON('test/mockups/api.devices.get.json')
			,listAll: readJSON('test/mockups/api.devices.listAll.json')
			,realtime: {
				 current: readJSON('test/mockups/api.devices.realtime.current.json')
				,lastHour: readJSON('test/mockups/api.devices.realtime.lastHour.json')
				,lastHourEvent: readJSON('test/mockups/api.devices.realtime.lastHour.event.json')
			}
		}
		,apps: {
			 list: readJSON('test/mockups/api.apps.list.json')
			,get: readJSON('test/mockups/api.apps.get.json')
			,updateDashboard: readJSON('test/mockups/api.apps.updateDashboard.json')
			,schedule: {
				 get: readJSON('test/mockups/api.apps.schedule.get.json')
			}
			,params: {
				 get: readJSON('test/mockups/api.apps.params.get.json')
			}
		}
		,rules: {
			 get: readJSON('test/mockups/api.rules.get.json')
			,list: readJSON('test/mockups/api.rules.list.json')
			,schedule: {
				 get: readJSON('test/mockups/api.rules.schedule.get.json')
			}
			,params: {
				 get: readJSON('test/mockups/api.rules.params.get.json')
			}
		}
		,company: {
			 get: readJSON('test/mockups/api.company.get.json')
			,billing: {
				 get: readJSON('test/mockups/api.company.billing.get.json')
				,payment: {
					 getToken: readJSON('test/mockups/api.company.billing.payment.getToken.json')
				}
				,subscription: {
					 get: readJSON('test/mockups/api.company.billing.subscription.get.json')
					,start: readJSON('test/mockups/api.company.billing.subscription.start.json')
				}
			}
		}
	}
}