var gulp = require('gulp');
var browserSync = require('browser-sync');
var $ = require('gulp-load-plugins')({ lazy: true });
var runSequence = require('run-sequence');
var historyApiFallback = require('connect-history-api-fallback');
var lazypipe = require('lazypipe');

/*============ Config ============*/

var package = require('./package.json');
var prod = $.util.env.prod;

var srcPath = './src';
var distPath = './dist';
var tmpPath = './.tmp';
var coveragePath = './coverage';

var fontelloFile = {
    path: './config/fontello.json'
};

// Env config
var envFile = {
    name: 'app.env.js',
    path: 'app',
    module: 'app.env'
}

// HTML Cache File Config
var htmlCacheFile = {
    name: 'app.templates.js',
    path: 'app',
    module: 'app.templates'
}

// HTML min config
var htmlMinConfig = {
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true,
    removeComments: true
}

/*============ Help ============*/

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

/*============ Validate ============*/

gulp.task('validate', function () {
    runSequence('validate:html', 'validate:js');
})

gulp.task('validate:html', function () {
    return gulp.src([srcPath + "/app/index.html", srcPath + "/app/**/*.html"])
        .pipe($.htmlhint({ 'doctype-first': false }))
        .pipe($.htmlhint.reporter());
});

gulp.task('validate:js', function () {
    return gulp.src(srcPath + "/app/**/*.js")
        .pipe($.jshint())
        .pipe($.jshint.reporter('default'));
});

/*============ Fontello ============*/

gulp.task('fontello:update', function () {
    return gulp.src(fontelloFile.path)
        .pipe($.fontello({ font: 'fonts', css: 'css' }))
        .pipe(gulp.dest(srcPath + '/assets/fontello'));
});

/*============ SASS ============*/

gulp.task('sass:tmp', function () {
    return gulp
        .src(srcPath + '/assets/scss/style.scss')
        .pipe(!prod ? $.sourcemaps.init() : $.util.noop())
        .pipe($.sass({ outputStyle: 'nested' }))
        .pipe(!prod ? $.sourcemaps.write() : $.util.noop())
        .pipe(gulp.dest(tmpPath + '/assets/css'));
});

gulp.task('sass:tmp:watch', function () {
    gulp.watch(srcPath + '/**/*.scss', ['sass:tmp']);
});

/*============ Images ============*/

gulp.task('img:dist', function () {
    return gulp.src(srcPath + '/assets/img/**')
        .pipe($.imagemin())
        .pipe(gulp.dest(distPath + '/assets/img'));
});

/*============ Fonts ============*/

gulp.task('fonts:dist', function () {
    return gulp.src([
        srcPath + '/assets/fonts/**'
        , srcPath + '/assets/fontello/fonts/**'
    ])
        .pipe(gulp.dest(distPath + '/assets/fonts'));
});

/*============ HTML Cache ============*/

gulp.task('htmlcache:tmp', function () {
    return gulp.src([
        srcPath + '/app/**/*.html'
        , '!' + srcPath + '/index.html'
    ])
        .pipe(prod ? $.htmlmin(htmlMinConfig) : $.util.noop())
        .pipe($.angularTemplatecache(htmlCacheFile.name, {
            standalone: true
            , module: htmlCacheFile.module
            , root: 'app/'
        }))
        .pipe(gulp.dest(tmpPath + '/' + htmlCacheFile.path));
});

gulp.task('htmlcache:tmp:watch', function () {
    gulp.watch(srcPath + '/app/**/*.html', ['htmlcache:tmp']);
});

/*============ Env Variables ============*/

gulp.task('env:tmp', function () {
    var envData = require('./config/env.json');
    envData = prod ? envData.prod : envData.dev;
    envData.version = package.version;
    envData.production = prod;
    return strToFile(envFile.name, dataToEnvVariablesCode(envData))
        .pipe(gulp.dest(tmpPath + '/' + envFile.path));
});

function strToFile(filename, string) {
    var src = require('stream').Readable({ objectMode: true })
    src._read = function () {
        this.push(new $.util.File({ cwd: "", base: "", path: filename, contents: new Buffer(string) }))
        this.push(null)
    }
    return src
}

function dataToEnvVariablesCode(data) {
    return "angular.module('" + envFile.module + "', [])" + "\n" +
        "   .constant('env', " + JSON.stringify(data) + ");"
}

/*============ HTML ============*/

var appWrapTmpl =
    "(function(angular, moment, _, md5, async){" + "\n" +
    "'use strict';" + "\n" +
    "<%= contents %>" + "\n" +
    "})(angular, moment, _, md5, async);"

var appModChanel = lazypipe()
    .pipe($.ngAnnotate)
    .pipe($.wrap, appWrapTmpl, {}, { parse: false })
    .pipe($.uglify);

var appFileFilter = function (file) {
    if (!file.history) return false;
    if (!file.history.length) return false;
    var fileName = file.history[0]
    if (/\/app\/.+?\.js$/g.test(fileName)) {
        return true;
    } else {
        return false;
    }
}

gulp.task('html:dist', function () {
    return gulp.src(srcPath + '/index.html')

        .pipe($.useref({ searchPath: [srcPath, tmpPath] }))
        .pipe($.if(appFileFilter, appModChanel()))
        .pipe($.if('*.js', $.rev()))
        .pipe($.if('*.css', $.rev()))
        .pipe($.revReplace())

        .pipe($.if('*.css', $.cleanCss()))
        .pipe($.if('*.js', $.uglify()))

        .pipe($.if('*.html', $.htmlmin(htmlMinConfig)))

        .pipe(gulp.dest(distPath));
});

/*============ Dist ============*/

gulp.task('dist', function (cb) {
    runSequence('clean', ['env:tmp', 'htmlcache:tmp', 'sass:tmp', 'img:dist', 'fonts:dist'], 'html:dist', cb);
});

/*============ Watch ============*/

gulp.task('watch', ['sass:tmp:watch', 'htmlcache:tmp:watch']);

/*============ Serving ============*/

gulp.task('serve', ['serve:src']);

gulp.task('serve:src', ['sass:tmp', 'env:tmp', 'htmlcache:tmp', 'watch'], function () {
    startBrowserSync([srcPath, tmpPath]);
});

gulp.task('serve:dist', ['dist'], function () {
    startBrowserSync(distPath);
});

function startBrowserSync(basePaths) {
    if (typeof basePaths === 'string') basePaths = [basePaths];
    var filesToWatch = [];
    basePaths.forEach(function (path) {
        filesToWatch.push(path + '/**/*.html');
        filesToWatch.push(path + '/**/*.css');
        filesToWatch.push(path + '/**/*.js');
        filesToWatch.push(path + '/**/*.jpg');
        filesToWatch.push(path + '/**/*.png');
        filesToWatch.push(path + '/**/*.svg');
        filesToWatch.push(path + '/**/*.eot');
        filesToWatch.push(path + '/**/*.ttf');
        filesToWatch.push(path + '/**/*.woff');
        filesToWatch.push(path + '/**/*.woff2');
        filesToWatch.push(path + '/**/*.json');
    });
    var options = {
        port: 3030,
        ghostMode: {
            clicks: false,
            location: false,
            forms: false,
            scroll: true
        },
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 0,
        online: false,
        server: {
            baseDir: basePaths
        },
        files: filesToWatch,
        middleware: [historyApiFallback()]
    };
    browserSync(options);
}

/*============ Clear ============*/

gulp.task('clean', function (done) {
    return gulp.src([tmpPath, distPath, coveragePath], { read: false })
        .pipe($.clean());
});

/*============ Deploy ============*/

gulp.task('deploy:s3', function () {
    var s3Credentials = getS3Credentials();
    $.util.log('deploing to: ' + JSON.stringify(s3Credentials.bucket));
    gulp.src("./dist/**")
        .pipe($.s3(s3Credentials));
});

function getS3Credentials(){
    var config = {
        key: process.env.PANDO_S3_KEY,
        secret: process.env.PANDO_S3_SECRET,
        bucket: process.env.PANDO_S3_BUCKET,
        region: process.env.PANDO_S3_REGION,
    }

    // key
    if(prod && process.env.PANDO_PROD_S3_KEY) config.key = process.env.PANDO_PROD_S3_KEY;
    else if(!prod && process.env.PANDO_DEV_S3_KEY) config.key = process.env.PANDO_DEV_S3_KEY;
    // secret
    if(prod && process.env.PANDO_PROD_S3_SECRET) config.secret = process.env.PANDO_PROD_S3_SECRET;
    else if(!prod && process.env.PANDO_DEV_S3_SECRET) config.secret = process.env.PANDO_DEV_S3_SECRET;
    // bucket
    if(prod && process.env.PANDO_PROD_S3_BUCKET) config.bucket = process.env.PANDO_PROD_S3_BUCKET;
    else if(!prod && process.env.PANDO_DEV_S3_BUCKET) config.bucket = process.env.PANDO_DEV_S3_BUCKET;
    // region
    if(prod && process.env.PANDO_PROD_S3_REGION) config.region = process.env.PANDO_PROD_S3_REGION;
    else if(!prod && process.env.PANDO_DEV_S3_REGION) config.region = process.env.PANDO_DEV_S3_REGION;

    return config;
}