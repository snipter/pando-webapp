# Pando App

Web client for [getpando.com](http://getpando.com/).

## Links

- [API](http://docs.vehicle.apiary.io/)
- [Chart.js](http://www.chartjs.org/docs/)
- [angular-chart.js](http://jtblin.github.io/angular-chart.js/)

## Used software versions

- **node** - 4.5.0
- **npm** - 2.15.9
- **gulp (CLI)** - 1.2.1
- **gulp (local)** - 3.9.1

## Config

For configuring deploy development and production versions create file  `app/config.json`. Checkout `app/config.example.json` files for example of file's structure.

## Enviromoment variables

For creating app's environment variables create `app/env.json` file. Checkout `app/env.example.json` file for example of file's structure. Environment variables allow you to pass params to the app. All parameters will be avliable as angular's constant `env`. Example of use:

```javascript
angular.module('app')
	.controller('myCtrl', function(env){
		console.log('api.root: ' + env.apiRoot);
	});
```

## Install

Install npm packages:

```
npm install
```

## Run and distribute

In order to run app use:

```
gulp serve
```

In order to create distribution version of app use: 

```
gulp dist
```

The distribution version of app will be saved at `dist` folder.

Cerate dist version of app and run:

```
gulp serve:dist
```

By default gulp will use `dev` configs from `config.json` file and `dev` environment variables from `env.json` file. In order to use `prod` version of data perform all gulp's commands with `--prod` file. For example:

```
gulp serve --prod
gulp dist --prod
```

## Showing log

To show log add variable **log.level** to your local resources. The next levels are supported: **trace**, **debug**, **info**, **warning**, **error**. For example:

```
log.level = trace
```

## Changing API root

To change api root add variable **api.root** to your local resources. For example:

```
api.root = https://platform-testing.getpando.com/
```

By default application using **https://platform.getpando.com/** url.

## Configurate deploy

Add next enviromoment variables in order to deploy to S3:

```
export PANDO_DEV_S3_KEY=%key%
export PANDO_DEV_S3_SECRET=%secret%
export PANDO_DEV_S3_BUCKET=app-testing.getpando.com
export PANDO_DEV_S3_REGION=us-west-2

export PANDO_PROD_S3_KEY=%key%
export PANDO_PROD_S3_SECRET=%secret%
export PANDO_PROD_S3_BUCKET=app.getpando.com
export PANDO_PROD_S3_REGION=us-west-2
```

Then you will be able to deploy to S3 using next commands:

```
# Dev
$ gulp dist
$ gulp deploy:s3

# Prod
$ gulp dist --prod
$ gulp deploy:s3 --prod
```

## Contacts

Front-end developer: Jaroslav Khorishchenko (Ukraine)

**Email**: [websnipter@gmail.com](mailto:websnipter@gmail.com)

**Facebook**: [http://fb.com/snipter](http://fb.com/snipter)